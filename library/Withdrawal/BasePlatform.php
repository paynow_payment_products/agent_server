<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/2/24
 * Time: 下午3:43
 */

namespace Lib\Withdrawal;

use Carbon\Carbon;
use Withdrawal\LogModel as WithdrawalLog;
use Withdrawal\PlatformModel as WithdrawalPlatform;
use Withdrawal\WithdrawalModel as Withdrawal;

abstract class BasePlatform
{
    protected $_platform_order_num_key;
    protected $_order_num;

    // 把平台提现数据转换为支付平台对应的格式
    // 供脚本调用，输入平台的提现信息，转化为第三方平台需要的信息后调用公用方法发送给第三方
    public function cliWithdrawalRequest(Withdrawal $oWithdrawal)
    {
        // 调用子类的方法生成待提交的数据
        $aFormData = $this->createFormDataForWithdrawalRequest($oWithdrawal);

        // 提交数据
        $sRequestDateTime  = Carbon::now(); // 记录curl开始前时间
        $aResult           = $this->sendRequest($oWithdrawal, $aFormData);
        $sResponseDateTime = Carbon::now(); // 记录curl结束时的时间

        // 存档请求日志供后台查询
        WithdrawalLog::create([
            'withdrawal_id' => $oWithdrawal->withdrawal_id,
            'action_type'   => WithdrawalLog::ACTION_TYPE_REQUEST,
            'url'           => $oWithdrawal->platform->request_url,
            'request_data'  => $aFormData,
            'request_time'  => $sRequestDateTime,
            'response_data' => $aResult,
            'response_time' => $sResponseDateTime,
        ]);

        if (empty($aResult)) {
            return false;
        }

        // 记录 第三方生成的订单号
        if (!empty($aResult[$this->_platform_order_num_key])) {
            $oWithdrawal->update([
                'platform_order_no' => $aResult[$this->_platform_order_num_key],
            ]);
        }

        // 把接收到的数据转交给子类的方法进行处理
        return $this->resolveResultFromWithdrawalRequest($oWithdrawal, $aFormData, $aResult);
    }

    protected function sendRequest(Withdrawal $oWithdrawal, $aFormData)
    {
        $sResult = post($oWithdrawal->platform->request_url, $aFormData);
        return json_decode($sResult, true);
    }

    // 生成提交给提现平台的表单数据
    abstract protected function createFormDataForWithdrawalRequest(Withdrawal $oWithdrawal);

    // 处理提现平台回调数据
    abstract protected function resolveResultFromWithdrawalRequest(Withdrawal $oWithdrawal, $aFormData, $aResult);

    abstract protected function error($msg);

    /**
     * 供控制器调用，实现第三方平台出款时的信息确认操作
     * 说明:
     * 针对不同的平台可能有不同的url地址格式，
     * 针对特殊的格式需要单独封装，所以把请求时的参数或表单信息交给控制器校验和处理
     *
     * @param array  $aRequestData
     * @param object $oWithdrawalPlatform
     *
     * @return void
     */
    public function actionWithdrawalInfoConfirm($aRequestData, $oWithdrawalPlatform)
    {
        $iPlatformId      = $oWithdrawalPlatform->platform_id;
        $sRequestUrl      = "//$iPlatformId/withdrawal-info-confirm";
        $sRequestDateTime = Carbon::now();

        // 获取提现记录
        $sPlatformOrderId = array_get($aRequestData, $this->_platform_order_num_key, '');
        if (empty($sPlatformOrderId)) {
            $this->error('invalid order num!!!');
            exit;
        }

        $oWithdrawal = Withdrawal::where('platform_id', $iPlatformId)->where('platform_order_no', $sPlatformOrderId)->first();
        if (is_null($oWithdrawal)) {
            $this->error('invalid order num!!!');
            exit;
        }

        // 解析信息查询请求
        $aResult = $this->resolveWithdrawalInfoConfirm($aRequestData, $oWithdrawal);

        // 存档请求日志供后台查询
        WithdrawalLog::create([
            'withdrawal_id' => $oWithdrawal->withdrawal_id,
            'action_type'   => WithdrawalLog::ACTION_TYPE_CONFIRM,
            'url'           => $sRequestUrl,
            'request_data'  => $aRequestData,
            'request_time'  => $sRequestDateTime,
            'response_data' => $aResult,
            'response_time' => Carbon::now(),
        ]);
        exit;
    }

    abstract protected function resolveWithdrawalInfoConfirm($data, Withdrawal $oWithdrawal);

    // 主动调用支付平台接口查询时，支付平台返回的数据处理成平台可以接受的状态
    public function cliWithdrawalQuery($oWithdrawal, $oQuery)
    {
    }

    protected $transfer_params = false;
    protected function transferParams($aRequestData)
    {
        return $aRequestData;
    }

    /**
     * 接收支付平台的状态通知，并且把状态映射为平台所用的状态
     * 供控制器调用，实现DP的信息推送
     * 说明:
     * 针对不同的平台可能有不同的url地址格式，
     * 针对特殊的格式需要单独封装，所以把请求时的参数或表单信息交给控制器校验和处理
     *
     * @param $aRequestData
     * @param $oWithdrawalPlatform
     */
    public function actionWithdrawalResult($aRequestData, $oWithdrawalPlatform)
    {

        if ($this->transfer_params) {
            $aRequestData = $this->transferParams($aRequestData);
        }

        if (empty($aRequestData)) {
            throw new Exception('invalid callback');
        }

        $iPlatformId      = $oWithdrawalPlatform->platform_id;
        $sRequestUrl      = "//$iPlatformId/withdrawal-result";
        $sRequestDateTime = Carbon::now();

        // 获取提现记录
        $sOrderId         = array_get($aRequestData, $this->_order_num, '');
        $sPlatformOrderId = array_get($aRequestData, $this->_platform_order_num_key, '');
        if (empty($sPlatformOrderId) && empty($sOrderId)) {
            $this->error('invalid order num!!!');
            exit;
        }

        $oWithdrawal = Withdrawal::where('platform_id', $iPlatformId)->where('platform_order_no', $sPlatformOrderId)->first();
        if (is_null($oWithdrawal)) {
            $oWithdrawal = Withdrawal::where('serial_number', $sOrderId)->first();
            if (is_null($oWithdrawal)) {
                $this->error('invalid order num!!!');
                exit;
            }
        }

        // 解析信息查询请求
        $aResult = $this->resolveWithdrawalResult($aRequestData, $oWithdrawal);

        // 存档请求日志供后台查询
        WithdrawalLog::create([
            'withdrawal_id' => $oWithdrawal->withdrawal_id,
            'action_type'   => WithdrawalLog::ACTION_TYPE_RESULT,
            'url'           => $sRequestUrl,
            'request_data'  => $aRequestData,
            'request_time'  => $sRequestDateTime,
            'response_data' => $aResult,
            'response_time' => Carbon::now(),
        ]);
        exit;
    }

    abstract protected function resolveWithdrawalResult($aRequestData, Withdrawal $oWithdrawal);

}
