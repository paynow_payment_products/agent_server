<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2018/9/5
 * Time: 17:01
 */

namespace Lib\Withdrawal;

use Core\Singleton;
use Withdrawal\PlatformModel;
use Withdrawal\WithdrawalModel as Withdrawal;

class WithdrawalPNX extends BasePlatform
{
    use Singleton;

    protected function createFormDataForWithdrawalRequest(Withdrawal $oWithdrawal)
    {
        $platform = $oWithdrawal->platform;
        $data     = [
            'app_id'         => $platform->platform_account,
            'app_order_no'   => $oWithdrawal->serial_number,
            'app_user'       => $oWithdrawal->user->app_user_id,
            'app_order_time' => date('Y-m-d H:i:s'),
            'amount'         => $oWithdrawal->amount * 100,
            'card_name'      => $oWithdrawal->card->name,
            'card_no'        => $oWithdrawal->card->card_no,
            'bank_id'        => $oWithdrawal->card->bank_id,
            'province'       => $oWithdrawal->card->province,
            'city'           => $oWithdrawal->card->city,
            'branc_name'     => $oWithdrawal->card->branc_name,
            'notify_url'     => 'http://notify.minghui4.top/api/withdrawal/notify/platform_id/' . $oWithdrawal->platform_id,
            'verify_url'     => '',
            'note'           => '',
            'client_ip'      => get_client_ip(),
        ];
        $data['sign'] = $this->compileLoadSign($platform, $data);
        return $data;
    }

    protected $needSignColums = [
        'app_id',
        'app_order_no',
        'app_user',
        'app_order_time',
        'amount',
        'card_name',
        'card_no',
        'bank_id',
        'province',
        'city',
        'branc_name',
        'notify_url',
        'verify_url',
        'note',
        'client_ip',
        'success',
        'order_no',
        'message',
        'pay_amount',
        'pay_time',
    ];

    public function compileLoadSign(PlatformModel $platform, array $data)
    {
        $tip   = '';
        $words = '';
        ksort($data);
        foreach ($data as $key => $value) {
            if ($key == 'sign' || empty($value) || !in_array($key, $this->needSignColums)) {
                continue;
            }
            $words .= $tip . $key . '=' . $value;
            $tip = '&';
        }
        $words .= $platform->platform_key;

        return strtolower(md5($words));
    }

    protected $_platform_order_num_key = 'order_no';
    protected $_order_num              = "app_order_no";
    protected function resolveResultFromWithdrawalRequest(Withdrawal $oWithdrawal, $aFormData, $aResult)
    {
        // 接口故障
        if (empty($aResult['success']) || $aResult['success'] != 1 || !isset($aResult[$this->_platform_order_num_key]) || $aResult[$this->_platform_order_num_key] == '') {
            $oWithdrawal->transfer_failed();

            return false;
        }

        return true;
    }

    protected function error($msg)
    {
        echo 'error:' . $msg;
    }

    protected function resolveWithdrawalInfoConfirm($data, Withdrawal $oWithdrawal)
    {
        return false;
    }

    protected function resolveWithdrawalResult($aRequestData, Withdrawal $oWithdrawal)
    {
        if (empty($oWithdrawal)) {
            $return['error_msg'] = "order cannot find!";
            $return['status']    = 0;
            self::echoCallBack($return);

            return $return;
        }

        if ($aRequestData['sign'] != $this->compileLoadSign($oWithdrawal->platform, $aRequestData)) {

            $return['error_msg'] = "verify sign failed!!!";
            $return['status']    = 0;
            self::echoCallBack($return);

            return $return;
        }

        $company_order_num  = array_get($aRequestData, $this->_order_num);
        $mownecum_order_num = array_get($aRequestData, $this->_platform_order_num_key);
        $return             = array(
            "error_msg"          => "",
            "mownecum_order_num" => $mownecum_order_num,
            "company_order_num"  => $company_order_num,
            "status"             => "",
        );

        // 检查提现订单的状态
        $status = $oWithdrawal->getAttribute('status');
        if ($status != Withdrawal::WITHDRAWAL_STATUS_TRANSFER_START) {

            if ($status == Withdrawal::WITHDRAWAL_STATUS_SUCCESS) {
                echo "SUCCESS";
                return "SUCCESS";
            }

            $return['error_msg'] = "status check failed!";
            $return['status']    = 0;
            self::echoCallBack($return);

            return $return;
        }

        if ($aRequestData['success'] != "1") {
            $oWithdrawal->transfer_failed();
            $return['status'] = 0;
        } else {
            $oWithdrawal->update([
                'status' => Withdrawal::WITHDRAWAL_STATUS_SUCCESS,
            ]);
            $return['status'] = 1;
        }

        echo "SUCCESS";
        return $return;
    }

    public static function echoCallBack($msg, $fWithHeader = true)
    {
        if ($fWithHeader) {
            header('Content-Type: application/json');
        }
        echo json_encode($msg);
    }

}
