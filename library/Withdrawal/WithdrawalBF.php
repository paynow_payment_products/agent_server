<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/10/3
 * Time: 16:16
 */

namespace Lib\Withdrawal;

use Carbon\Carbon;
use Core\Singleton;
use Withdrawal\QueryLogModel;
use Withdrawal\WithdrawalModel as Withdrawal;

class WithdrawalBF extends BasePlatform
{
    use Singleton;

    // 私钥路径
    const prv_key_fileName = MAIN_PATH . '/library/Payment/FB_pem/102018042400000008_prv.pem';
    // 公钥路径
    const pub_key_fileName = MAIN_PATH . '/library/Payment/FB_pem/102018042400000008_pub.pem';

    public $banks = [
        '1'  => [
            'name' => '中国工商银行',
            'sign' => 'ICBC',
        ],
        '2'  => [
            'name' => '招商银行',
            'sign' => 'CMB',
        ],
        '3'  => [
            'name' => '中国建设银行',
            'sign' => 'CCB',
        ],
        '4'  => [
            'name' => '中国农业银行',
            'sign' => 'ABC',
        ],
        '5'  => [
            'name' => '中国银行',
            'sign' => 'BOC',
        ],
        '6'  => [
            'name' => '交通银行',
            'sign' => 'COMM',
        ],
        '7'  => [
            'name' => '中国民生银行',
            'sign' => 'CMBC',
        ],
        '8'  => [
            'name' => '中信银行',
            'sign' => 'CNCB',
        ],
        '9'  => [
            'name' => '浦发银行',
            'sign' => 'SPDB',
        ],
        '10' => [
            'name' => '中国邮政储蓄银行',
            'sign' => 'PSBC',
        ],
        '11' => [
            'name' => '中国光大银行',
            'sign' => 'CEB',
        ],
        '12' => [
            'name' => '平安银行',
            'sign' => 'PAB',
        ],
        '13' => [
            'name' => '广发银行股份有限公司',
            'sign' => 'GDB',
        ],
        '14' => [
            'name' => '华夏银行',
            'sign' => 'HXB',
        ],
        '15' => [
            'name' => '兴业银行',
            'sign' => 'CIB',
        ],
    ];

    protected $_platform_order_num_key = "orderNo";
    protected $_order_num              = 'orderNo';
    protected $transfer_params         = true;

    public function transferParams($aRequestData)
    {
        return $this->formartResponse($_POST['params']);
    }

    public function formatParams($params)
    {
        $data = array();
        // save as key=>key=value
        foreach ($params as $key => $value) {
            $data[$key] = $key . '=' . $value;
        }
        //  Sort an array by key asc
        ksort($data);
        // Join array elements with  &
        return implode('&', $data);
    }

    // 生成提交给提现平台的表单数据
    protected function createFormDataForWithdrawalRequest(Withdrawal $oWithdrawal)
    {
        $platform = $oWithdrawal->platform;

        $data = [
            'requestNo'     => md5($oWithdrawal->serial_number . time()),
            'version'       => 'V1.0',
            'productId'     => '0202',
            'transId'       => '01',
            'merNo'         => $platform->platform_account,
            'orderDate'     => $oWithdrawal->created_at->format('Ymd'),
            'orderNo'       => $oWithdrawal->serial_number,
            'returnUrl'     => 'https://www.minghui4.top/m',
            'notifyUrl'     => 'http://notify.minghui4.top/api/withdrawal/notify/platform_id/' . $oWithdrawal->platform_id,
            'transAmt'      => $oWithdrawal->amount * 100,
            'commodityName' => "奖品兑换",
            'usrSyt'        => 1,
            'terminalType'  => 1,
            'accName'       => empty($oWithdrawal->card->name) ? $oWithdrawal->user->name : $oWithdrawal->card->name,
            'acctNo'        => $oWithdrawal->card->card_no,
            'cardCity'      => $oWithdrawal->card->city,
            'payAccType'    => array_get($platform->custom_params, 'payAccType', "10"),
        ];

        $data['signature'] = $this->compileLoadSign($platform, $data);
        return $data;
    }

    // 签名
    public function compileLoadSign($platform, array $data)
    {
        $signstr = $this->formatParams($data);
        $pkcs12  = file_get_contents(self::prv_key_fileName);
        $pKey    = openssl_pkey_get_private($pkcs12, $platform->platform_key);
        openssl_sign($signstr, $sign, $pKey);
        return base64_encode($sign);
    }

    public function formartResponse($result)
    {
        $response = [];
        foreach (explode("&", $result) as $value) {
            $tmp               = explode("=", $value);
            $response[$tmp[0]] = $tmp[1];
        }
        return $response;
    }

    public function verify($params)
    {
        // separate with &
        $paramsKVArray = explode("&", $params);
        $paramsArray   = array();
        foreach ($paramsKVArray as $key => $value) {
            // separate with = and save key-value in array
            $temp = explode("=", $value);
            if ('signature' == $temp[0]) {
                // get signature
                $signature = $temp[1];
                continue;
            }
            // convert to array
            $paramsArray[$temp[0]] = $temp[1];
        }
        // join params
        $data = $this->formatParams($paramsArray);
        // verify
        return $this->verifySign($data, $signature, null);
    }

    public function verifySign($data, $signature, $dataType)
    {
        if ('array' == $dataType) {
            // convert array to string
            $data = $this->formatParams($data);
        }
        // read public key content
        $pkcs12 = file_get_contents(self::pub_key_fileName);
        // get public key id
        $pubkeyid = openssl_get_publickey($pkcs12);
        if (!is_resource($pubkeyid)) {
            return false;
        }
        // verify sign
        $signature    = base64_decode($signature);
        $verifyResult = openssl_verify($data, $signature, $pubkeyid);
        if (1 == $verifyResult) {
            // success and Free key resource
            openssl_free_key($pubkeyid);
            return true;
        }
        return false;
    }

    private function CURL($url, $data)
    {
        // 启动一个CURL会话
        $ch = curl_init();
        // 设置curl允许执行的最长秒数
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        // 忽略证书
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //  获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        // 发送一个常规的POST请求。
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        // 是否需要头部信息（否）
        curl_setopt($ch, CURLOPT_HEADER, 0);
        // 执行操作
        $result = curl_exec($ch);
        // 返回数据
        return $result;
    }

    // 发送数据
    protected function sendRequest(Withdrawal $oWithdrawal, $aFormData)
    {
        $sResult = $this->CURL($oWithdrawal->platform->request_url, $aFormData);

        $verifyResult = $this->verify($sResult);
        if ($verifyResult) {
            return $this->formartResponse($sResult);
        } else {
            return $sResult;
        }
    }

    // 处理发起提现接口返回数据
    protected function resolveResultFromWithdrawalRequest(Withdrawal $oWithdrawal, $aFormData, $aResult)
    {
        if (is_array($aResult) && $aResult['respCode'] == 'P000') {

            if (!empty($aResult['orderNo'])) {
                $oWithdrawal->update([
                    'platform_order_no' => $aResult['orderNo'],
                ]);
            }
            return true;
        }

        $oWithdrawal->transfer_failed();
        return false;
    }

    protected function error($msg)
    {

    }

    protected function resolveWithdrawalInfoConfirm($data, Withdrawal $oWithdrawal)
    {

    }

    // 主动调用支付平台接口查询时，支付平台返回的数据处理成平台可以接受的状态
    public function cliWithdrawalQuery($oWithdrawal, $oQuery)
    {
        $data = [
            'requestNo' => md5($oWithdrawal->serial_number . time()),
            'version'   => 'V1.0',
            'transId'   => '03',
            'merNo'     => $oWithdrawal->platform->platform_account,
            'orderDate' => $oWithdrawal->created_at->format('Ymd'),
            'orderNo'   => $oWithdrawal->serial_number,
        ];

        $strsign           = $this->compileLoadSign($oWithdrawal->platform, $data);
        $data['signature'] = $strsign;

        $sResult = $this->CURL($oWithdrawal->platform->query_url, $data);

        $verifyResult = $this->verify($sResult);
        if ($verifyResult) {
            $aResult = $this->formartResponse($sResult);
        } else {
            return $sResult;
        }

        $request_time = Carbon::now();
        $oQuery->queryLog()->save(
            new QueryLogModel([
                'request_url'   => $oWithdrawal->platform->query_url,
                'request_data'  => $data,
                'request_time'  => $request_time,
                'response_data' => $aResult,
                'response_time' => Carbon::now(),
            ])
        );

        if (is_array($aResult) && $aResult['respCode'] == "0000") {

            if ($aResult['origRespCode'] == "0000") {
                // 检查提现订单的状态
                $status = $oWithdrawal->getAttribute('status');
                if ($status != Withdrawal::WITHDRAWAL_STATUS_TRANSFER_START) {

                    if ($status == Withdrawal::WITHDRAWAL_STATUS_SUCCESS) {
                        return 1;
                    }
                    return 0;
                }

                $oWithdrawal->update([
                    'status' => Withdrawal::WITHDRAWAL_STATUS_SUCCESS,
                ]);
                return 1;

            } elseif (in_array($aResult['origRespCode'], [9998, 0046, 0042])) {
                $oWithdrawal->transfer_failed();
                return 1;
            }
            return 0;
        }
        return 0;
    }

    // 处理根据首付款返回的通知数据 处理订单
    protected function resolveWithdrawalResult($aRequestData, Withdrawal $oWithdrawal)
    {

        if (empty($oWithdrawal)) {
            $return['error_msg'] = "mownecum_order_num cannot find!";
            $return['status']    = 0;
            self::echoCallBack($return);

            return $return;
        }

        if ($this->verifySign($aRequestData, $aRequestData['signature'], 'array')) {

            $return['error_msg'] = "verify sign failed!!!";
            $return['status']    = 0;
            self::echoCallBack($return);

            return $return;
        }

        $company_order_num  = array_get($aRequestData, $this->_order_num);
        $mownecum_order_num = array_get($aRequestData, $this->_platform_order_num_key);
        $return             = array(
            "error_msg"          => "",
            "mownecum_order_num" => $mownecum_order_num,
            "company_order_num"  => $company_order_num,
            "status"             => "",
        );

        if ($aRequestData['respCode'] == "0000") {
            // 检查提现订单的状态
            $status = $oWithdrawal->getAttribute('status');
            if ($status != Withdrawal::WITHDRAWAL_STATUS_TRANSFER_START) {

                if ($status == Withdrawal::WITHDRAWAL_STATUS_SUCCESS) {
                    echo "SUCCESS";
                    return "SUCCESS";
                }

                $return['error_msg'] = "status check failed!";
                $return['status']    = 0;
                self::echoCallBack($return);

                return $return;
            }

            $oWithdrawal->update([
                'status' => Withdrawal::WITHDRAWAL_STATUS_SUCCESS,
            ]);
            $return['status'] = 1;

        } elseif (in_array($aRequestData['respCode'], [9998, 0046, 0042])) {
            $oWithdrawal->transfer_failed();
            $return['status'] = 0;
        } else {
            $return['status']    = 2;
            $return['error_msg'] = "status no clear";
        }
        return $return;
    }

    /**
     * 输出Json数据
     *
     * @param array   $msg
     * @param boolean $fWithHeader
     */
    public static function echoCallBack($msg, $fWithHeader = true)
    {
        if ($fWithHeader) {
            header('Content-Type: application/json');
        }
        echo json_encode($msg);
    }

}
