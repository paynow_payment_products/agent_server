<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/10/3
 * Time: 16:16
 */

namespace Lib\Withdrawal;

use Carbon\Carbon;
use Core\Singleton;
use Withdrawal\QueryLogModel;
use Withdrawal\WithdrawalModel as Withdrawal;
use Curl\Curl;

class WithdrawalJX extends BasePlatform
{
    use Singleton;

    public $banks = [
    ];

    protected $_platform_order_num_key = "settleNo";
    protected $_order_num              = 'settleNo';
    protected $transfer_params         = true;

    public function transferParams($aRequestData)
    {
        return json_decode(file_get_contents("php://input"), true);
    }

    // 生成提交给提现平台的表单数据
    protected function createFormDataForWithdrawalRequest(Withdrawal $oWithdrawal)
    {
        $platform = $oWithdrawal->platform;    

        $data = [
            'merchNo'          => $platform->platform_account,  //商户编号
            'settleNo'         => $oWithdrawal->serial_number, //待付订单号
            'settleAmount'      => strval($oWithdrawal->amount*100),  //代付金额
            'notifyUrl'          => 'http://notify.minghui4.top/api/withdrawal/notify/platform_id/' . $oWithdrawal->platform_id, //商户通知地址
            'accIdentity'         => '51072519940405423X', //开户人身份证号
            'accType'      => "1",  //1对私，2对公
            'accName' => $oWithdrawal->card->name, //收款人姓名
            'accNo' => $oWithdrawal->card->card_no, //收款人账号
            'accPhone' => '15510180278', //开户人手机号
            'unionCode' => '305100001395', //清算行联行号
            'bankCode' => 'CMBC', //开户行代码
        ];
        $data['sign'] = $this->compileLoadSign($platform, $data);
        return $data;
    }

    // 签名
    public function compileLoadSign($platform, array $data)
    {
            $signPars = "";
            ksort($data);
            foreach($data as $k => $v) {
                if("" != $v && "sign" != $k) {
                    $signPars .= $k . "=" . $v . "&";
                }
            }
            $signPars = rtrim($signPars,'&');   
            $signPars .= $platform->platform_key;  

            $sign = MD5($signPars);
            return $sign;
    }

  //验证发通知服务器的IP
    public function verify($data,$oWithdrawal)
    {
        $data = json_decode($data,true);
        $signPars = "";
        ksort($data);
        foreach($data as $k => $v) {
            if("" != $v && "sign" != $k) {
                $signPars .= $k . "=" . $v . "&";
            }
        }
        $signPars = rtrim($signPars,'&');
        $signPars .= $oWithdrawal->platform->platform_key;
        $sign = MD5($signPars);
        if($data['sign'] == $sign){
            return true;
        }else{
            return false;
        }
      
    }

    public function verifySign($data, $oWithdrawal)
    {
        $signPars = "";
        ksort($data);
        foreach($data as $k => $v) {
            if("" != $v && "sign" != $k) {
                $signPars .= $k . "=" . $v . "&";
            }
        }
        $signPars = rtrim($signPars,'&');
        $signPars .= $oWithdrawal->platform->platform_key;   
        $sign = MD5($signPars);
        if($data['sign'] == $sign){
            return false;
        }else{
            return true;
        }
    }

    protected function sendRequest(Withdrawal $oWithdrawal, $aFormData)
    {

        $curl = new Curl();
        $curl->setOpt(CURLOPT_CONNECTTIMEOUT, 120);
        $curl->setOpt(CURLOPT_TIMEOUT, 120);
        $curl->setOpt(CURLOPT_HTTPHEADER, [
            "Content-Type:application/json;charset=UTF-8"
        ]);
        $curl->post($oWithdrawal->platform->request_url, json_encode($aFormData));
      
        $sResult = $curl->response;
        $verifyResult = $this->verify($sResult,$oWithdrawal);
        if ($verifyResult) {
            return json_decode($sResult,true);
        } else {
            return $sResult;
        }
    }

    // 处理发起提现接口返回数据
    protected function resolveResultFromWithdrawalRequest(Withdrawal $oWithdrawal, $aFormData, $aResult)
    {

        if (is_array($aResult) && key_exists('settleStatus', $aResult) && $aResult['settleStatus'] == 1) {

            if (!empty($aResult['orderNo'])) {
                $oWithdrawal->update([
                    'platform_order_no' => $aResult['orderNo'],
                ]);
            }
            return true;
        }
        
        $oWithdrawal->transfer_failed();
        return false;
    }

    protected function error($msg)
    {

    }

    protected function resolveWithdrawalInfoConfirm($data, Withdrawal $oWithdrawal)
    {

    }

    // 主动调用支付平台接口查询时，支付平台返回的数据处理成平台可以接受的状态
    public function cliWithdrawalQuery($oWithdrawal, $oQuery)
    {
        $data = [
            'merchNo' => $oWithdrawal->platform->platform_account,
            'requestId'   => $oWithdrawal->serial_number
        ];
        
        $strsign           = $this->compileLoadSign($oWithdrawal->platform, $data);
        $data['sign'] = $strsign;
        
        $curl = new Curl();
        $curl->setOpt(CURLOPT_CONNECTTIMEOUT, 120);
        $curl->setOpt(CURLOPT_TIMEOUT, 120);
        $curl->setOpt(CURLOPT_HTTPHEADER, [
            "Content-Type:application/json;charset=UTF-8"
        ]);
        $curl->post($oWithdrawal->platform->query_url, json_encode($data));
        $response = $curl->response;
        $verifyResult = $this->verify($response);
        if ($verifyResult) {
            $aResult = $this->formartResponse($response);
        } else {
            return $response;
        }

        $request_time = Carbon::now();
        $oQuery->queryLog()->save(
            new QueryLogModel([
                'request_url'   => $oWithdrawal->platform->query_url,
                'request_data'  => $data,
                'request_time'  => $request_time,
                'response_data' => $aResult,
                'response_time' => Carbon::now(),
            ])
        );

        if (is_array($aResult)) {

            if ($aResult['settleStatus'] == 2) {
                echo 'success';
                // 检查提现订单的状态
                $status = $oWithdrawal->getAttribute('status');
                if ($status != Withdrawal::WITHDRAWAL_STATUS_TRANSFER_START) {

                    if ($status == Withdrawal::WITHDRAWAL_STATUS_SUCCESS) {                        
                        return 1;
                    }
                    return 0;
                }

                $oWithdrawal->update([
                    'status' => Withdrawal::WITHDRAWAL_STATUS_SUCCESS,
                ]);               
                return 1;

            } else {
                echo 'fail';
                $oWithdrawal->transfer_failed();
                return 1;
            }
            return 0;
        }
        return 0;
    }

    // 处理根据首付款返回的通知数据 处理订单
    protected function resolveWithdrawalResult($aRequestData, Withdrawal $oWithdrawal)
    {      
        if (empty($oWithdrawal)) {
            $return['error_msg'] = "mownecum_order_num cannot find!";
            $return['status']    = 0;
            self::echoCallBack($return);

            return $return;
        }
       
        if ($this->verifySign($aRequestData, $oWithdrawal)) {
            $return['error_msg'] = "verify sign failed!!!";
            $return['status']    = 0;
            self::echoCallBack($return);
            return $return;
        }
        $company_order_num  = array_get($aRequestData, $this->_order_num);
        $mownecum_order_num = array_get($aRequestData, $this->_platform_order_num_key);
        $return             = array(
            "error_msg"          => "",
            "mownecum_order_num" => $mownecum_order_num,
            "company_order_num"  => $company_order_num,
            "status"             => "",
        );

        if ($aRequestData['settleStatus'] == 2) {
            // 检查提现订单的状态
            $status = $oWithdrawal->getAttribute('status');
            if ($status != Withdrawal::WITHDRAWAL_STATUS_TRANSFER_START) {

                if ($status == Withdrawal::WITHDRAWAL_STATUS_SUCCESS) {
                    echo "success";
                    return "SUCCESS";
                }

                $return['error_msg'] = "status check failed!";
                $return['status']    = 0;
                self::echoCallBack($return);
                echo "success";
                return $return;
            }

            $oWithdrawal->update([
                'status' => Withdrawal::WITHDRAWAL_STATUS_SUCCESS,
            ]);
            $return['status'] = 1;

        } else{
            echo "fail";
            $return['status'] = 2;
            $return['error_msg'] = "status no clear";
        }

        return $return;
    }

    /**
     * 输出Json数据
     *
     * @param array   $msg
     * @param boolean $fWithHeader
     */
    public static function echoCallBack($msg, $fWithHeader = true)
    {
        if ($fWithHeader) {
            header('Content-Type: application/json');
        }
        echo json_encode($msg);
    }

}
