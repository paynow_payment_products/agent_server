<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/10/3
 * Time: 16:16
 */

namespace Lib\Withdrawal;

use Carbon\Carbon;
use Core\Singleton;
use Withdrawal\QueryLogModel;
use Withdrawal\WithdrawalModel as Withdrawal;

class WithdrawalXFT extends BasePlatform
{
    use Singleton;

    public $banks = [
        '1'  => [
            'name' => '中国工商银行',
            'sign' => 'ICBC',
        ],
        '2'  => [
            'name' => '招商银行',
            'sign' => 'CMB',
        ],
        '3'  => [
            'name' => '中国建设银行',
            'sign' => 'CCB',
        ],
        '4'  => [
            'name' => '中国农业银行',
            'sign' => 'ABC',
        ],
        '5'  => [
            'name' => '中国银行',
            'sign' => 'BOC',
        ],
        '6'  => [
            'name' => '交通银行',
            'sign' => 'BOCM',
        ],
        '7'  => [
            'name' => '中国民生银行',
            'sign' => 'CMBC',
        ],
        '8'  => [
            'name' => '中信银行',
            'sign' => 'CITIC',
        ],
        '9'  => [
            'name' => '上海浦东发展银行',
            'sign' => 'SPDB',
        ],
        '10' => [
            'name' => '中国邮政储蓄银行',
            'sign' => 'PSBC',
        ],
        '11' => [
            'name' => '中国光大银行',
            'sign' => 'CEB',
        ],
        '12' => [
            'name' => '平安银行',
            'sign' => 'PAYH',
        ],
        '13' => [
            'name' => '广发银行',
            'sign' => 'CGB',
        ],
        '14' => [
            'name' => '华夏银行',
            'sign' => 'HXB',
        ],
        '15' => [
            'name' => '兴业银行',
            'sign' => 'CIB',
        ],
    ];

    protected $needSignColums = ['batchAmount', 'batchBiztype', 'batchContent', 'batchCount', 'batchDate', 'batchNo', 'batchVersion', 'charset', 'merchantId'];

    // protected $transfer_params = true;

    protected $_platform_order_num_key = "opeNo";
    protected $_order_num              = 'tradeNo';

    // 生成提交给提现平台的表单数据
    protected function createFormDataForWithdrawalRequest(Withdrawal $oWithdrawal)
    {

        $batchContent = "1,";
        $batchContent .= $oWithdrawal->card->card_no . ',';
        $batchContent .= empty($oWithdrawal->card->name) ? $oWithdrawal->user->name : $oWithdrawal->card->name . ",";
        $batchContent .= $this->banks[$oWithdrawal->card->bank_id]['name'] . ",";
        $batchContent .= "总行,{$oWithdrawal->card->branc_name},私,";
        $batchContent .= $oWithdrawal->amount . ",";
        $batchContent .= "CNY,{$oWithdrawal->card->province},{$oWithdrawal->card->city},{$oWithdrawal->user->mobile},,,," . $oWithdrawal->serial_number . ',OK';

        $data = [
            'batchAmount'  => $oWithdrawal->amount,
            'batchBiztype' => '00000',
            'batchContent' => $batchContent,
            'batchCount'   => 1,
            'batchDate'    => $oWithdrawal->created_at->format('Ymd'),
            'batchNo'      => $oWithdrawal->serial_number,
            'batchVersion' => '00',
            'charset'      => 'UTF-8',
            'merchantId'   => $oWithdrawal->platform->platform_account,
            'signType'     => "SHA",
        ];

        $strsign      = $this->compileLoadSign($oWithdrawal->platform, $data);
        $data['sign'] = $strsign;
        return $data;
    }

    // 签名
    public function compileLoadSign($platform, array $data)
    {
        ksort($data);
        $signstr = '';
        foreach ($data as $key => $value) {
            if ($value && in_array($key, $this->needSignColums)) {
                $signstr .= "{$key}={$value}&";
            }
        }
        $signstr = substr($signstr, 0, strlen($signstr) - 1);
        $signstr .= $platform->platform_key;
        // pr($signstr);
        // pr(strtoupper(sha1($signstr)));
        // return md5($signstr);
        return strtoupper(sha1($signstr));
    }

    // 发送数据
    protected function sendRequest(Withdrawal $oWithdrawal, $aFormData)
    {
        $request_url = $oWithdrawal->platform->request_url . $aFormData['merchantId'] . '-' . $aFormData['batchNo'];
        // pr($request_url);
        // pr($aFormData);

        $sResult = post($request_url, $aFormData);
        // $sResult = '{"respMessage":"","respCode":"S0001"}';

        // pr($sResult);exit;

        return json_decode($sResult, true);
    }

    // 获取请求数据 饼转化为一维数组
    protected function transferParams($aRequestData)
    {
    }

    // 发起提现接口结果处理
    protected function resolveResultFromWithdrawalRequest(Withdrawal $oWithdrawal, $aFormData, $aResult)
    {

        if ($aResult['respCode'] == 'S0001') {

            if (!empty($aResult['OPENO'])) {
                $oWithdrawal->update([
                    'platform_order_no' => $aResult['OPENO'],
                ]);
            }

            return true;
        }

        $oWithdrawal->transfer_failed();
        return false;
    }

    protected function error($msg)
    {

    }

    protected function resolveWithdrawalInfoConfirm($data, Withdrawal $oWithdrawal)
    {

    }

    /**
     * 查询代付订单状态
     * @param  [type] $oWithdrawal 提现订单
     * @param  [type] $oQuery      查询task
     * @return [type]              [description]
     */
    public function cliWithdrawalQuery($oWithdrawal, $oQuery)
    {
        $data = [
            'batchDate'    => $oWithdrawal->created_at->format('Ymd'),
            'batchNo'      => $oWithdrawal->serial_number,
            'batchVersion' => '00',
            'charset'      => 'UTF-8',
            'merchantId'   => $oWithdrawal->platform->platform_account,
            'signType'     => 'SHA',
        ];

        $strsign      = $this->compileLoadSign($oWithdrawal->platform, $data);
        $data['sign'] = $strsign;

        $query_url = $oWithdrawal->platform->query_url . $data['merchantId'] . '-' . $data['batchNo'];
        $query_url .= "?" . http_build_query($data);
        // pr($query_url);
        // pr($data);

        $request_time = Carbon::now();
        $sResult = json_decode(get($query_url), true);

        $oQuery->queryLog()->save(
            new QueryLogModel([
                'request_url'   => $query_url,
                'request_data'  => $data,
                'request_time'  => $request_time,
                'response_data' => $sResult,
                'response_time' => Carbon::now(),
            ])
        );

        if ( !empty($sResult) && $sResult['respCode'] == "S0001" ) {

            list($tradeNum, $tradeCustorder, $tradeCardnum, $tradeCardname, $tradeBranchbank, $tradeSubbranchban, $tradeAccountname, $tradeAccounttype, $tradeAmount, $tradeAmounttype, $tradeRemark, $contractUsercode, $tradeFeedbackcode, $tradeReason) = explode(',', $sResult['batchContent']);

            if ( $tradeFeedbackcode == "成功") {
                // 检查提现订单的状态
                $status = $oWithdrawal->getAttribute('status');
                if ($status != Withdrawal::WITHDRAWAL_STATUS_TRANSFER_START) {

                    if ($status == Withdrawal::WITHDRAWAL_STATUS_SUCCESS) {
                        return 1;
                    }

                    return 0;
                }

                $oWithdrawal->update([
                    'status' => Withdrawal::WITHDRAWAL_STATUS_SUCCESS,
                ]);

                return 1;

            }elseif ( $tradeFeedbackcode == '失败' ) {
                $oWithdrawal->transfer_failed();
                return 1;
            }

            return 0;
        }

        return 0;
    }

    // 处理根据首付款返回的通知数据 处理订单
    protected function resolveWithdrawalResult($aRequestData, Withdrawal $oWithdrawal)
    {

        if (empty($oWithdrawal)) {
            $return['error_msg'] = "mownecum_order_num cannot find!";
            $return['status']    = 0;
            self::echoCallBack($return);

            return $return;
        }

        if ($aRequestData['sign'] != strtoupper($this->compileLoadSign($oWithdrawal->platform, $aRequestData))) {

            $return['error_msg'] = "verify sign failed!!!";
            $return['status']    = 0;
            self::echoCallBack($return);

            return $return;
        }

        // 保存实际第三方订单号
        $oWithdrawal->update([
            'platform_order_no' => array_get($aRequestData, $this->_platform_order_num_key),
        ]);

        $company_order_num  = array_get($aRequestData, $this->_order_num);
        $mownecum_order_num = array_get($aRequestData, $this->_platform_order_num_key);
        $return             = array(
            "error_msg"          => "",
            "mownecum_order_num" => $mownecum_order_num,
            "company_order_num"  => $company_order_num,
            "status"             => "",
        );

        // 检查提现订单的状态
        $status = $oWithdrawal->getAttribute('status');
        if ($status != Withdrawal::WITHDRAWAL_STATUS_TRANSFER_START) {

            if ($status == Withdrawal::WITHDRAWAL_STATUS_SUCCESS) {
                echo "SUCCESS";
                return "SUCCESS";
            }

            $return['error_msg'] = "status check failed!";
            $return['status']    = 0;
            self::echoCallBack($return);

            return $return;
        }

        if ($aRequestData['status'] != "1") {
            $oWithdrawal->transfer_failed();
        } else {
            $oWithdrawal->update([
                'status' => Withdrawal::WITHDRAWAL_STATUS_SUCCESS,
            ]);
        }

        $return['status'] = 1;
        echo "SUCCESS";

        return $return;
    }

    /**
     * 输出Json数据
     *
     * @param array   $msg
     * @param boolean $fWithHeader
     */
    public static function echoCallBack($msg, $fWithHeader = true)
    {
        if ($fWithHeader) {
            header('Content-Type: application/json');
        }
        echo json_encode($msg);
    }

}
