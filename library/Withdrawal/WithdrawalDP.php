<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/12
 * Time: 14:08
 */

namespace Lib\Withdrawal;

use Withdrawal\WithdrawalModel as Withdrawal;
use Core\Singleton;

class WithdrawalDP extends BasePlatform
{
    use Singleton;
    /**
     * 提现请求
     *
     * @var int
     */
    const WITHDRAW_REQUEST = 5;

    /**
     * 提现状态确认
     *
     * @var int
     */
    const WITHDRAW_APPROVE = 6;

    /**
     * 提现结果确认
     *
     * @var int
     */
    const WITHDRAW_RESULT_APPROVE = 7;

    // 平台侧订单号的键名
    protected $_platform_order_num_key = 'mownecum_order_num';

    protected function createFormDataForWithdrawalRequest(Withdrawal $oWithdrawal)
    {
        $postData                      = array();
        $postData['bank_id']           = $oWithdrawal->card->bank_id;
        $postData['company_id']        = $oWithdrawal->platform->platform_account;
        $postData['amount']            = $oWithdrawal->amount;
        $postData['card_num']          = $oWithdrawal->card->card_no;
        $postData['card_name']         = $oWithdrawal->card->name;
        $postData['company_user']      = $oWithdrawal->user->user_id;
        $postData['company_order_num'] = $oWithdrawal->serial_number;
        $postData['key']               = $this->_getKey($postData, self::WITHDRAW_REQUEST,
            $oWithdrawal->platform->platform_key);

        return $postData;
    }

    protected function resolveResultFromWithdrawalRequest(Withdrawal $oWithdrawal, $aFormData, $aResult)
    {
        // DP接口故障
        if (empty($aResult['status']) || $aResult['status'] != 1 || ! isset($aResult[$this->_platform_order_num_key]) || $aResult[$this->_platform_order_num_key] == '') {
            $oWithdrawal->transfer_failed();

            return false;
        }

        return true;
    }

    /**
     * 供控制器调用，实现DP的信息确认
     *
     * @param                    $data
     * @param Withdrawal         $oWithdrawal
     *
     * @return array
     */
    public function resolveWithdrawalInfoConfirm($data, Withdrawal $oWithdrawal)
    {
        $mownecum_order_num = array_get($data, $this->_platform_order_num_key);
        $company_order_num  = array_get($data, 'company_order_num');

        //处理异常情况
        if (empty($oWithdrawal)) {
            $return_status = 5;
            $msg           = "cannot find this record";
            $result        = [
                "error_msg"          => $msg,
                "mownecum_order_num" => $mownecum_order_num,
                "company_order_num"  => $company_order_num,
                "status"             => $return_status,
            ];
            self::echoCallBack($result);

            return $result;
        }

        $msg = '';

        $status = $oWithdrawal->getAttribute('status');

        switch ($status) {
            case Withdrawal::WITHDRAWAL_STATUS_TRANSFER_START:
                $return_status = 4;
                break;
            case Withdrawal::WITHDRAWAL_STATUS_SUCCESS:
                $return_status = 1;
                break;
            case Withdrawal::WITHDRAWAL_STATUS_TRANSFER_FAILED:
                $return_status = 0;
                break;
            default:
                $return_status = 5;
                $msg           = 'error status';
                break;
        }

        // check key
        $key          = array_get($data, 'key');
        $i_origin_key = self::_getKey($data, self::WITHDRAW_APPROVE, $oWithdrawal->platform->platform_key);
        //密钥不匹配
        if ($key != $i_origin_key) {
            $return_status = 9;
            $msg           = "error key is not compare!";
        }

        // check amount
        $amount = array_get($data, 'amount');
        if ($amount != $oWithdrawal->getAttribute('amount')) {
            $return_status = 0;
            $msg           = "Amount is not compare!";
        }

        $result = array(
            "error_msg"          => $msg,
            "mownecum_order_num" => $mownecum_order_num,
            "company_order_num"  => $company_order_num,
            "status"             => $return_status,
        );
        self::echoCallBack($result);

        return $result;
    }

    /**
     * 供控制器调用，实现DP的提现结果信息推送
     *
     * @param                    $data
     * @param Withdrawal         $oWithdrawal
     *
     * @return array
     */
    public function resolveWithdrawalResult($data, Withdrawal $oWithdrawal)
    {
        $company_order_num  = array_get($data, 'company_order_num');
        $mownecum_order_num = array_get($data, 'mownecum_order_num');
        $return             = array(
            "error_msg"          => "",
            "mownecum_order_num" => $mownecum_order_num,
            "company_order_num"  => $company_order_num,
            "status"             => "",
        );

        if (empty($oWithdrawal)) {
            $return['error_msg'] = "mownecum_order_num cannot find!";
            $return['status']    = 0;
            self::echoCallBack($return);

            return $return;
        }

        $status = $oWithdrawal->getAttribute('status');
        if (!in_array($status, [$oWithdrawal::WITHDRAWAL_STATUS_TRANSFER_START, $oWithdrawal::WITHDRAWAL_STATUS_TRANSFER_EXCEPTION])) {
            $return['error_msg'] = "status check failed!";
            $return['status']    = 0;
            self::echoCallBack($return);

            return $return;
        }

        // check key
        $key          = array_get($data, 'key');
        $i_origin_key = self::_getKey($data, self::WITHDRAW_RESULT_APPROVE, $oWithdrawal->platform->platform_key);
        if ($key != $i_origin_key) {
            $return['error_msg'] = "error key is not compare!";
            $return['status']    = 0;
            self::echoCallBack($return);

            return $return;
        }

        // check amount
        $mc_amount = array_get($data, 'amount', 0);
        $amount    = $oWithdrawal->getAttribute('amount');
        if ($mc_amount > $amount) {
            $return['error_msg'] = "Mc return amount bigger than apply amount!";
            $return['status']    = 0;
            self::echoCallBack($return);

            return $return;
        }

        $mc_status = array_get($data, 'status');
        if ( ! in_array($mc_status, array(1, 2, 0))) {
            $return['error_msg'] = "invaild status";
            $return['status']    = 0;
            self::echoCallBack($return);

            return $return;
        }

        if ($mc_status == 0) {
            $oWithdrawal->update([
                'status' => Withdrawal::WITHDRAWAL_STATUS_TRANSFER_FAILED,
            ]);
            $oWithdrawal->transfer_failed();
        }

        // 处理成功，部分成功，未处理
        if ($mc_status == 1) {
            $oWithdrawal->update([
                'status' => Withdrawal::WITHDRAWAL_STATUS_SUCCESS,
            ]);
        }

        $return['status'] = 1;
        self::echoCallBack($return);

        return $return;
    }

    /**
     * 生成通讯密钥
     *
     * @param array $aPostData 通信数据包
     * @param int   $iType 通信类型
     *
     * @return string | FALSE
     */
    private static function _getKey($aPostData, $iType, $sConfigStr)
    {
        if ( ! is_array($aPostData)) {
            return false;
        }
        switch ($iType) {
            case self::WITHDRAW_REQUEST: //提现申请
                $sDataStr = array_get($aPostData, 'company_id') . array_get($aPostData,
                        'bank_id') . array_get($aPostData, 'company_order_num') . array_get($aPostData, 'amount')
                            . array_get($aPostData, 'card_num') . array_get($aPostData,
                        'card_name') . array_get($aPostData, 'company_user') . array_get($aPostData, 'issue_bank_name')
                            . array_get($aPostData, 'issue_bank_address') . array_get($aPostData, 'memo');
                break;
            case self::WITHDRAW_APPROVE: //提现信息确认
                $sDataStr = array_get($aPostData, 'company_order_num') . array_get($aPostData,
                        'mownecum_order_num') . array_get($aPostData, 'amount')
                            . array_get($aPostData, 'card_num') . array_get($aPostData,
                        'card_name') . array_get($aPostData, 'company_user');
                break;
            case self::WITHDRAW_RESULT_APPROVE: //提现结果确认
                $sDataStr = array_get($aPostData, 'mownecum_order_num') . array_get($aPostData,
                        'company_order_num') . array_get($aPostData, 'status')
                            . array_get($aPostData, 'amount') . array_get($aPostData, 'exact_transaction_charge');
                break;
            default :
                return false;
        }

        return md5(md5($sConfigStr) . $sDataStr);
    }

    /**
     * 输出Json数据
     *
     * @param array   $msg
     * @param boolean $fWithHeader
     */
    public static function echoCallBack($msg, $fWithHeader = true)
    {
        if ($fWithHeader) {
            header('Content-Type: application/json');
        }
        echo json_encode($msg);
    }

    public function error($msg)
    {
        header('Content-Type: application/json');
        echo json_encode([
            "error_msg"          => $msg,
            "mownecum_order_num" => '',
            "company_order_num"  => '',
            "status"             => 0,
        ]);
    }

}
