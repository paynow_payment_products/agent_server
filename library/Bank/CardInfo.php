<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/3
 * Time: 19:51
 */

namespace Lib\Bank;

use Core\Singleton;
use Curl\Curl;

class CardInfo
{
    use Singleton;

    public function getBankName($card_no)
    {
        $request = [
            '_input_charset' => 'utf-8',
            'cardBinCheck'   => 'true',
            'cardNo'         => $card_no,
        ];

        $return = $this->get('https://ccdcapi.alipay.com/validateAndCacheCardInfo.json', $request);
        if (empty($return)) {
            return '';
        }

        $return = json_decode($return, true);
        if (empty($return) || empty($return['validated']) || ! $return['validated']) {
            return '';
        }

        if (empty($this->_banks[$return['bank']])) {
            return '';
        }

        return $this->_banks[$return['bank']];
    }

    private $_curl;
    private $_banks;

    public function __construct()
    {
        $this->_curl = new Curl();

        $this->_banks = json_decode('{
            "ICBC": "中国工商银行",
            "CMB": "招商银行",
            "CCB": "中国建设银行",
            "ABC": "中国农业银行",
            "BOC": "中国银行",
            "COMM": "交通银行",
            "CMBC": "中国民生银行",
            "CITIC": "中信银行",
            "SPDB": "上海浦东发展银行",
            "PSBC": "中国邮政储蓄银行",
            "CEB": "中国光大银行",
            "SPABANK": "平安银行",
            "GDB": "广东发展银行",
            "HXBANK": "华夏银行",
            "CIB": "兴业银行"
        }', true);
    }

    public function __destruct()
    {
        $this->_curl->close();
    }

    public function get($url, $params = [])
    {
        if (is_array($params) && ! empty($params)) {
            $prefix = '?';
            if (strpos($url, '?') !== false) {
                $prefix = '&';
            }
            $url .= $prefix;
            $url .= http_build_query($params);
        }

//        return file_get_contents($url);

        $this->_curl->get($url);

        return $this->_curl->response;
    }

}