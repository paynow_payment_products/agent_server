<?php

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/15
 * Time: 18:45
 */

use SmsCenter\Tools as Smstools;
use User\UserModel;
use \Illuminate\Pagination\Paginator;

abstract class BaseAction extends \Core\Action
{
    public $pageSize;

    /**
     * @return \User\UserModel
     * @throws Exception
     */
    public function checkLogin()
    {
        $session = $this->session;

        $user_id = empty($session->user_id) ? '' : $session->user_id;

        if (empty($user_id)) {
            throw new Exception('not login');
        }

        $user = UserModel::find($user_id);

        if (empty($user)) {
            throw new Exception('invalid user');
        }

        return $user;
    }

    public function checkFundPin()
    {
        $user     = $this->checkLogin();
        $fund_pin = aes_decrypt(Request::get('fund_pin'));

        $result = $user->checkFundPin($fund_pin);
        if ($result) {
            return true;
        }
        $result1 = $user->checkFundPin(Request::get('fund_pin'));
        if ($result1) {
            return true;
        }
        throw new Exception('资金密码错误');
    }

    public function checkCaptcha()
    {
        $session = $this->session;

        if (! ($captcha = $session->captcha) || empty($captcha) || $captcha['created'] < (time() - 120)) {
            throw new Exception('验证码已过期');
        }
        if (strtolower($captcha['phrase']) !== strtolower(Request::get('captcha', ''))) {
            throw new Exception('请检查验证码');
        }
        $session->captcha = [];
    }

    public function checkVcode()
    {
        $mobile = aes_decrypt(Request::get('mobile'));
        $vcode  = aes_decrypt(Request::get('vcode'));
        if (empty($mobile)) {
            $user = $this->checkLogin();
            if (empty($user->mobile)) {
                throw new Exception('您还没有绑定手机号');
            }
            $mobile = $user->mobile;
        }
        if (!SmsTools::checkVcode($mobile, $vcode)) {
            throw new Exception('请检查手机验证码');
        }

    }

    public function _initPageConf()
    {
        $pageSize = Request::getQuery('pageSize');

        if (filter_var($pageSize, FILTER_VALIDATE_INT) !== false && (int) $pageSize >= 1 && (int) $pageSize <= 100) {
            $this->pageSize = $pageSize;
        } else {
            $this->pageSize = 15;
        }

        Paginator::currentPageResolver(function () {
            $page = Request::getQuery('page');

            if (filter_var($page, FILTER_VALIDATE_INT) !== false && (int) $page >= 1) {
                return $page;
            }

            return 1;
        });
    }
}
