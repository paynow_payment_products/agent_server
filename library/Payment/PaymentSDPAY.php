<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/8/16
 * Time: 17:06
 */

namespace Lib\Payment;

use Core\Singleton;
use Deposit\DepositModel;
use Deposit\OrderModel as OrderModel;
use Deposit\PlatformModel;
use Exception;
use Request;
use SimpleXMLElement;

class PaymentSDPAY extends BasePlatform
{
    use Singleton;

    public function compileLoadInputData(DepositModel $deposit)
    {
        $platform = $deposit->platform;
        return [
            'cmd'        => array_get($platform->custom_params, 'cmd', '6006'),
            'merchantid' => $platform->account,
            'language'   => 'zh-cn',
            'userinfo'   => [
                'order'    => $deposit->order_no,
                'username' => $deposit->user->user_id,
                'money'    => $deposit->amount,
                'unit'     => 1,
                'time'     => date('Y-m-d H:i:s'),
                'remark'   => 'deposit points ' . $deposit->amount,
                'backurl'  => $platform->notify_url . $platform->platform_id,
            ],
        ];
    }

    /**
     * @param PlatformModel $platform
     * @param array         $data
     *
     * @return mixed
     */
    public function compileLoadSign(PlatformModel $platform, array $data)
    {
    }

    public function compileLoadUrl(PlatformModel $platform, array $data, &$aResponse = null)
    {
        $aResponse                 = $data;
        $formData['___DepositUrl'] = $platform->load_url;
        $formData['cmd']           = $data['cmd'];
        $formData['pid']           = $data['merchantid'];

        $str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
                <message>
                  <cmd>" . $data['cmd'] . "</cmd>
                  <merchantid>" . $data['merchantid'] . "</merchantid>
                  <language>" . $data['language'] . "</language>
                  <userinfo>
                    <order>" . $data['userinfo']['order'] . "</order>
                    <username>" . $data['userinfo']['username'] . "</username>
                    <money>" . $data['userinfo']['money'] . "</money>
                    <unit>" . $data['userinfo']['unit'] . "</unit>
                    <time>" . $data['userinfo']['time'] . "</time>
                    <remark>" . $data['userinfo']['remark'] . "</remark>
                    <backurl>" . $data['userinfo']['backurl'] . "</backurl>
                    <backurlbrowser>" .SITE_URL. "</backurlbrowser>
                  </userinfo>
                </message>";
        list($key1, $key2, $key3) = explode('|', $platform->key);
        $myencrypt                = new DesEncrypt($key1, $key2);
        $md5Str                   = md5($str . $key3);
        $tempStr                  = $str . $md5Str;
        $formData['des']          = $myencrypt->encryptData($tempStr);
        return $platform->relay_load_url . '?' . http_build_query($formData);
    }

    public function saveLoadOrder(
        DepositModel $deposit,
        array $data,
        array $response,
        $loadUrl
    ) {
        $order                = new OrderModel();
        $order->deposit_id    = $deposit->deposit_id;
        $order->load_url      = $loadUrl;
        $order->load_request  = $data;
        $order->load_response = $response;
        $order->save();
        $deposit->order_id = $order->order_id;
        $deposit->save();
    }

    public $transfer_params = true;
    protected function transferParams()
    {
        $platformId = Request::get('platform_id');
        $platform   = PlatformModel::find($platformId);
        if (empty($platform)) {
            throw new Exception('invalid platform');
        }
        list($key1, $key2, $key3) = explode('|', $platform->key);
        $res                      = Request::get('res');
        $dec                      = new DesEncrypt($key1, $key2);
        $decryptStr               = $dec->decryptData($res);
        $xml                      = substr($decryptStr, 0, strlen($decryptStr) - 32);
        $sign                     = substr($decryptStr, -32, strlen($decryptStr));
        $params                   = (array) new SimpleXMLElement($xml);
        $params['sign']           = $sign === md5($xml . $key3);

        return $params;
    }

    public $signColumn           = 'sign';
    public $successColumn        = 'result';
    public $successValue         = '1';
    public $successMsg           = '1';
    public $orderNoColumn        = 'order';
    public $amountColumn         = 'money';
    public $paymentOrderNoColumn = 'order';
    protected function compileNotifySign(PlatformModel $platform, array $params)
    {
        return true;
    }

    protected function returnNotifySuccess(array $params)
    {
        $responseStr = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" .
            "<message>" .
            "<cmd>60071</cmd>" .
            "<merchantid>" . $params['merchantid'] . "</merchantid>" .
            "<order></order>" .
            "<username></username>" .
            "<result>100</result>" .
            "</message>";
        exit($responseStr);
    }

    public function returnNotifyError(array $params, $errMsg)
    {
        vd($errMsg);exit;
    }
}

/**
 * DES加密类
 * User: gaowei
 * Date: 2017/12/12
 * Time: 19:23
 */
class DesEncrypt
{
    private $key = "";
    private $iv  = "";

    /**
     * 构造，传递二个已经进行base64_encode的KEY与IV
     *
     * @param string $key
     * @param string $iv
     */
    public function __construct($key, $iv)
    {
        if (empty($key) || empty($iv)) {
            echo 'key and iv is not valid';
            exit();
        }
        $this->key = $key;
        $this->iv  = $iv;

    }

    public function encryptData($value)
    {
        $md5hash = md5($this->GetMac() . date("Y-m-d h:m:s"));
        $value   = $value . $md5hash;
        return $this->encrypt($value);
    }

    public function decryptData($value)
    {
        $des = $this->decrypt($value);
        return substr($des, 0, strlen($des) - 32);
    }

    public function GetMd5Hash($input)
    {
        return sha1($input);
    }

    public function GetMac()
    {
        return date("Y-m-d h:m:s") . rand();
    }

    /**
     * @title 加密
     * @author gaowei
     * @date 2017/12/18
     * @param string $value 要传的参数
     * @ //OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING //AES-128-ECB|AES-256-CBC|BF-CBC
     * @return json
     * */
    public function encrypt($value)
    {

        //参考地址：https://stackoverflow.com/questions/41181905/php-mcrypt-encrypt-to-openssl-encrypt-and-openssl-zero-padding-problems#
        $value = $this->PaddingPKCS7($value);
        $key   = base64_decode($this->key);
        $iv    = base64_decode($this->iv);
        //AES-128-ECB|不能用 AES-256-CBC|16 AES-128-CBC|16 BF-CBC|8 aes-128-gcm|需要加$tag  DES-EDE3-CBC|8
        $cipher = "DES-EDE3-CBC";
        if (in_array($cipher, openssl_get_cipher_methods())) {
            //$ivlen = openssl_cipher_iv_length($cipher);
            // $iv = openssl_random_pseudo_bytes($ivlen);
            $result = openssl_encrypt($value, $cipher, $key, OPENSSL_SSLV23_PADDING, $iv);
            //$result = base64_encode($result); //为3的时间要用
            //store $cipher, $iv, and $tag for decryption later
            /* $original_plaintext = openssl_decrypt($result, $cipher, $key, OPENSSL_SSLV23_PADDING, $iv);
        echo $original_plaintext."\n";*/
        }
        return $result;
    }

    /**
     * @title 解密
     * @author gaowei
     * @date 2017/12/18
     * @param string $value 要传的参数
     * @return json
     * */
    public function decrypt($value)
    {
        $key       = base64_decode($this->key);
        $iv        = base64_decode($this->iv);
        $decrypted = openssl_decrypt($value, 'DES-EDE3-CBC', $key, OPENSSL_SSLV23_PADDING, $iv);
        $ret       = $this->UnPaddingPKCS7($decrypted);
        return $ret;
    }

    private function PaddingPKCS7($data)
    {
        //$block_size = mcrypt_get_block_size('tripledes', 'cbc');//获取长度
        //$block_size = openssl_cipher_iv_length('tripledes', 'cbc');//获取长度
        $block_size   = 8;
        $padding_char = $block_size - (strlen($data) % $block_size);
        $data .= str_repeat(chr($padding_char), $padding_char);
        return $data;
    }
    private function UnPaddingPKCS7($text)
    {
        $pad = ord($text{strlen($text) - 1});
        if ($pad > strlen($text)) {
            return false;
        }
        if (strspn($text, chr($pad), strlen($text) - $pad) != $pad) {
            return false;
        }
        return substr($text, 0, -1 * $pad);
    }
}
