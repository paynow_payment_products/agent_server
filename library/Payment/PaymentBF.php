<?php

namespace Lib\Payment;

use Core\Singleton;
use Deposit\DepositModel as DepositModel;
use Deposit\OrderModel as OrderModel;
use Deposit\PlatformModel as PlatformModel;

/**
 * 自由付
 */
class PaymentBF extends BasePlatform
{
    use Singleton;

    // 私钥路径
    const prv_key_fileName = MAIN_PATH . '/library/Payment/FB_pem/102018042400000008_prv.pem';
    // 公钥路径
    const pub_key_fileName = MAIN_PATH . '/library/Payment/FB_pem/102018042400000008_pub.pem';

    public $successMsg = 'SUCCESS';

    // protected $test = true;
    protected $transfer_params = true;

    // 返回数据中的 自身平台订单编号字段
    protected $orderNoColumn = 'orderNo';
    // 点三放支付订单编号
    protected $paymentOrderNoColumn = "orderNo";

    protected $amountColumn = "transAmt";

    protected $successColumn = "respCode";
    protected $successValue  = '0000';

    protected $signColumn = 'signature';

    protected function transferParams()
    {
        return $this->formartResponse($_POST['params']);
    }

    protected function compileNotifySign(PlatformModel $platform, array $params)
    {
        return '';
    }

    /**
     * 生成创建订单数据
     * @param  DepositModel $deposit [description]
     * @return [type]                [description]
     */
    public function compileLoadInputData(DepositModel $deposit)
    {
        $platform = $deposit->platform;

        $data = [
            'requestNo'     => $deposit->order_no,
            'version'       => 'V1.0',
            'productId'     => array_get($platform->custom_params, 'productId', "1020"),
            'transId'       => '01',
            'merNo'         => $platform->account,
            'orderDate'     => $deposit->created_at->format('Ymd'),
            'orderNo'       => $deposit->order_no,
            'returnUrl'     => SITE_URL,
            'notifyUrl'     => $platform->notify_url . $platform->platform_id,
            'transAmt'      => $deposit->amount * 100,
            'commodityName' => "奖品兑换",
            'usrSyt'        => 1,
            'terminalType'  => 1,
        ];

        $data['signature'] = $this->compileLoadSign($platform, $data);
        return $data;
    }

    public function formatParams($params)
    {
        $data = array();
        // save as key=>key=value
        foreach ($params as $key => $value) {
            if ($key != $this->signColumn) {
                $data[$key] = $key . '=' . $value;
            }
        }
        //  Sort an array by key asc
        ksort($data);
        // Join array elements with  &
        return implode('&', $data);
    }

    public function formartResponse($result)
    {
        $response = [];
        foreach (explode("&", $result) as $value) {
            $tmp               = explode("=", $value);
            $response[$tmp[0]] = $tmp[1];
        }
        return $response;
    }

    public function verify($params)
    {
        // separate with &
        $paramsKVArray = explode("&", $params);
        $paramsArray   = array();
        foreach ($paramsKVArray as $key => $value) {
            // separate with = and save key-value in array
            $temp = explode("=", $value);
            if ('signature' == $temp[0]) {
                // get signature
                $signature = $temp[1];
                continue;
            }
            // convert to array
            $paramsArray[$temp[0]] = $temp[1];
        }
        // join params
        $data = $this->formatParams($paramsArray);
        // verify
        return $this->verifySign($data, $signature, null);
    }

    public function verifySign($data, $signature, $dataType)
    {
        if ('array' == $dataType) {
            // convert array to string
            $data = $this->formatParams($data);
        }
        // read public key content
        $pkcs12 = file_get_contents(self::pub_key_fileName);
        // get public key id
        $pubkeyid = openssl_get_publickey($pkcs12);
        if (!is_resource($pubkeyid)) {
            return false;
        }
        // verify sign
        $signature    = base64_decode($signature);
        $verifyResult = openssl_verify($data, $signature, $pubkeyid);
        if (1 == $verifyResult) {
            // success and Free key resource
            openssl_free_key($pubkeyid);
            return true;
        }
        return false;
    }

    public function CURL($url, $data)
    {
        // 启动一个CURL会话
        $ch = curl_init();
        // 设置curl允许执行的最长秒数
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        // 忽略证书
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //  获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        // 发送一个常规的POST请求。
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        // 是否需要头部信息（否）
        curl_setopt($ch, CURLOPT_HEADER, 0);
        // 执行操作
        $result = curl_exec($ch);
        // 返回数据
        return $result;
    }

    // 签名
    public function compileLoadSign(PlatformModel $platform, array $data)
    {
        $signstr = $this->formatParams($data);

        $pkcs12 = file_get_contents(self::prv_key_fileName);
        $pKey   = openssl_pkey_get_private($pkcs12, $platform->key);
        openssl_sign($signstr, $sign, $pKey);
        return base64_encode($sign);
    }

    public function compileLoadUrl(PlatformModel $platform, array $data, &$response = null)
    {
        $result = $this->CURL($platform->load_url, $data);
        // pr($result);

        $verifyResult = $this->verify($result);
        if ($verifyResult) {
            // echo "签名校验：","签名校验成功...";
            $response = $this->formartResponse($result);
            // pr($response);
            if (empty($response) || empty($response['payUrl'])) {
                return '';
            }
            return urldecode($response['payUrl']);
        } else {
            // echo "签名校验：","签名校验失败...";
            return '';
        }
    }

    public function saveLoadOrder(
        DepositModel $deposit,
        array $data,
        array $response,
        $loadUrl
    ) {
        $order                 = new OrderModel();
        $order->deposit_id     = $deposit->deposit_id;
        $order->third_order_no = array_get($response, $this->paymentOrderNoColumn);
        $order->load_url       = $loadUrl;
        $order->load_request   = $data;
        $order->load_response  = $response;
        $order->save();
        $deposit->order_id = $order->order_id;
        $deposit->save();
    }

    protected function checkNotifySign($platform, $params)
    {
        return $this->verifySign($params, $params[$this->signColumn], 'array');
    }

    protected function getPayAmount($params)
    {
        return $params[$this->amountColumn] / 100;
    }

    protected function returnNotifySuccess(array $params)
    {
        $aResponse = [
            'company_order_num'  => array_get($params, $this->orderNoColumn),
            'mownecum_order_num' => array_get($params, $this->paymentOrderNoColumn),
            'status'             => 1,
            'msg'                => $this->successMsg,
        ];
        exit("SUCCESS");
    }

    public function returnNotifyError(array $params, $errMsg)
    {
        $aResponse = [
            'company_order_num'  => array_get($params, $this->orderNoColumn),
            'mownecum_order_num' => array_get($params, $this->paymentOrderNoColumn),
            'status'             => 0,
            'error_msg'          => $errMsg,
        ];
        exit(json_encode($aResponse));
    }

    public function makeQueryData(DepositModel $deposit)
    {
        $custom_params = $deposit->platform->custom_params;

        $data = [
            'requestNo' => md5($deposit->order_no . time()),
            'version'   => 'V1.0',
            'transId'   => '03',
            'merNo'     => $deposit->platform->account,
            'orderDate' => $deposit->created_at->format('Ymd'),
            'orderNo'   => $deposit->order_no,
        ];

        $data['signature'] = $this->compileQuerySign($deposit->platform, $data);
        return $data;
    }

    public function compileQuerySign(PlatformModel $platform, array $data)
    {
        return $this->compileLoadSign($platform, $data);
    }

    public function getQueryResponse(array $data, DepositModel $deposit, &$response = null)
    {
        $result = $this->CURL($deposit->platform->query_url, $data);
        // pr($result);
        $verifyResult = $this->verify($result);
        if ($verifyResult) {
            // echo "签名校验：","签名校验成功...";
            $response = $this->formartResponse($result);
            // pr($response);
            if ($response['respCode'] == "0000" && $response['origRespCode'] == "0000") {

                $deposit->order->update([
                    'third_order_no'     => $response['orderNo'],
                    'third_order_status' => $response['origRespCode'],
                ]);

                if (!$bSucc = $deposit->setWaitingLoad()) {
                    return 0;
                }

                // add notice task
                $res = $deposit->addDepositTask();

                if ($res) {
                    return 1;
                } else {
                    return 0;
                }
            }
        } else {
            // echo "签名校验：","签名校验失败...";
            return 0;
        }
        return 0;
    }
}
