<?php

namespace Lib\Payment;

use Core\Singleton;
use Deposit\DepositModel;
use Deposit\OrderModel as OrderModel;
use Deposit\PlatformModel;

class PaymentPNXPAY extends BasePlatform
{
    use Singleton;

    public function compileLoadInputData(DepositModel $deposit)
    {
        $data = [
            'app_id'         => $deposit->platform->account,
            'app_order_no'   => $deposit->order_no,
            'app_order_time' => $deposit->created_at->toDateTimeString(),
            'app_user'       => $deposit->user_id,
            'type_id'        => array_get($deposit->platform->custom_params, 'type_id'),
            'platform_id'    => array_get($deposit->platform->custom_params, 'platform_id'),
            'amount'         => $deposit->amount * 100,
            'notify_url'     => $deposit->platform->notify_url . $deposit->platform_id,
            'client_ip'      => $deposit->client_ip,
        ];

        $data['sign'] = $this->compileLoadSign($deposit->platform, $data);

        return $data;
    }

    /**
     * @param PlatformModel $platform
     * @param array         $data
     *
     * @return mixed
     */
    public function compileLoadSign(PlatformModel $platform, array $data)
    {
        $tip   = '';
        $words = '';
        ksort($data);
        foreach ($data as $key => $value) {
            if ($key == 'sign' || empty($value)) {
                continue;
            }
            $words .= $tip . $key . '=' . $value;
            $tip = '&';
        }
        $words .= $platform->key;

        return strtolower(md5($words));
    }

    public function compileLoadUrl(PlatformModel $platform, array $data, &$response = null)
    {
        $response = json_decode(post($platform->load_url, $data), true);

        return $response['pay_url'] ?: '';
    }

    public function saveLoadOrder(
        DepositModel $deposit,
        array $data,
        array $response,
        $loadUrl
    ) {
        $order                 = new OrderModel();
        $order->deposit_id     = $deposit->deposit_id;
        $order->third_order_no = array_get($response, 'order_no');
        $order->load_url       = $loadUrl;
        $order->load_request   = $data;
        $order->load_response  = $response;
        $order->save();
        $deposit->order_id = $order->order_id;
        $deposit->save();
    }

    protected $needSignColums = [
        'app_id',
        'success',
        'app_order_no',
        'order_no',
        'amount',
        'pay_amount',
        'pay_time',
        'note',
        'message',
    ];
    protected function compileNotifySign(PlatformModel $platform, array $params)
    {
        $tip   = '';
        $words = '';
        ksort($params);
        foreach ($params as $key => $value) {
            if ($key == 'sign' || empty($value) || !in_array($key, $this->needSignColums)) {
                continue;
            }
            $words .= $tip . $key . '=' . $value;
            $tip = '&';
        }
        $words .= $platform->key;

        return strtolower(md5($words));
    }

    public $signColumn           = 'sign';
    public $successColumn        = "success";
    public $successValue         = '1';
    public $successMsg           = 'SUCCESS';
    public $orderNoColumn        = 'app_order_no';
    public $amountColumn         = 'pay_amount';
    public $paymentOrderNoColumn = 'order_no';

    protected function returnNotifySuccess(array $params)
    {
        exit('SUCCESS');
    }

    public function returnNotifyError(array $params, $errMsg)
    {
        $aResponse = [
            'company_order_num'  => array_get($params, $this->orderNoColumn),
            'mownecum_order_num' => array_get($params, $this->paymentOrderNoColumn),
            'status'             => 0,
            'error_msg'          => $errMsg,
        ];
        exit(json_encode($aResponse));
    }

    protected function getPayAmount($params)
    {
        return amount_format($params[$this->amountColumn] / 100);
    }
}
