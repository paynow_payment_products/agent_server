<?php

namespace Lib\Payment;

use Deposit\DepositModel as DepositModel;
use Deposit\LogModel as LogModel;
use Deposit\PlatformModel as PlatformModel;
use Exception;

abstract class BasePlatform
{
    public $banks = [];

    /**
     * @param DepositModel  $deposit
     *
     * @return array
     */
    abstract public function compileLoadInputData(DepositModel $deposit);

    /**
     * @param PlatformModel $platform
     * @param array         $data
     *
     * @return mixed
     */
    abstract public function compileLoadSign(PlatformModel $platform, array $data);

    protected $goStraight = true;

    abstract public function compileLoadUrl(PlatformModel $platform, array $data, &$aResponse = null);

    abstract public function saveLoadOrder(
        DepositModel $deposit,
        array $data,
        array $response,
        $loadUrl
    );

    abstract protected function compileNotifySign(PlatformModel $platform, array $params);
    abstract protected function returnNotifySuccess(array $params);

    protected $signColumn;
    protected $orderNoColumn;
    protected $amountColumn;
    protected $paymentOrderNoColumn;

    protected $test = false;

    protected function mkTestData()
    {
        return [];
    }

    protected $transfer_params = false;

    protected function transferParams()
    {
        return [];
    }

    public function actionDepositNotify(array $params, PlatformModel $platform)
    {
        if ($this->test) {
            $params = $this->mkTestData();
        }
        if ($this->transfer_params) {
            $params = $this->transferParams();
        }
        // if (key_exists('msg', $params)) {
        //     unset($params['msg']);
        // }
        if (empty($params)) {
            throw new Exception('invalid callback');
        }

        /**
         * @var LogModel $depositLog
         */
        $depositLog = LogModel::create([
            'status'  => LogModel::STATUS_NEW,
            'request' => $params,
            'ip'      => get_client_ip(),
        ]);

        if (!$this->checkNotifySign($platform, $params)) {
            $depositLog->update(['status' => LogModel::STATUS_SIGN_ERROR]);
            throw new Exception('sign error');
        }

        //TODO check ip

        // check order number
        if (!$this->checkSuccessFlag($params)) {
            $depositLog->update(['status' => LogModel::STATUS_DEPOSIT_FAILED]);
            $this->returnNotifySuccess($params);
        }

        // TODO add query on notify

        $sOrderNo = array_get($params, $this->orderNoColumn);
        /**
         * @var DepositModel $oUserDeposit
         */
        if (empty($sOrderNo) || empty($oUserDeposit = DepositModel::where('order_no', $sOrderNo)->first())) {
            $depositLog->update(['status' => LogModel::STATUS_NON_DEPOSIT]);
            throw new Exception("Deposit $sOrderNo Not Exists");
        }
        $oUserDeposit->logs()->save($depositLog);

        if ($this->getPayAmount($params) != $oUserDeposit->amount) {
            $depositLog->update(['status' => LogModel::STATUS_AMOUNT_ERROR]);
            throw new Exception("Wrong Deposit Amount");
        }

        // check deposit status
        if (!($oUserDeposit->isNew() || $oUserDeposit->isReceived())) {
            $depositLog->update(['status' => LogModel::STATUS_DEPOSIT_STATUS_ERROR]);
            $this->returnNotifySuccess($params);
        }

        $oUserDeposit->order->update([
            'third_order_no'     => array_get($params, $this->paymentOrderNoColumn),
            'third_order_status' => empty($this->successColumn) ?
            $this->successValue : array_get($params, $this->successColumn),
        ]);

        if (!$bSucc = $oUserDeposit->setWaitingLoad()) {
            $depositLog->update(['status' => LogModel::STATUS_SET_WAITINGLOAD_FAILED]);
            throw new Exception("Set Status To Waiting Failed");
        }

        // add notice task
        // $oUserDeposit->addDepositTask();
        $oUserDeposit->add_gold();

        $depositLog->update(['status' => LogModel::STATUS_SUCCESS]);
        $this->returnNotifySuccess($params);
    }

    protected function getPayAmount($params)
    {
        return array_get($params, $this->amountColumn);
    }

    protected function checkNotifySign($platform, $params)
    {
        // TODO remove true
        if (empty($params[$this->signColumn])) {
            return false;
        }

        // pr($this->compileNotifySign($platform, $params));
        // pr($params[$this->signColumn]);

        return $this->compileNotifySign($platform, $params) == $params[$this->signColumn];
    }

    protected $successValue;
    protected $successColumn;

    private function checkSuccessFlag($params)
    {
        if (empty($this->successColumn)) {
            return true;
        }

        return $this->successValue == array_get($params, $this->successColumn);
    }
}
