<?php

namespace Lib\Payment;

use Core\Singleton;
use Deposit\DepositModel as DepositModel;
use Deposit\OrderModel as OrderModel;
use Deposit\PlatformModel as PlatformModel;

/**
 * 自由付
 */
class PaymentZYFPAY extends BasePlatform
{
    use Singleton;

    public $banks = [
        '1'  => [
            'name' => '中国工商银行',
            'sign' => 'ICBC',
        ],
        '2'  => [
            'name' => '招商银行',
            'sign' => 'CMB',
        ],
        '3'  => [
            'name' => '中国建设银行',
            'sign' => 'CCB',
        ],
        '4'  => [
            'name' => '中国农业银行',
            'sign' => 'ABC',
        ],
        '5'  => [
            'name' => '中国银行',
            'sign' => 'BOC',
        ],
        '7'  => [
            'name' => '中国民生银行',
            'sign' => 'CMBC',
        ],
        '8'  => [
            'name' => '中信银行',
            'sign' => 'CNBC',
        ],
        '11' => [
            'name' => '中国光大银行',
            'sign' => 'CEB',
        ],
        '13' => [
            'name' => '广发银行股份有限公司',
            'sign' => 'GDB',
        ],
        '14' => [
            'name' => '华夏银行',
            'sign' => 'HXB',
        ],
    ];

    public $successMsg = '充值成功';

    // protected $test = true;
    // protected $transfer_params = true;

    // 返回数据中的 自身平台订单编号字段
    protected $orderNoColumn = 'tradeNo';
    // 点三放支付订单编号
    protected $paymentOrderNoColumn = "opeNo";

    protected $amountColumn = "amount";

    protected $successColumn = "status";
    protected $successValue  = '1';

    protected $signColumn = 'sign';

    protected $needSignColums = ['service', 'version', 'merId', 'tradeNo', 'tradeDate', 'amount', 'notifyUrl', 'extra', 'summary', 'expireTime', 'clientIp', 'bankId', 'opeNo', 'opeDate', 'status', 'payTime'];

    protected function mkTestData()
    {
        $data = [
            'platform_id'                       => '18',
            'api/deposit/notify/platform_id/18' => '',
            'service'                           => 'TRADE.NOTIFY',
            'merId'                             => '2017110944010081',
            'tradeNo'                           => 'a4f6a741bc5c86f957',
            'tradeDate'                         => '20171207',
            'opeNo'                             => '113832',
            'opeDate'                           => '20171207',
            'amount'                            => '1.00',
            'status'                            => '1',
            'extra'                             => '123',
            'payTime'                           => '20171207163555',
            'sign'                              => '335C254DE3A7CCBF4E0AB89AE717CB4B',
            'notifyType'                        => '1',
        ];
        return $data;
    }

    /**
     * 生成创建订单数据
     * @param  DepositModel $deposit [description]
     * @return [type]                [description]
     */
    public function compileLoadInputData(DepositModel $deposit)
    {
        $platform = $deposit->platform;

        $data = [
            'service'    => 'TRADE.B2C',
            'version'    => '1.0.0.0',
            'merId'      => $platform->account,
            'tradeNo'    => $deposit->order_no,
            'tradeDate'  => $deposit->created_at->format('Ymd'),
            'amount'     => $deposit->amount,
            'notifyUrl'  => $platform->notify_url,
            'extra'      => '123',
            'summary'    => '钻石积分',
            'expireTime' => 1800,
            'clientIp'   => get_client_ip(),
            'bankId'     => $this->banks[$deposit->bank_id]['sign'],
        ];

        $strsign      = $this->compileLoadSign($platform, $data);
        $data['sign'] = $strsign;
        return $data;
    }

    // 签名
    public function compileLoadSign(PlatformModel $platform, array $data)
    {
        $signstr = '';
        foreach ($data as $key => $value) {
            if ($key != $this->signColumn && in_array($key, $this->needSignColums)) {
                $signstr .= "{$key}={$value}&";
            }
        }
        $signstr = substr($signstr, 0, strlen($signstr) - 1);
        $signstr .= "{$platform->key}";
        // pr($signstr);exit;
        return md5($signstr);
    }

    public function compileLoadUrl(PlatformModel $platform, array $data, &$response = null)
    {

        $data['___DepositUrl'] = $platform->load_url;
        $aResponse             = $data;
        return $platform->relay_load_url . '?' . http_build_query($data);
    }

    public function saveLoadOrder(
        DepositModel $deposit,
        array $data,
        array $response,
        $loadUrl
    ) {
        $order                 = new OrderModel();
        $order->deposit_id     = $deposit->deposit_id;
        $order->third_order_no = array_get($response, $this->paymentOrderNoColumn);
        $order->load_url       = $loadUrl;
        $order->load_request   = $data;
        $order->load_response  = $response;
        $order->save();
        $deposit->order_id = $order->order_id;
        $deposit->save();
    }

    protected function compileNotifySign(PlatformModel $platform, array $params)
    {
        return strtoupper($this->compileLoadSign($platform, $params));
    }

    protected function returnNotifySuccess(array $params)
    {
        $aResponse = [
            'company_order_num'  => array_get($params, $this->orderNoColumn),
            'mownecum_order_num' => array_get($params, $this->paymentOrderNoColumn),
            'status'             => 1,
            'msg'                => $this->successMsg,
        ];
        exit(json_encode($aResponse));
    }

    public function returnNotifyError(array $params, $errMsg)
    {
        $aResponse = [
            'company_order_num'  => array_get($params, $this->orderNoColumn),
            'mownecum_order_num' => array_get($params, $this->paymentOrderNoColumn),
            'status'             => 0,
            'error_msg'          => $errMsg,
        ];
        exit(json_encode($aResponse));
    }

    public function makeQueryData(DepositModel $deposit)
    {
        $custom_params = $deposit->platform->custom_params;
        $data          = [
            'service'   => 'TRADE.QUERY',
            'version'   => '1.0.0.0',
            'merId'     => $deposit->platform->account,
            'tradeNo'   => $deposit->order_no,
            'tradeDate' => $deposit->created_at->format('Ymd'),
            'amount'    => $deposit->amount,
        ];
        $data['sign'] = $this->compileQuerySign($deposit->platform, $data);
        return $data;
    }

    public function compileQuerySign(PlatformModel $platform, array $data)
    {
        return $this->compileLoadSign($platform, $data);
    }

    public function getQueryResponse(array $data, DepositModel $deposit, &$response = null)
    {
        $response = json_decode(post($deposit->platform->query_url, $data), true);

        if ($response['status'] == 1) {

            $deposit->order->update([
                'third_order_no'     => $response['ali_trade_no'],
                'third_order_status' => $response['status'],
            ]);

            if (!$bSucc = $deposit->setWaitingLoad()) {
                return 0;
            }

            // add notice task
            $res = $deposit->addDepositTask();

            if ($res) {
                return 1;
            } else {
                return 0;
            }
        }

        return 0;
    }
}
