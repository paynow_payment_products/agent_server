<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/9/7
 * Time: 14:44
 */

namespace Lib\Payment;

use Core\Singleton;
use Deposit\DepositModel;
use Deposit\OrderModel as OrderModel;
use Deposit\PlatformModel;
use Request;

class PaymentJYPAY extends BasePlatform
{
    use Singleton;
    public $signColumn           = 'sign';
    public $successColumn        = 'result_code';
    public $successValue         = '0';
    public $successMsg           = '0';
    public $orderNoColumn        = 'out_trade_no';
    public $amountColumn         = 'total_fee';
    public $paymentOrderNoColumn = 'transaction_id';

    public function compileLoadInputData(DepositModel $deposit)
    {
        $data = [
            'service'       => 'create',
            'trade_type'    => 'pay.weixin.h5',
            'mch_id'        => $deposit->platform->account,
            'nonce_str'     => md5(microtime(true)),
            'mch_create_ip' => get_client_ip(),
            'out_trade_no'  => $deposit->order_no,
            'body'          => $deposit->amount . '积分',
            'scene_info'    => 'wap_url=http://www.f2dvip.com&wap_name=付二代',
            'total_fee'     => (string) ($deposit->amount * 100),
        ];
        $data['sign'] = $this->compileLoadSign($deposit->platform, $data);

        return $data;
    }

    /**
     * @param PlatformModel $platform
     * @param array         $data
     *
     * @return mixed
     */
    public function compileLoadSign(PlatformModel $platform, array $data)
    {
        //签名步骤一：按字典序排序参数
        ksort($data);
        $string = $this->ToUrlParams($data);

        //签名步骤二：在string后加入KEY
        $string .= "&key=" . $platform->key;

        //签名步骤三：MD5加密
        $string = md5($string);
        //签名步骤四：所有字符转为大写
        $result = strtoupper($string);

        return $result;
    }

    private function ToUrlParams($array)
    {
        $strings = "";
        foreach ($array as $k => $v) {
            if ($k != "sign" && $v != "" && !is_array($v)) {
                $strings .= $k . "=" . $v . "&";
            }
        }

        $strings = trim($strings, "&");

        return $strings;
    }

    public function compileLoadUrl(PlatformModel $platform, array $data, &$aResponse = null)
    {
        $result = post($platform->load_url, json_encode($data));
        if (empty($result)) {
            $aResponse = [$result];
            return '';
        }
        $aResponse = json_decode($result, true);
        if (empty($aResponse)) {
            // 无响应
            $aResponse = [$result];
            return '';
        }
        if (!is_array($aResponse)) {
            $aResponse = [$aResponse];
            return '';
        }
        if (!isset($aResponse['pay_info'])) {
            return '';
        }

        $pay_info = json_decode($aResponse['pay_info'], true);

        if ($aResponse['status'] !== '0') {
            return '';
        }

        return $pay_info['mweb_url'];
    }

    public function saveLoadOrder(
        DepositModel $deposit,
        array $data,
        array $response,
        $loadUrl
    ) {
        $order                 = new OrderModel();
        $order->deposit_id     = $deposit->deposit_id;
        $order->third_order_no = array_get($response, 'transaction_id');
        $order->load_url       = $loadUrl;
        $order->load_request   = $data;
        $order->load_response  = $response;
        $order->save();
        $deposit->order_id = $order->order_id;
        $deposit->save();
    }

    protected $transfer_params = true;

    protected function transferParams()
    {
        file_put_contents('/tmp/records.log', print_r(['GLOBALS', $GLOBALS], true), FILE_APPEND);
        file_put_contents('/tmp/records.log', print_r(['POST', $_POST], true), FILE_APPEND);
        file_put_contents('/tmp/records.log', print_r(['SERVER', $_SERVER], true), FILE_APPEND);
        file_put_contents('/tmp/records.log', print_r(['file', file_get_contents("php://input")], true), FILE_APPEND);

        if (isset($GLOBALS['HTTP_RAW_POST_DATA'])) {
            $post = $GLOBALS['HTTP_RAW_POST_DATA'];
        } else {
            $post = file_get_contents("php://input");
        }
        $data = json_decode($post, true);
        return $data;
    }

    protected function compileNotifySign(PlatformModel $platform, array $params)
    {
        return $this->compileLoadSign($platform, $params);
    }

    protected function returnNotifySuccess(array $params)
    {
        $platform_id = Request::get('platform_id');
        $platform    = PlatformModel::find($platform_id);
        $data        = [
            'status' => 0,
        ];
        $data['sign'] = $this->compileLoadSign($platform, $data);
        echo json_encode($data);
        exit;
    }

    protected function getPayAmount($data)
    {
        return $data[$this->amountColumn] / 100;
    }

    public function returnNotifyError(array $params, $errMsg)
    {
        $platform_id = Request::get('platform_id');
        $platform    = PlatformModel::find($platform_id);
        $data        = [
            'status'  => 0,
            'message' => $errMsg,
        ];
        $data['sign'] = $this->compileLoadSign($platform, $data);
        echo json_encode($data);
        exit;
    }
}
