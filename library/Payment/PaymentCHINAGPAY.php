<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/8/4
 * Time: 15:41
 */

namespace Lib\Payment;
use Core\Singleton;
use Deposit\DepositModel;
use Deposit\PlatformModel;
use Deposit\OrderModel as OrderModel;

class PaymentCHINAGPAY extends BasePlatform
{
    use Singleton;

    public $signColumn = 'signature';
    public $accountColumn = 'merId';
    public $successMsg = 'success';
    public $successValue = '1001';
    public $successColumn = 'respCode';
    public $orderNoColumn = 'merOrderId';
    public $amountColumn = 'txnAmt';
    public $paymentOrderNoColumn = 'merOrderId';

    public function compileLoadInputData(DepositModel $deposit)
    {
        $platform = $deposit->platform;
        $data = [
            'version' => '1.0.0',
            'txnType' => '01',
            'txnSubType' => '00',
            'bizType' => '000000',
            'accessType' => '0',
            'accessMode' => '01',
            'merId' => $platform->account,
            'merOrderId' => $deposit->order_no,
            'txnTime' => date('YmdHis'),
            'txnAmt' => $deposit->amount * 100,
            'currency' => 'CNY',
            'backUrl' => $platform->notify_url,
            'payType' => '0002',
        ];
        $data['signature'] = $this->compileLoadSign($platform, $data);
        $data['signMethod'] = 'MD5';
        return $data;
    }

    public function compileLoadSign(PlatformModel $platform, array $data)
    {
        if (isset($data['signMethod'])) {
            $data['signMethod'] = null;
            unset($data['signMethod']);
        }
        if (isset($data['signature'])) {
            $data['signature'] = null;
            unset($data['signature']);
        }
        if (isset($data['respMsg'])) {
            $data['respMsg'] = base64_decode($data['respMsg']);
        }
        if (isset($data['resv'])) {
            $data['resv'] = base64_decode($data['resv']);
        }
        $tip = '';
        $words = '';
        ksort($data);
        foreach ($data as $key => $value) {
            $words .= $tip . $key . '=' . $value;
            $tip = '&';
        }
        $words .= $platform->key;
        return base64_encode(md5($words, true));
    }

    public function compileLoadUrl(PlatformModel $platform, array $data, & $response = null)
    {
        $response = [];
        return $platform->load_url . '?' . http_build_query($data);
    }

    public function saveLoadOrder(
        DepositModel $deposit,
        array $data,
        array $response,
        $loadUrl
    ) {
        $order                 = new OrderModel();
        $order->deposit_id     = $deposit->deposit_id;
        $order->load_url       = $loadUrl;
        $order->load_request   = $data;
        $order->load_response  = $response;
        $order->save();
        $deposit->order()->associate($order);
        $deposit->save();
    }

    protected function compileNotifySign(PlatformModel $platform, array $params)
    {
        return $this->compileLoadSign($platform, $params);
    }

    protected $test = true;
    protected function mkTestData()
    {
        $oDeposit = DepositModel::find(1);
        $data = [
            'version' => '1.0.0',
            'txnType' => '01',
            'txnSubType' => '00',
            'bizType' => '000000',
            'accessType' => '0',
            'merId' => $oDeposit->platform->account,
            'merOrderId' => $oDeposit->order_no,
            'txnAmt' => $oDeposit->amount * 100, // 分模式
            'currency' => 'CNY',
            'channelId' => '',
            'txnTime' => date('YmdHis'),
            'merResv1' => '',
            'resv' => '',
            'settleAmount' => '',
            'settleCurrency' => '',
            'settleDate' => '',
            'succTime' => date('YmdHis'),
            'respCode' => '1001',
            'respMsg' => '交易成功',
        ];
        $data['signature'] = $this->compileNotifySign($oDeposit->platform, $data);
        $data['signMethod'] = 'MD5';
        return $data;
    }

    protected function getPayAmount($data)
    {
        return $data[$this->amountColumn] / 100;
    }

    protected function returnNotifySuccess(array $params)
    {
        echo 'success';
        exit;
    }

    public function returnNotifyError(array $params, $errMsg)
    {
        echo $errMsg;
        exit;
    }
}