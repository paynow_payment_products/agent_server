<?php

namespace Lib\Payment;

use Core\Singleton;
use Deposit\DepositModel as DepositModel;
use Deposit\OrderModel as OrderModel;
use Deposit\PlatformModel as PlatformModel;

/**
 * 幸福通
 */
class PaymentXFT extends BasePlatform
{
    use Singleton;

    public $successMsg = '充值成功';

    // protected $test = true;
    // protected $transfer_params = true;

    // 返回数据中的 自身平台订单编号字段
    protected $orderNoColumn = 'order_no';
    // 点三放支付订单编号
    protected $paymentOrderNoColumn = "trade_no";

    protected $amountColumn = "total_fee";

    protected $successColumn = "trade_status";
    protected $successValue  = 'TRADE_FINISHED';

    protected $signColumn = 'sign';

    protected $needSignColums = ['service', 'paymentType', 'merchantId', 'sellerEmail', 'returnUrl', 'notifyUrl', 'orderNo', 'title', 'body', 'totalFee', 'buyerEmail', 'paymethod', 'defaultbank', 'isApp', 'appName', 'appMsg', 'appType', 'userIp', 'backUrl', 'charset', 'buyer_email', 'buyer_id', 'discount', 'ext_param1', 'ext_param2', 'gmt_create', 'gmt_logistics_modify', 'gmt_payment', 'is_success', 'is_total_fee_adjust', 'notify_id', 'notify_time', 'notify_type', 'order_no', 'payment_type', 'price', 'quantity', 'seller_actions', 'seller_email', 'seller_id', 'title', 'total_fee', 'trade_no', 'trade_status', 'use_coupon'];

    public $banks = [
        '1'  => [
            'name' => '招商银行',
            'sign' => 'CMB',
        ],
        '2'  => [
            'name' => '中国工商银行',
            'sign' => 'ICBC',
        ],
        '3'  => [
            'name' => '中国建设银行',
            'sign' => 'CCB',
        ],
        '4'  => [
            'name' => '中国银行',
            'sign' => 'BOC',
        ],
        '5'  => [
            'name' => '中国农业银行',
            'sign' => 'ABC',
        ],
        '6'  => [
            'name' => '交通银行',
            'sign' => 'BOCM',
        ],
        '7'  => [
            'name' => '上海浦东发展银行',
            'sign' => 'SPDB',
        ],
        '8'  => [
            'name' => '广发银行股份有限公司',
            'sign' => 'CGB',
        ],
        '9'  => [
            'name' => '中信银行',
            'sign' => 'CITIC',
        ],
        '10' => [
            'name' => '中国光大银行',
            'sign' => 'CEB',
        ],
        '11' => [
            'name' => '福建兴业银行',
            'sign' => 'CIB',
        ],
        '12' => [
            'name' => '平安银行',
            'sign' => 'PAYH',
        ],
        '13' => [
            'name' => '中国民生银行',
            'sign' => 'CMBC',
        ],
        '14' => [
            'name' => '华夏银行',
            'sign' => 'HXB',
        ],
        '15' => [
            'name' => '邮政储汇',
            'sign' => 'PSBC',
        ],
        '16' => [
            'name' => '北京银行',
            'sign' => 'BCCB',
        ],
        '17' => [
            'name' => '上海银行',
            'sign' => 'SHBANK',
        ],
    ];

    protected function mkTestData()
    {
        $data = [
            'platform_id'                       => '17',
            'api/deposit/notify/platform_id/17' => '',
            'body'                              => 'AAA',
            'buyer_email'                       => 'xxxxxxx@163.com',
            'buyer_id'                          => '10086',
            'discount'                          => '0.00',
            'ext_param1'                        => '0015101342492891',
            'ext_param2'                        => 'ALIPAY',
            'gmt_create'                        => '2017-08-07 13:39:25',
            'gmt_logistics_modify'              => '1',
            'gmt_payment'                       => '2017-08-07 13:39:25',
            'is_success'                        => 'T',
            'is_total_fee_adjust'               => '0',
            'notify_id'                         => 'f63ce850785a4b3bb70cf11e65d62d2a',
            'notify_time'                       => '2017-08-07 23:59:42',
            'notify_type'                       => 'WAIT_TRIGGER',
            'order_no'                          => '3bd8394e571d1aec70',
            'payment_type'                      => '1',
            'price'                             => '1',
            'quantity'                          => '1',
            'seller_actions'                    => 'SEND_GOODS',
            'seller_email'                      => 'xxxxxxx@163.com',
            'seller_id'                         => '100000000002311',
            'title'                             => 'AAA',
            'total_fee'                         => '10',
            'trade_no'                          => '101708071024875',
            'trade_status'                      => 'TRADE_FINISHED',
            'use_coupon'                        => '1',
            'signType'                          => 'SHA',
        ];

        $platform = PlatformModel::find($data['platform_id']);

        $strsign      = $this->compileNotifySign($platform, $data);
        $data['sign'] = $strsign;
        return $data;
    }

    /**
     * 生成创建订单数据
     * @param  DepositModel $deposit [description]
     * @return [type]                [description]
     */
    public function compileLoadInputData(DepositModel $deposit)
    {
        $platform = $deposit->platform;

        $paymethodType = $platform->custom_params['paymethod'];

        if ($paymethodType === 'ALIH5') {
            $defaultbank = 'ALIPAY';
            $paymethod   = 'directPay';
            $isapp       = 'H5';
        } else if ($paymethodType === 'WXPAY') {
            $defaultbank = 'WXPAY';
            $paymethod   = 'directPay';
            $isapp       = 'app';
        } else if ($paymethodType === "WY") {
            $defaultbank = $this->banks[$deposit->bank_id]['sign'];
            $paymethod   = 'directPay';
            $isapp       = 'web';
        } else if ($paymethodType === 'QQH5') {
            $defaultbank = 'QQPAY';
            $paymethod   = 'directPay';
            $isapp       = 'app';
        } else if ($paymethodType === "QUICKPAY") {
            $defaultbank = 'QUICKPAY';
            $paymethod   = 'bankPay';
            $isapp       = 'web';
        } else if ($paymethodType === "JDPAY") {
            $defaultbank = 'JDPAY';
            $paymethod   = 'directPay';
            $isapp       = 'app';
        } else if ($paymethodType === 'UNIONPAY') {
            $defaultbank = 'UNIONQRPAY';
            $paymethod   = 'directPay';
            $isapp       = 'app';
        } else if ($paymethodType === 'JDH5') {
            $defaultbank = 'JDPAY';
            $paymethod   = 'directPay';
            $isapp       = 'H5';
        }else if ($paymethodType === 'EASYQUICK') {
            $defaultbank = 'EASYQUICK';
            $paymethod   = 'directPay';
            $isapp       = 'web';
        }

        $data = [
            "service"     => 'online_pay',
            "paymentType" => '1',
            "merchantId"  => $platform->account,
            "sellerEmail" => '593819004@qq.com',
            "returnUrl"   => SITE_URL,
            "notifyUrl"   => $platform->notify_url . $platform->platform_id,
            "orderNo"     => $deposit->order_no,
            "title"       => 'goods',
            "body"        => 'goods',
            "totalFee"    => (int) $deposit->amount,
            "buyerEmail"  => '4026739782@qq.com',
            "paymethod"   => $paymethod,
            "defaultbank" => $defaultbank,
            "isApp"       => $isapp,
            "charset"     => "UTF-8",
            "signType"    => 'SHA',
        ];
        $data['sign'] = $this->compileLoadSign($platform, $data);
        return $data;
    }

    // 签名
    public function compileLoadSign(PlatformModel $platform, array $data)
    {
        ksort($data);
        $signstr = '';
        foreach ($data as $key => $value) {
            if ($value && $key != $this->signColumn && in_array($key, $this->needSignColums)) {
                $signstr .= "{$key}={$value}&";
            }
        }
        $signstr = substr($signstr, 0, strlen($signstr) - 1);
        $signstr .= $platform->key;
        // pr($signstr);
        // return md5($signstr);
        return strtoupper(sha1($signstr));
    }

    public function compileLoadUrl(PlatformModel $platform, array $data, &$response = null)
    {

        $request_url = $platform->load_url . $data['merchantId'] . '-' . $data['orderNo'];

        if (in_array($platform->custom_params['paymethod'], ['WY', 'QUICKPAY', 'JDH5', 'ALIH5', 'EASYQUICK'])) {
            $data['___DepositUrl'] = $request_url;
            $aResponse             = $data;
            return $platform->relay_load_url . '?' . http_build_query($data);
        } else {
            $response = json_decode(post($request_url, $data), true);
            // pr($response);
            // exit;
            if (empty($response)) {
                // 无响应
                $response = [];
                return false;
            }
            if (!is_array($response) || !isset($response['respCode']) || $response['respCode'] != 'S0001') {
                // 主动返回错误
                $response = [$response];
                return false;
            }
            return array_get($response, 'codeUrl', '');
        }

    }

    public function saveLoadOrder(
        DepositModel $deposit,
        array $data,
        array $response,
        $loadUrl
    ) {
        $order                 = new OrderModel();
        $order->deposit_id     = $deposit->deposit_id;
        $order->third_order_no = array_get($response, $this->paymentOrderNoColumn);
        $order->load_url       = $loadUrl;
        $order->load_request   = $data;
        $order->load_response  = $response;
        $order->save();
        $deposit->order_id = $order->order_id;
        $deposit->save();
    }

    protected function compileNotifySign(PlatformModel $platform, array $params)
    {
        ksort($params);
        $signstr = '';
        foreach ($params as $key => $value) {
            if (in_array($key, $this->needSignColums) && $key != $this->signColumn) {
                $signstr .= "{$key}={$value}&";
            }
        }
        $signstr = substr($signstr, 0, strlen($signstr) - 1);
        $signstr .= $platform->key;
        // pr($signstr);
        // return md5($signstr);
        return strtoupper(sha1($signstr));
    }

    protected function returnNotifySuccess(array $params)
    {
        // header('HTTP/1.0 200 OK');exit;
        exit('success');
    }

    public function returnNotifyError(array $params, $errMsg)
    {
        $aResponse = [
            'company_order_num'  => array_get($params, $this->orderNoColumn),
            'mownecum_order_num' => array_get($params, $this->paymentOrderNoColumn),
            'status'             => 0,
            'error_msg'          => $errMsg,
        ];
        exit(json_encode($aResponse));
    }
}
