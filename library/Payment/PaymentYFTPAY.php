<?php

namespace Lib\Payment;

use Core\Singleton;
use Deposit\DepositModel as DepositModel;
use Deposit\OrderModel as OrderModel;
use Deposit\PlatformModel as PlatformModel;

class PaymentYFTPAY extends BasePlatform
{
    use Singleton;

    public $successMsg = '充值成功';

    // protected $test = true;
    // protected $transfer_params = true;

    // 返回数据中的 自身平台订单编号字段
    protected $orderNoColumn = 'merchantOutOrderNo';
    // 点三放支付订单编号
    protected $paymentOrderNoColumn = "orderNo";

    protected $amountColumn = "payMoney";

    protected $successColumn = "payResult";
    protected $successValue  = '1';

    protected $signColumn = 'sign';

    protected $needSignColums = ['merchantOutOrderNo', 'mch_no', 'merid', 'msg', 'noncestr', 'orderNo', 'payResult', 'notifyUrl', 'orderMoney', 'orderTime'];

    protected function mkTestData()
    {
        $data = [
            'platform_id'                       => '15',
            'api/deposit/notify/platform_id/15' => '',
            'merchantOutOrderNo'                => '50055ce3598bf1c441',
            'merid'                             => 'yft1403201710130002',
            'msg'                               => '{"payMoney":"100.00"}',
            'noncestr'                          => '1510134242868417',
            'orderNo'                           => '0015101342492891',
            'payResult'                         => '1',
        ];

        $platform = PlatformModel::find($data['platform_id']);

        $strsign      = $this->compileLoadSign($platform, $data);
        $data['sign'] = $strsign;
        return $data;
    }

    /**
     * 生成创建订单数据
     * @param  DepositModel $deposit [description]
     * @return [type]                [description]
     */
    public function compileLoadInputData(DepositModel $deposit)
    {
        $platform = $deposit->platform;

        $data = [
            'mch_no'             => $platform->account,
            'merchantOutOrderNo' => $deposit->order_no,
            'method'             => 1,
            'noncestr'           => time() . rand(100000, 999999),
            'notifyUrl'          => $platform->notify_url,
            'orderMoney'         => $deposit->amount,
            'orderTime'          => $deposit->created_at->format('YmdHis'),
            'type'               => 1,
        ];

        $strsign      = $this->compileLoadSign($platform, $data);
        $data['sign'] = $strsign;
        return $data;
    }

    // 签名
    public function compileLoadSign(PlatformModel $platform, array $data)
    {
        ksort($data);
        $signstr = '';
        foreach ($data as $key => $value) {
            if ($value && $key != $this->signColumn && in_array($key, $this->needSignColums)) {
                $signstr .= "{$key}={$value}&";
            }
        }
        $signstr .= "key={$platform->key}";
        // pr($signstr);
        return md5($signstr);
    }

    public function compileLoadUrl(PlatformModel $platform, array $data, &$response = null)
    {
        $data['___DepositUrl'] = $platform->load_url;
        $aResponse             = $data;
        return $platform->relay_load_url . '?' . http_build_query($data);

        // $urlscheme = '';
        // $agent     = strtolower($_SERVER['HTTP_USER_AGENT']);
        // if (strpos($agent, 'iphone') || strpos($agent, 'ipad')) {
        //     $urlscheme = 'alipay://platformapi/startApp?appId=10000011&url=';
        // } else if (strpos($agent, 'android')) {
        //     $urlscheme = 'alipays://platformapi/startApp?appId=10000011&url=';
        // }else{
        //     $urlscheme = 'alipay://platformapi/startApp?appId=10000011&url=';
        // }

        // $params = '';
        // foreach ($data as $key => $value) {
        //     $params .= $key . "=" .$value . "&";
        // }
        // $params = substr($params, 0, strlen($params) -1 );

        // return $urlscheme . urlencode( $platform->load_url . '?' . $params);
    }

    public function saveLoadOrder(
        DepositModel $deposit,
        array $data,
        array $response,
        $loadUrl
    ) {
        $order                 = new OrderModel();
        $order->deposit_id     = $deposit->deposit_id;
        $order->third_order_no = array_get($response, $this->paymentOrderNoColumn);
        $order->load_url       = $loadUrl;
        $order->load_request   = $data;
        $order->load_response  = $response;
        $order->save();
        $deposit->order_id = $order->order_id;
        $deposit->save();
    }

    protected function compileNotifySign(PlatformModel $platform, array $params)
    {
        return $this->compileLoadSign($platform, $params);
    }

    protected function returnNotifySuccess(array $params)
    {
        $aResponse = [
            'company_order_num'  => array_get($params, $this->orderNoColumn),
            'mownecum_order_num' => array_get($params, $this->paymentOrderNoColumn),
            'status'             => 1,
            'msg'                => $this->successMsg,
        ];
        exit(json_encode($aResponse));
    }

    public function returnNotifyError(array $params, $errMsg)
    {
        $aResponse = [
            'company_order_num'  => array_get($params, $this->orderNoColumn),
            'mownecum_order_num' => array_get($params, $this->paymentOrderNoColumn),
            'status'             => 0,
            'error_msg'          => $errMsg,
        ];
        exit(json_encode($aResponse));
    }

    protected function getPayAmount($params)
    {
        return json_decode($params['msg'], true)[$this->amountColumn];
    }
}
