<?php

namespace Lib\Payment;

use Core\Singleton;
use Deposit\DepositModel as DepositModel;
use Deposit\OrderModel as OrderModel;
use Deposit\PlatformModel as PlatformModel;

class PaymentJBP extends BasePlatform
{
    use Singleton;
    public $signColumn           = 'key';
    public $successValue         = '1';
    public $successMsg           = 'Daddy Pay充值成功';
    public $orderNoColumn        = 'company_order_num';
    public $amountColumn         = 'amount';
    public $paymentOrderNoColumn = 'mownecum_order_num';

    public $banks = [
        '1'  => [
            'name' => '中国工商银行',
            'sign' => 'ICBC',
        ],
        '2'  => [
            'name' => '招商银行',
            'sign' => 'CMB',
        ],
        '3'  => [
            'name' => '中国建设银行',
            'sign' => 'CCB',
        ],
        '4'  => [
            'name' => '中国农业银行',
            'sign' => 'ABC',
        ],
        '5'  => [
            'name' => '中国银行',
            'sign' => 'BOC',
        ],
        '6'  => [
            'name' => '交通银行',
            'sign' => 'BCM',
        ],
        '7'  => [
            'name' => '中国民生银行',
            'sign' => 'CMBC',
        ],
        '8'  => [
            'name' => '中信银行',
            'sign' => 'ECC',
        ],
        '9'  => [
            'name' => '上海浦东发展银行',
            'sign' => 'SPDB',
        ],
        '10' => [
            'name' => '邮政储汇',
            'sign' => 'PSBC',
        ],
        '11' => [
            'name' => '中国光大银行',
            'sign' => 'CEB',
        ],
        '12' => [
            'name' => '平安银行',
            'sign' => 'PINGAN',
        ],
        '13' => [
            'name' => '广发银行股份有限公司',
            'sign' => 'CGB',
        ],
        '14' => [
            'name' => '华夏银行',
            'sign' => 'HXB',
        ],
        '15' => [
            'name' => '福建兴业银行',
            'sign' => 'CIB',
        ],
    ];

    const DEPOSIT_MODE_BANK_CARD  = 1;
    const DEPOSIT_MODE_THIRD_PART = 2;

    public function compileLoadInputData(DepositModel $deposit)
    {
        $platform = $deposit->platform;
        $data     = [
            'company_id'             => $platform->account,
            'bank_id'                => $deposit->bank_id,
            'amount'                 => $deposit->amount,
            'company_order_num'      => $deposit->order_no,
            'company_user'           => $deposit->user_id,
            'estimated_payment_bank' => $deposit->bank_id,
            'deposit_mode'           => array_get($platform->custom_params, 'deposit_mode',
                self::DEPOSIT_MODE_THIRD_PART),
            'group_id'               => '0',
            'web_url'                => SITE_URL,
            'note_model'             => array_get($platform->custom_params, 'note_model', 2),
            'memo'                   => '',
            'note'                   => 'asd123',
            'terminal'               => array_get($platform->custom_params, 'terminal', '1'),
        ];
        $data['key'] = $sSafeStr = $this->compileLoadSign($platform, $data);

        return $data;
    }

    public function compileLoadSign(PlatformModel $platform, array $data)
    {
        $signNeedColumns = [
            'company_id',
            'bank_id',
            'amount',
            'company_order_num',
            'company_user',
            'estimated_payment_bank',
            'deposit_mode',
            'group_id',
            'web_url',
            'memo',
            'note',
            'note_model',
        ];
        $msg = '';
        foreach ($signNeedColumns as $sColumn) {
            $msg .= isset($data[$sColumn]) ? $data[$sColumn] : '';
        }

        return strtolower(md5(md5($platform->key) . $msg));
    }

    public function compileLoadUrl(PlatformModel $platform, array $data, &$response = null)
    {
        $response = json_decode(post($platform->load_url, $data), true);
        if (empty($response)) {
            // MC无响应
            $response = [];
            return false;
        }
        if (!is_array($response) || !array_get($response, 'status', 0)) {
            // MC主动返回错误
            $response = [$response];
            return false;
        }
        $depositMode = array_get($response, 'deposit_mode', 2);

        // deposit by bank card with note
        if ($depositMode == self::DEPOSIT_MODE_BANK_CARD) {
            // TODO create notepay detail page
            return '/fuyan_detail.php';
        }

        return array_get($response, 'break_url', '');
    }

    public function saveLoadOrder(
        DepositModel $deposit,
        array $data,
        array $response,
        $loadUrl
    ) {
        $order                 = new OrderModel();
        $order->deposit_id     = $deposit->deposit_id;
        $order->third_order_no = array_get($response, 'mownecum_order_num');
        $order->load_url       = $loadUrl;
        $order->load_request   = $data;
        $order->load_response  = $response;
        $order->save();
        $deposit->order_id = $order->order_id;
        $deposit->save();
    }

    protected function compileNotifySign(PlatformModel $platform, array $params)
    {
        $aData                    = [];
        $signNeedColumnsForNotify = [
            'pay_time',
            'bank_id',
            'amount',
            'company_order_num',
            'mownecum_order_num',
            'pay_card_num',
            'pay_card_name',
            'channel',
            'area',
            'fee',
            'transaction_charge',
            'deposit_mode',
        ];
        foreach ($signNeedColumnsForNotify as $sColumn) {
            $aData[$sColumn] = (isset($params[$sColumn]) ? $params[$sColumn] : '');
        }
        $sMsg = implode('', $aData);

        return strtolower(md5(md5($platform->key) . $sMsg));
    }

    protected function returnNotifySuccess(array $params)
    {
        $aResponse = [
            'company_order_num'  => array_get($params, 'company_order_num'),
            'mownecum_order_num' => array_get($params, 'mownecum_order_num'),
            'status'             => 1,
            'error_msg'          => $this->successMsg,
        ];
        exit(json_encode($aResponse));
    }

    public function returnNotifyError(array $params, $errMsg)
    {
        $aResponse = [
            'company_order_num'  => array_get($params, 'company_order_num'),
            'mownecum_order_num' => array_get($params, 'mownecum_order_num'),
            'status'             => 0,
            'error_msg'          => $errMsg,
        ];
        exit(json_encode($aResponse));
    }

    protected $test = false;
    protected function mkTestData()
    {
        $deposit = DepositModel::find(1);
        $data    = [
            'pay_time'           => date('YmdHis'),
            'bank_id'            => $deposit->bank_id,
            'amount'             => $deposit->amount,
            'company_order_num'  => $deposit->order_no,
            'mownecum_order_num' => $deposit->order->third_order_no,
            'transaction_charge' => '0.58',
            'deposit_mode'       => '2',
            'operating_time'     => $deposit->created_at->format('YmdHis'),
        ];
        $data['key'] = $this->compileNotifySign($deposit->platform, $data);
        return $data;
    }

    public function getBankName($band_id)
    {
        return $this->banks[$band_id]['name'];
    }
}
