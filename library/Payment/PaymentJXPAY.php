<?php

/**
 * RH Pay
 * stone
 */
namespace Lib\Payment;

use Core\Singleton;
use Curl\Curl;
use Deposit\DepositModel as DepositModel;
use Deposit\OrderModel as OrderModel;
use Deposit\PlatformModel as PlatformModel;

class PaymentJXPAY extends BasePlatform
{
    use Singleton;

    public $signColumn = 'sign';
    // 签名
    public $successValue = 1;
    // 1支付成功，非1支付失败
    public $successColumn = 'orderStatus';
    // 1支付成功。2支付失败
    public $successMsg = 'success';

    public $orderNoColumn = 'orderNo';
    // 上行过程中传入订单号
    public $amountColumn = 'transAmount';
    // 交易金额
    public $paymentOrderNoColumn = 'orderId';
    // 此次交易金融系统内的订单ID
    public $banks = [];

    const DEPOSIT_MODE_BANK_CARD = 1;

    const DEPOSIT_MODE_THIRD_PART = 2;

    public function compileLoadInputData(DepositModel $deposit)
    {
        $platform = $deposit->platform;
        $data     = [
            'merchNo'     => $platform->account,
            'orderNo'     => $deposit->order_no,
            'productName' => '玩具',
            'transAmount' => (string) ($deposit->amount * 100),
            'notifyUrl'   => $platform->notify_url,
            'pageUrl'     => SITE_URL,
            // 快捷支付参数，页面跳转链接
            // 'accNo' => '6214836555199575' //消费卡号（仅支持储蓄卡）

        ];
        if ($platform->custom_params['post_type'] == 'jsonKJ') {
            // 兼容不同支付模式
            $data['accNo'] = '6214836555199575';
        }
        $data['sign'] = $sSafeStr = $this->compileLoadSign($platform, $data);
        return $data;
    }

    public function compileLoadSign(PlatformModel $platform, array $data)
    {
        $signPars = "";
        ksort($data);
        foreach ($data as $k => $v) {
            if ("" != $v && "sign" != $k) {
                $signPars .= $k . "=" . $v . "&";
            }
        }
        $signPars = rtrim($signPars, '&');
        $signPars .= $platform->key;
        $sign = MD5($signPars);
        return $sign;
    }

    public function compileLoadUrl(PlatformModel $platform, array $data, &$response = null)
    {
        if (substr($platform->custom_params['post_type'], 0, 4) == 'form') {
            $data['___DepositUrl'] = $platform->load_url;
            $aResponse             = $data;
            return $platform->relay_load_url . '?' . http_build_query($data);
        } else if (substr($platform->custom_params['post_type'], 0, 4) == 'json') {
            $curl = new Curl();
            $curl->setOpt(CURLOPT_CONNECTTIMEOUT, 120);
            $curl->setOpt(CURLOPT_TIMEOUT, 120);
            $curl->setOpt(CURLOPT_HTTPHEADER, [
                "Content-Type:application/json;charset=UTF-8",
            ]);
            $curl->post($platform->load_url, json_encode($data));
            $response = json_decode($curl->response, true);
            if ($platform->custom_params['post_type'] == 'jsonKJ') {
                return $response['qrcodeUrl'];
            } else {
                if (key_exists('qrcodeUrl', $response)) {
                    return $response['qrcodeUrl'];
                } else {
                    return '';
                }

            }
        }
    }

    public function saveLoadOrder(DepositModel $deposit, array $data, array $response, $loadUrl)
    {
        $order                 = new OrderModel();
        $order->deposit_id     = $deposit->deposit_id;
        $order->third_order_no = array_get($response, 'mownecum_order_num');
        $order->load_url       = $loadUrl;
        $order->load_request   = $data;
        $order->load_response  = $response;
        $order->save();
        $deposit->order_id = $order->order_id;
        $deposit->save();
    }

    public $transfer_params = true;

    protected function transferParams()
    {
        return json_decode(file_get_contents("php://input"), true);
    }

    public function getPayAmount($params)
    {
        return $params['transAmount'] / 100;
    }

    protected function compileNotifySign(PlatformModel $platform, array $params)
    {
        $signPars = "";
        $data     = $params;
        ksort($data);
        foreach ($data as $k => $v) {
            if ("" != $v && "sign" != $k) {
                $signPars .= $k . "=" . $v . "&";
            }
        }
        $signPars = rtrim($signPars, '&');
        $signPars .= $platform->key;
        $sign = MD5($signPars);
        return $sign;
    }

    protected function returnNotifySuccess(array $params)
    {
        exit('ok');
    }

    public function returnNotifyError(array $params, $errMsg)
    {
        $aResponse = [
            'company_order_num'  => array_get($params, 'company_order_num'),
            'mownecum_order_num' => array_get($params, 'mownecum_order_num'),
            'status'             => 0,
            'error_msg'          => $errMsg,
        ];
        exit(json_encode($aResponse));
    }

    protected $test = false;

    protected function mkTestData()
    {
        $deposit = DepositModel::find(1);
        $data    = [
            'pay_time'           => date('YmdHis'),
            'bank_id'            => $deposit->bank_id,
            'amount'             => $deposit->amount,
            'company_order_num'  => $deposit->order_no,
            'mownecum_order_num' => $deposit->order->third_order_no,
            'transaction_charge' => '0.58',
            'deposit_mode'       => '2',
            'operating_time'     => $deposit->created_at->format('YmdHis'),
        ];
        $data['key'] = $this->compileNotifySign($deposit->platform, $data);
        return $data;
    }

    public function getBankName($band_id)
    {
        return $this->banks[$band_id]['name'];
    }
}
