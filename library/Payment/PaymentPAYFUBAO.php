<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/8/16
 * Time: 17:03
 */

namespace Lib\Payment;

use Core\Singleton;
use Deposit\DepositModel;
use Deposit\PlatformModel;
use Deposit\OrderModel as OrderModel;

class PaymentPAYFUBAO extends BasePlatform
{
    use Singleton;

    public $signColumn = 'sign';
    public $successMsg = 'OK';
    public $successValue = 'OK';
    public $successColumn = '';
    public $orderNoColumn = 'orderno';
    public $amountColumn = 'fee';
    public $paymentOrderNoColumn = 'wxno';

    public function compileLoadInputData(DepositModel $deposit)
    {
        $data = [
            'body' => ( (int) $deposit->amount ) . '积分',
            'total_fee' => $deposit->amount * 100,
            'para_id' => $deposit->platform->account,
            'order_no' => $deposit->order_no,
            'notify_url' => $deposit->platform->notify_url,
            'attach' => 'asd123',
            'child_para_id' => 1,
            'device_id' => strpos($_SERVER ['HTTP_USER_AGENT'], 'Android') !== false? 1: 2,
        ];
        if ($deposit->platform->icon == 'wechat') {
            $data += [
                'mch_create_ip' => get_client_ip(),
                'mch_app_id' => 'http://www.88dafu.com/',
                'mch_app_name' => '华科富丽商贸',
            ];
        }
        $data += $deposit->platform->custom_params;
        $data['sign'] = $this->compileLoadSign($deposit->platform, $data);
        return $data;
    }

    public function compileLoadSign(PlatformModel $platform, array $data)
    {
        return md5($data['para_id'] . $data['app_id'] . $data['order_no'] . $data['total_fee'] . $platform->key);
    }

    public function compileLoadUrl(PlatformModel $platform, array $data, & $response = null)
    {
        $response = json_decode(post($platform->load_url, $data), true);
        if (empty($response)) { // 无响应
            $response = [];
            return false;
        }
        if (!is_array($response) || !isset($response['status']) || $response['status'] != 0) { // 主动返回错误
            $response = [$response];
            return false;
        }
        return array_get($response, 'pay_url', '');
    }

    public function saveLoadOrder(
        DepositModel $deposit,
        array $data,
        array $response,
        $loadUrl
    )
    {
        $order                 = new OrderModel();
        $order->deposit_id     = $deposit->deposit_id;
        $order->load_url       = $loadUrl;
        $order->load_request   = $data;
        $order->load_response  = $response;
        $order->save();
        $deposit->order_id = $order->order_id;
        $deposit->save();
    }

    protected function compileNotifySign(PlatformModel $platform, array $params)
    {
        return md5($params['orderno'] . $params['fee'] . $platform->key);
    }

    protected function returnNotifySuccess(array $params)
    {
        exit('ok');
    }

    protected function getPayAmount($params)
    {
        return array_get($params, $this->amountColumn) / 100;
    }

    public function returnNotifyError($params, $error)
    {
        exit($error);
    }
}
