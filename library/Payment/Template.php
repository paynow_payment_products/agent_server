<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/8/16
 * Time: 17:06
 */

namespace Lib\Payment;

use Core\Singleton;
use Deposit\DepositModel;
use Deposit\PlatformModel;
use Deposit\OrderModel as OrderModel;

class Template extends BasePlatform
{
    use Singleton;

    public function compileLoadInputData(DepositModel $deposit)
    {

    }

    /**
     * @param PlatformModel $platform
     * @param array         $data
     *
     * @return mixed
     */
    public function compileLoadSign(PlatformModel $platform, array $data)
    {

    }

    public function compileLoadUrl(PlatformModel $platform, array $data, & $aResponse = null)
    {

    }

    public function saveLoadOrder(
        DepositModel $deposit,
        array $data,
        array $response,
        $loadUrl
    )
    {

    }

    protected function compileNotifySign(PlatformModel $platform, array $params)
    {

    }
    protected function returnNotifySuccess(array $params)
    {

    }
}