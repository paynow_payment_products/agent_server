<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/8/9
 * Time: 16:22
 */

namespace SmsCenter;

use Session;
use Exception;
use libphonenumber\PhoneNumberUtil;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberType;

class Tools
{
    public static function getVcode($mobile)
    {
        $mobile  = self::formatPhoneNumber($mobile);
        $session = Session::getInstance();
        if (empty($session->vcodes[$mobile]) || $session->vcodes[$mobile]['created_at'] <= (time() - 600)) {
            $vcodes          = generateVCode();
            $session->vcodes = [
                $mobile => [
                    'vcode'      => $vcodes,
                    'created_at' => time(),
                    'send_times' => 0,
                ],
            ];
        }

        if ($session->vcodes[$mobile]['send_times'] >= 3) {
            throw new Exception('发送太频繁了，请稍后再试');
        }

        return $session->vcodes[$mobile];
    }

    public static function sent($mobile)
    {
        $mobile              = self::formatPhoneNumber($mobile);
        $session             = Session::getInstance();
        $vcode               = $session->vcodes[$mobile];
        $vcode['created_at'] = time();
        $vcode['send_times']++;
        $session->vcodes = [
            $mobile => $vcode,
        ];
    }

    public static function checkVcode($mobile, $vcode)
    {
        $mobile  = self::formatPhoneNumber($mobile);
        $session = Session::getInstance();
        if (empty($session->vcodes[$mobile])) {
            return false;
        }

        if ($session->vcodes[$mobile]['created_at'] <= (time() - 600)) {
            $session->vcodes = null;

            return false;
        }

        if ($session->vcodes[$mobile]['vcode'] != $vcode) {
            return false;
        }

        return true;
    }

    public static function validateIsMobilePhoneNumber($value)
    {
        $phoneNumberUtil = PhoneNumberUtil::getInstance();
        if ( ! $phoneNumberUtil->isValidNumber(self::parsePhoneNumber($value))) {
            return false;
        }
        $numberType = $phoneNumberUtil->getNumberType(self::parsePhoneNumber($value));
        if ($numberType !== PhoneNumberType::MOBILE) {
            return false;
        }

        return true;
    }

    public static function formatPhoneNumber($value)
    {
        $phoneNumberUtil = PhoneNumberUtil::getInstance();

        return $phoneNumberUtil->format(self::parsePhoneNumber($value), PhoneNumberFormat::E164);
    }

    public static function parsePhoneNumber($value)
    {
        try {
            $phoneNumberUtil = PhoneNumberUtil::getInstance();

            return $phoneNumberUtil->parse($value);
        } catch (Exception $e) {
            throw new Exception('请检查手机号！');
        }
    }
}