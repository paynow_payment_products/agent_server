<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/2/7
 * Time: 下午2:05
 *
 * 本类为单例模式，需要调用getInstance获取实例
 */
namespace SmsCenter\Sdk;
use SmsCenter\Tools;

class Sms
{
    // 定义模板名称，需要和服务里对应起来
    const SMS_RONGLIAN_YZM = 'yunpian';

    // 定义服务地址
    const SERVICE_SENDSMS_URL = 'http://127.0.0.1:9502/sendsms';

    public function send($mobile, $type, $message)
    {
        $mobile = Tools::formatPhoneNumber($mobile);
        $data = array(
            'mobile' => $mobile,
            'type' => $type,
            'message' => $message,
        );
        $this->httpGet(self::SERVICE_SENDSMS_URL, $data);
        return true;
    }

    protected function httpGet($url, $data)
    {
        if ($data) {
            $url .= '?' . http_build_query($data);
        }
        curl_setopt($this->_curl, CURLOPT_URL, $url);   //设置网址
        $response = curl_exec($this->_curl);   //执行
        return $response;
    }

    private function __construct()
    {
        $curlObj = curl_init();    //初始化curl，
        curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);  //将curl_exec的结果返回
        curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curlObj, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curlObj, CURLOPT_HEADER, 0);         //是否输出返回头信息
        $this->_curl = $curlObj;
    }

    public function __destruct()
    {
        curl_close($this->_curl);          //关闭会话
    }

    private static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance instanceof self) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
}