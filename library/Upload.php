<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/17
 * Time: 16:44
 */

namespace Lib;

class Upload
{
    public $savePath;
    public $upTypes = [
        'image/jpeg',
        'image/jpg',
        'image/png',
        'image/pjpeg',
        'image/gif',
    ];
    public $maxFileSize = 2000000;
    public $waterMark = false;

    public function __construct($savePath = '') {
        $this->savePath = $savePath ? $savePath : MAIN_PATH . '/uploads/imgs/';
        $this->checkFilePath();
    }

    public function checkFilePath(){
        file_exists($this->savePath) or mkdir($this->savePath, 0777, true);
    }

    public function saveImgs($data){

        $tmpName = $data['tmp_name'];
        $fileName = $data['name'];
        $filePath = $this->savePath . $fileName;

        if ( !$tmpName ) {
            return ['result' => 1, 'message' => '文件不存在！', 'data' => ''];
        }

        if ( $data['size'] > $this->maxFileSize) {
            return ['result' => 1, 'message' => '文件过大！', 'data' => ''];
        }

        if ( !in_array($data['type'], $this->upTypes) ) {
            return ['result' => 1, 'message' => '文件类型不对！', 'data' => ''];
        }

        if ( $data['error'] ) {
            return ['result' => 1, 'message' => 'Upload error '.$data['error'], 'data' => ''];
        }

        $url = '/uploads/imgs/' . rawurlencode($fileName);

        if ( file_exists($filePath) && md5_file($filePath) == md5_file($tmpName) ) {
            return ['result' => 0, 'message' => '上传成功', 'data' => ['url' => $url]];
        }else{
            $result = move_uploaded_file($tmpName, $filePath);
            if ( $result ) {
                return ['result' => 0, 'message' => '上传成功', 'data' => ['url' => $url]];
            }else{
                return ['result' => 1, 'message' => '保存失败', 'data' => ''];
            }
        }

    }

}
