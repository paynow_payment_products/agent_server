<?php

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/6
 * Time: 11:35
 */

namespace Lib;

use LaravelArdent\Ardent\InvalidModelException;
use Lib\Pn\Exception as PnException;
use Request;
use Session;
use Yaf_Exception;

class Error
{

    protected $disableActions = true;
    protected $disableView = true;

    private static $_isApi = false;

    public static function isApi()
    {
        self::$_isApi = true;
    }

    private static $_isPn = false;

    public static function isPn()
    {
        self::$_isPn = true;
    }

    public static function error($exception)
    {
        $session = Session::getInstance();
        if (! is_null(Request::get('debug')) || ! empty($session->debug)) {
            // pr($exception);
            exit;
        }
        if (self::$_isPn || $exception instanceof PnException) {
            exit(json_encode([
                'ret'   => 1,
                'error' => $exception->getMessage(),
            ]));
        }
        if (self::$_isApi || Request::isXmlHttpRequest()) {
            exit(self::ajaxError($exception));
        }

        if (Request::isCli()) {
            exit(self::cliError($exception));
        }

        if ($exception instanceof InvalidModelException) {
            pr($exception->getErrors()->toArray());
            exit;
        }

        exit(self::ajaxError($exception));
    }

    public static function ajaxError($exception)
    {
        if ($exception instanceof Yaf_Exception || $exception instanceof InvalidModelException) {
            $message = '系统出现异常，请稍后再试！';//$exception->getErrors()->toArray();
        } else {
            $message = $exception->getMessage();
        }

        return json_encode([
            'success' => 0,
            'message' => $message,
        ]);
    }

    public static function cliError($exception)
    {
        if ($exception instanceof InvalidModelException) {
            $message = print_r($exception->getErrors()->toArray());
        } else {
            $message = $exception->getMessage();
        }

        $data = date('Y-m-d H:i:s');

        return "[$data] $message \n";
    }
}