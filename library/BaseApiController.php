<?php

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/17
 * Time: 00:31
 */
class BaseApiController extends BaseController
{
    public $disableView = true;

    public function _initError()
    {
        \Lib\Error::isApi();
    }
}