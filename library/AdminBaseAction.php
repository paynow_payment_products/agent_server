<?php

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/15
 * Time: 18:45
 */

use \Illuminate\Pagination\Paginator;
use \Manage\AdminLogModel;
use \Manage\AdminModel;
use \Manage\FunctionalitiesModel;

abstract class AdminBaseAction extends \Core\Action
{

    protected $needCheckLogin = true;

    protected $needCheckRights = true;

    protected $isAjax = false;

    protected $pageName = 'page';

    /**
     * @return \User\UserModel
     * @throws Exception
     */
    public function _initCheckLogin()
    {
        if (!$this->needCheckLogin) {
            return;
        }

        $session = $this->session();
        // pr($session);

        $admin_id = empty($session->admin_id) ? '' : $session->admin_id;

        if (empty($admin_id)) {
            $this->redirect('/manage/index');
        }

        $admin = AdminModel::find($admin_id);

        if (empty($admin)) {
            throw new Exception('invalid user');
        }

        $admin->logs()->save(new AdminLogModel([
            'controller' => get_class($this->getController()),
            'action'     => get_called_class(),
        ]));

        $this->assign('admin', $admin);

        return $admin;
    }

    public function _initCheckRights()
    {
        if (!$this->needCheckRights) {
            return true;
        }

        $userGroup = AdminModel::find($this->session()->admin_id)->group()->orderBy('priority', 'asc')->first();

        // 超级管理员不检测权限
        if (is_object($userGroup) && $userGroup->group_id == 1) {
            return true;
        }

        if (!is_object($userGroup)) {
            $rights = [];
        }else{
            $rights = $userGroup->functionalitie->pluck('function_id')->toArray();
        }

        $currentRight = FunctionalitiesModel::where([
            'module'     => 'Manage',
            'controller' => get_class($this->getController()),
            'action'     => get_called_class(),
        ])->first();

        // pr($rights);
        // pr($currentRight);

        if (!is_object($currentRight) || !in_array($currentRight->function_id, $rights)) {
            echo "没有权限！！！";
            echo '<script type="text/javascript">setTimeout(function(){ window.history.back(); },1000);</script>';
            exit;
        }
    }

    public function _initCheckIsAjax()
    {
        $this->isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == "XMLHttpRequest" ? true : false;
    }

    public function _initPagination()
    {
        paginator::currentPathResolver(function () {
            $url = Request::getRequestUri() . '?';
            foreach (Request::getQuery() as $key => $value) {
                $key == $this->pageName or $url .= "&$key=$value";
            }
            return $url;
        });

        Paginator::currentPageResolver(function () {
            $page = Request::getQuery($this->pageName);

            if (filter_var($page, FILTER_VALIDATE_INT) !== false && (int) $page >= 1) {
                return $page;
            }

            return 1;
        });
    }

    public function writeDataToCsv($filename, $data)
    {
        header("Content-type:text/csv");
        header("Content-Disposition:attachment;filename=" . $filename);
        header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
        header('Expires:0');
        header('Pragma:public');
        header("Content-type: text/csv");
        echo chr(239) . chr(187) . chr(191); // 加上bom头，系统自动默认为UTF-8编码
        echo $data;
    }
}
