<?php

define('SSL_CERT_DIR', __DIR__ . '/cert');
define('LOG_DIR', __DIR__ . '/logs');
define('PID_FILE', LOG_DIR . '/sms_socket_server.pid');

class Sms
{
    const YUNPIAN = 'yunpian';
    const SMS_TYPES = [self::YUNPIAN];

    public $_http;
    public $application;

    public function __construct($server_host, $server_port)
    {
        ob_start();
        $this->_http = new swoole_http_server($server_host, $server_port);
        $this->_http->set([
            'ssl_cert_file'       => SSL_CERT_DIR . '/ssl.crt',
            'ssl_key_file'        => SSL_CERT_DIR . '/ssl.key',
            'open_http2_protocol' => true,
            'worker_num'          => 1,   //一般设置为服务器CPU数的1-4倍
            'daemonize'           => true,  //以守护进程执行
            'max_request'         => 10000, //
            'dispatch_mode'       => 2, // 数据包分发策略:固定模式
            'task_worker_num'     => 8,  //task进程的数量
            "task_ipc_mode "      => 3,  //使用消息队列通信，并设置为争抢模式
            'log_file'            => LOG_DIR . '/sms_socket_server.log',
        ]);

        $this->_http->on('Request', array($this, 'onRequests'));
        $this->_http->on('Task', array($this, 'onTask'));
        $this->_http->on('Finish', function ($serv, $task_id, $data) {
            echo "Task {$task_id} finish\n";
            echo "Result: {$data}\n\n\n\n";
        });
    }

    public function run()
    {
        $this->_http->start();
    }

    public function onRequests($request, $response)
    {
        $response->status('200');
        $server = $request->server;

        // 检查请求分发任务
        if (isset($server['request_uri']) && $server['request_uri'] == '/sendsms') {
            if ( ! isset($server['query_string'])) {
                echo "no query string!\n";
                exit;
            }

            $query_string = $server['query_string'];
            parse_str($query_string, $data);
            if ( ! isset($data['mobile']) || ! isset($data['type']) || ! isset($data['message'])) {
                echo "params error!\n";
                exit;
            }

            if ( ! in_array($data['type'], self::SMS_TYPES)) {
                echo "invalid sms type id!\n";
                exit;
            }
            $this->_http->task($data);
        }
        $result = ob_get_contents();
        @ob_end_clean();
        if ($result == '') {
            $result = ' ';
        }
        $response->end($result);
    }

    public function onTask($serv, $task_id, $from_id, Array $data)
    {
        $to     = $data['mobile'];
        $vcode  = $data['message'];

        $sender    = new YunPian();
        $result    = $sender->send($to, $vcode);

        echo $result;

        return $result;
    }

    private static $_instance;

    public static function getInstance()
    {
        $ip      = isset($argv[1]) ? $argv[1] : '127.0.0.1';
        $port    = isset($argv[2]) ? $argv[2] : 9502;
        $address = $ip . ':' . $port;
        if ( ! self::$_instance[$address] instanceof self) {
            self::$_instance[$address] = new self($ip, $port);
        }

        return self::$_instance[$address];
    }
}

class YunPian
{
    use Singleton;

    // APPKEY
    public $apiKey = '69782b7c5f2cc070538b3523af865b03';

    //请求地址，格式如下，不需要写https://
    public $serverIP = 'https://sms.yunpian.com/v2/sms/single_send.json';

    public function send($to, $vcode) {
        $text = '【easypay】Your verification code is ' . $vcode;
//        if (strpos($to, '+86') !== false) {
//            $text = '';
//        }
        return post($this->serverIP, [
            'apikey' => $this->apiKey,
            'mobile' => $to,
            'text' => $text,
        ]);
    }
}

function post($url, $data) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //将数据传给变量
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); //取消身份验证
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    $response = curl_exec($ch); //接收返回信息
    if (curl_errno($ch)) {//出错则显示错误信息
        return curl_error($ch);
    }
    curl_close($ch); //关闭curl链接
    return $response;
}

trait Singleton
{
    /**
     * private construct, generally defined by using class
     */
    //private function __construct() {}

    /**
     * @return static
     */
    private static $_instance;

    public static function getInstance()
    {
        if ( ! self::$_instance instanceof self) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function __clone()
    {
        trigger_error('Cloning ' . __CLASS__ . ' is not allowed.', E_USER_ERROR);
    }

    public function __wakeup()
    {
        trigger_error('Unserializing ' . __CLASS__ . ' is not allowed.', E_USER_ERROR);
    }
}

Sms::getInstance()->run();
