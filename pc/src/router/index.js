import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'

Vue.use(Router)

const router = new Router(routes)

router.beforeEach((to, from, next) => {
  if (typeof to.query.token === 'undefined') {
    next()
    return
  }
  const token = to.query.token
  delete to.query.token
  Vue.models.User.token({token: token}).then(function () {
    router.replace({path: to.path, params: to.params, query: to.query})
    next()
  }).catch(function () {
    router.replace({path: to.path, params: to.params, query: to.query})
    next()
  })
})

export default router
