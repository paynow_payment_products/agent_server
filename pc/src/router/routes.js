import login from './routers/login'
import panel from './routers/panel'
import transaction from './routers/transaction'
import deposit from './routers/deposit'
import withdrawal from './routers/withdrawal'
import card from './routers/card'

const routes = {
  routes: []
}

routes.routes = routes.routes.concat(login)
routes.routes = routes.routes.concat(panel)
routes.routes = routes.routes.concat(transaction)
routes.routes = routes.routes.concat(deposit)
routes.routes = routes.routes.concat(withdrawal)
routes.routes = routes.routes.concat(card)

export default routes
