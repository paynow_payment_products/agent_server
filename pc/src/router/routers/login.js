import Login from '@/components/Login'
import LoginIndex from '@/components/login/Index'

export default {
  path: '/login',
  name: 'login',
  component: Login,
  redirect: '/login/index',
  children: [
    {
      path: 'index',
      name: 'login-index',
      component: LoginIndex
    }
  ]
}
