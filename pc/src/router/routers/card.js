import Card from '@/components/Card'
import CardList from '@/components/card/List'
import CardAdd from '@/components/card/Add'

export default {
  path: '/card',
  name: 'card',
  component: Card,
  redirect: '/card/list',
  children: [
    {
      path: 'list',
      name: 'card-list',
      component: CardList
    },
    {
      path: 'add',
      name: 'card-add',
      component: CardAdd
    }
  ]
}
