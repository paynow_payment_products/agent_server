import Transaction from '@/components/Transaction'
import TransactionList from '@/components/transaction/List'
import Transfer from '@/components/transaction/Transfer'
import TransactionTransferDetal from '@/components/transaction/TransferDetal'

export default {
  path: '/transaction',
  name: 'transaction',
  component: Transaction,
  redirect: '/transaction/list',
  children: [
    {
      path: 'list',
      name: 'transaction-list',
      component: TransactionList
    },
    {
      path: 'transfer',
      name: 'transfer',
      component: Transfer
    },
    {
      path: 'transferDetal',
      name: 'transfer-detal',
      component: TransactionTransferDetal
    }
  ]
}
