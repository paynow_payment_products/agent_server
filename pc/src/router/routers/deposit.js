import Deposit from '@/components/Deposit'
import DepositList from '@/components/deposit/List'
import DepositAdd from '@/components/deposit/Add'

export default {
  path: '/deposit',
  name: 'deposit',
  component: Deposit,
  redirect: '/deposit/list',
  children: [
    {
      path: 'list',
      name: 'deposit-list',
      component: DepositList
    },
    {
      path: 'add',
      name: 'deposit-add',
      component: DepositAdd
    }
  ]
}
