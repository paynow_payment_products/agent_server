import Panel from '@/components/Panel'
import PanelIndex from '@/components/panel/Index'

export default {
  path: '/',
  name: 'panel',
  component: Panel,
  redirect: '/',
  children: [
    {
      path: '',
      name: 'panel-index',
      component: PanelIndex
    },
    {
      path: 'index',
      name: 'panel-index',
      component: PanelIndex
    }
  ]
}
