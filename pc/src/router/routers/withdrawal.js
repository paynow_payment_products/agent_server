import Withdrawal from '@/components/Withdrawal'
import WithdrawalList from '@/components/withdrawal/List'
import WithdrawalAdd from '@/components/withdrawal/Add'
import WithdrawalDetail from '@/components/withdrawal/Detail'

export default {
  path: '/withdrawal',
  name: 'withdrawal',
  component: Withdrawal,
  redirect: '/withdrawal/list',
  children: [
    {
      path: 'list',
      name: 'withdrawal-list',
      component: WithdrawalList
    },
    {
      path: 'add',
      name: 'withdrawal-add',
      component: WithdrawalAdd
    },
    {
      path: 'detail',
      name: 'withdrawal-detail',
      component: WithdrawalDetail
    }
  ]
}
