// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App'
import router from './router'
import Models from './models'
import VueRouter from 'vue-router'
import axios from 'axios'
import Header from './components/units/header.vue'
import Password from './components/units/Password'

Vue.use(VueRouter)
axios.defaults.withCredentials = true
Vue.prototype.$axios = axios
Vue.use(Models)
Vue.use(ElementUI)
Vue.use(Header)
Vue.component('bestpay-header', Header)
Vue.use(Password)
Vue.component('password', Password)

Vue.prototype.$loading = ElementUI.Loading.service
Vue.prototype.$msgbox = ElementUI.MessageBox
Vue.prototype.$alert = ElementUI.MessageBox.alert
Vue.prototype.$confirm = ElementUI.MessageBox.confirm
Vue.prototype.$prompt = ElementUI.MessageBox.prompt
Vue.prototype.$notify = ElementUI.Notification
Vue.prototype.$message = ElementUI.Message

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
