import User from './models/User'
import Card from './models/Card'
import Transaction from './models/Transaction'
import Deposit from './models/Deposit'
import Withdrawal from './models/Withdrawal'
import Messages from './models/Messages'
import Contact from './models/Contact'

var serverUrl = '/api/'
if (window.location.host === 'localhost:8081' || window.location.host === 'pc.test.f2d.me' || window.location.host === 'c.test.f2d.me') {
  serverUrl = '//api.test.sc/api/'
}

const models = {
  User,
  Card,
  Transaction,
  Deposit,
  Withdrawal,
  Messages,
  Contact
}
export default {
  install (Vue) {
    Vue.prototype.$models = models
    Vue.models = models
    Vue.prototype.serverUrl = serverUrl
  },
  $models: models
}

export const $models = models
