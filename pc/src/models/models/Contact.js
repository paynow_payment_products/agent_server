import Vue from 'vue'

export default {
  add: function (params) {
    return Vue.prototype.$axios.get(Vue.prototype.serverUrl + 'feedback/report', {
      params: params
    })
  }
}
