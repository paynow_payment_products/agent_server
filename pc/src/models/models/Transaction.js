/**
 * Created by roger.s on 2017/7/3.
 */
import Vue from 'vue'

export default {
  list: function (params) {
    return Vue.prototype.$axios.get(Vue.prototype.serverUrl + 'transaction/list', {
      params: params
    })
  },
  desk: function (params) {
    return Vue.prototype.$axios.get(Vue.prototype.serverUrl + 'transaction/desks', {
      params: params
    })
  },
  deskout: function (params) {
    return Vue.prototype.$axios.get(Vue.prototype.serverUrl + 'transaction/deskout', {
      params: params
    })
  },
  deskin: function (params) {
    return Vue.prototype.$axios.get(Vue.prototype.serverUrl + 'transaction/deskin', {
      params: params
    })
  },
  transfer: function (params) {
    return Vue.prototype.$axios.get(Vue.prototype.serverUrl + 'transaction/transfer', {
      params: params
    })
  }
}
