import Vue from 'vue'

export default {
  info: function () {
    return Vue.prototype.$axios.get(Vue.prototype.serverUrl + 'user/info')
  },
  logout: function () {
    return Vue.prototype.$axios.get(Vue.prototype.serverUrl + 'user/logout')
  },
  name: function () {
    return Vue.prototype.$axios.get(Vue.prototype.serverUrl + 'user/name')
  },
  token: function (params) {
    return Vue.prototype.$axios.get(Vue.prototype.serverUrl + 'user/token', {
      params: params
    })
  },
  setfundpin: function (params) {
    return Vue.prototype.$axios.get(Vue.prototype.serverUrl + 'user/fundpin', {
      params: params
    })
  },
  mobileCode: function (params) {
    return Vue.prototype.$axios.get(Vue.prototype.serverUrl + 'user/sendvcode', {
      params: params
    })
  },
  login: function (params) {
    return Vue.prototype.$axios.get(Vue.prototype.serverUrl + 'user/login', {
      params: params
    })
  },
  register: function (params) {
    return Vue.prototype.$axios.get(Vue.prototype.serverUrl + 'user/register', {
      params: params
    })
  },
  search: function (params) {
    return Vue.prototype.$axios.get(Vue.prototype.serverUrl + 'user/search', {
      params: params
    })
  },
  profile: function (params) {
    return Vue.prototype.$axios.get(Vue.prototype.serverUrl + 'user/profile', {
      params: params
    })
  }
}
