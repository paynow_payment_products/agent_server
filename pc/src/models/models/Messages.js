import Vue from 'vue'

export default {
  list: function (params) {
    return Vue.prototype.$axios.get(Vue.prototype.serverUrl + 'messages/list', {
      params: params
    })
  }
}
