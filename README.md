Pn Pay All In One Version

1, 建一个默认站点，
指向路径 ./frontend
1.1 首页 /
说明：强制首页下所有请求指向根目录的index.html
location / {
    try_files $uri $uri/ /index.html?$query_string;
}

1.2 /m 移动端前端 纯静态
说明：强制所有请求指向/m子目录下的index.html
location /m {
            try_files $uri $uri/ /m/index.html?$query_string;
        }

1.3 /pc pc端前端 纯静态
说明：强制所有请求指向/pc子目录下的index.html
location /pc {
            try_files $uri $uri/ /pc/index.html?$query_string;
        }

1.4 /api 接口 
说明：
配置一个支持php的站点用于执行php，指向 backend/public/
配置反向代理到该子站点，则api和静态页面同域

1.5 /uploads 上传文件的地址
说明：
单独配置一个纯静态站点，指向 uploads 目录，用于展示用户上传的文件，注意，该站点不能有应用程序的执行能力
配置反代到这个子站点
程序里要存放客户文件就可以放在这个路径下

1.6 /manage 管理后台
需要配置白名单，通过nginx来限制ip即可

这样做的好处是一个域名全部跑完，最前面可以走cdn来隐藏服务器IP，或者通过多前端分散流量