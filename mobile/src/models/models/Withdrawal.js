/**
 * Created by roger.s on 2017/7/4.
 */

import Vue from 'vue'

export default {
  create: function (params) {
    return Vue.http.get(Vue.prototype.serverUrl + 'withdrawal/create', {
      params: params
    })
  },
  info: function (params) {
    return Vue.http.get(Vue.prototype.serverUrl + 'withdrawal/info', {
      params: params
    })
  },
  list: function (params) {
    return Vue.http.get(Vue.prototype.serverUrl + 'withdrawal/list', {
      params: params
    })
  }
}
