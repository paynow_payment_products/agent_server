import Vue from 'vue'

export default {
  list: function (params) {
    return Vue.http.get(Vue.prototype.serverUrl + 'messages/list', {
      params: params
    })
  }
}
