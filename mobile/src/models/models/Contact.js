import Vue from 'vue'

export default {
  add: function (params) {
    return Vue.http.get(Vue.prototype.serverUrl + 'feedback/report', {
      params: params
    })
  }
}
