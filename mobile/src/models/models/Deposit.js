/**
 * Created by roger.s on 2017/7/4.
 */

import Vue from 'vue'

export default {
  platforms: function (params) {
    return Vue.http.get(Vue.prototype.serverUrl + 'deposit/platforms', {
      params: params
    })
  },
  banks: function (params) {
    return Vue.http.get(Vue.prototype.serverUrl + 'deposit/banks', {
      params: params
    })
  },
  create: function (params) {
    return Vue.http.get(Vue.prototype.serverUrl + 'deposit/create', {
      params: params
    })
  },
  list: function (params) {
    return Vue.http.get(Vue.prototype.serverUrl + 'deposit/list', {
      params: params
    })
  }
}
