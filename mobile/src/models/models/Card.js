import Vue from 'vue'

export default {
  add: function (params) {
    return Vue.http.get(Vue.prototype.serverUrl + 'card/add', {
      params: params
    })
  },
  bank: function (params) {
    return Vue.http.get(Vue.prototype.serverUrl + 'card/bank', {
      params: params
    })
  },
  list: function (params) {
    return Vue.http.get(Vue.prototype.serverUrl + 'card/list', {
      params: params
    })
  },
  delete: function (params) {
    return Vue.http.get(Vue.prototype.serverUrl + 'card/delete', {
      params: params
    })
  },
  check: function (params) {
    return Vue.http.get(Vue.prototype.serverUrl + 'card/check', {
      params: params
    })
  },
  bankList: function () {
    return Vue.http.get(Vue.prototype.serverUrl + 'bank/list')
  }
}
