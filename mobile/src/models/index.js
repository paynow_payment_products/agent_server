import User from './models/User'
import Card from './models/Card'
import Transaction from './models/Transaction'
import Deposit from './models/Deposit'
import Withdrawal from './models/Withdrawal'
import Messages from './models/Messages'
import Contact from './models/Contact'
import Md5 from 'crypto-js/md5'

var serverUrl = '/api/'
if (window.location.host === 'pc.test.f2d.me' || window.location.host === 'c.test.f2d.me') {
  serverUrl = '//api.test.sc/api/'
}
if (window.location.host === 'localhost:1111') {
  serverUrl = 'https://www.minghui4.top/api/'
}

const models = {
  User,
  Card,
  Transaction,
  Deposit,
  Withdrawal,
  Messages,
  Contact
}
export default {
  install (Vue) {
    Vue.prototype.$models = models
    Vue.models = models
    Vue.prototype.serverUrl = serverUrl
    Vue.prototype.aesPassphrase = Md5('pn.sc.dist.api.aes.key' + Md5('20180601000000').toString()).toString()
  },
  $models: models
}

export const $models = models
