import UserCenter from '@/components/UserCenter'

import Card from '@/components/Card'
import CardList from '@/components/card/List'
import CardAdd from '@/components/card/Add'

import NoLogin from '@/components/NoLogin'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Forget from '@/components/Forget'

import deposit from './routes/deposit'
import withdrawal from './routes/withdrawal'
import transaction from './routes/transaction'
import messages from './routes/messages'
import contact from './routes/contact'
import funds from './routes/funds'

const routes = {
  routes: [
    {
      path: '/',
      name: 'index',
      component: UserCenter
    },
    {
      path: '/card',
      name: 'card',
      component: Card,
      alias: '',
      children: [
        {
          path: 'list',
          name: 'card list',
          component: CardList,
          alias: ''
        },
        {
          path: 'add',
          name: 'card add',
          component: CardAdd,
          alias: ''
        }
      ]
    },
    {
      path: '/not-login',
      component: NoLogin,
      alias: '*',
      unauth: true
    },
    {
      path: '/login',
      component: Login,
      unauth: true
    },
    {
      path: '/register',
      component: Register,
      unauth: true
    },
    {
      path: '/forget',
      component: Forget,
      unauth: true
    }
  ]
}

routes.routes = routes.routes.concat(deposit)
routes.routes = routes.routes.concat(withdrawal)
routes.routes = routes.routes.concat(transaction)
routes.routes = routes.routes.concat(messages)
routes.routes = routes.routes.concat(contact)
routes.routes = routes.routes.concat(funds)

export default routes
