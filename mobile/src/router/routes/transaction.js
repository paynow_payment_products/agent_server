import Transaction from '@/components/Transaction'
import TransactionList from '@/components/transaction/List'
import TransactionSearch from '@/components/transaction/Search'
import TransactionDesk from '@/components/transaction/Desk'
import TransactionDeskIn from '@/components/transaction/DeskIn'
import TransactionDeskOut from '@/components/transaction/DeskOut'
import TransactionTransfer from '@/components/transaction/Transfer'
import TransactionTransferDetal from '@/components/transaction/TransferDetal'
import TransactionDetal from '@/components/transaction/TransactionDetal'

export default {
  path: '/transaction',
  name: 'transaction',
  component: Transaction,
  children: [
    {
      path: 'list',
      name: 'transaction-list',
      component: TransactionList,
      alias: ''
    },
    {
      path: 'search',
      name: 'transaction-search',
      component: TransactionSearch
    },
    {
      path: 'Desk',
      name: 'desk',
      component: TransactionDesk
    },
    {
      path: 'DeskIn',
      name: 'desk-in',
      component: TransactionDeskIn
    },
    {
      path: 'DeskOut',
      name: 'desk-out',
      component: TransactionDeskOut
    },
    {
      path: 'Transfer',
      name: 'transfer',
      component: TransactionTransfer
    },
    {
      path: 'TransferDetal',
      name: 'transfer-detal',
      component: TransactionTransferDetal
    },
    {
      path: 'TransactionDetal',
      name: 'transaction-detal',
      component: TransactionDetal
    }
  ]
}
