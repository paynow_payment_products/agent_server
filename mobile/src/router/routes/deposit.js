import Deposit from '@/components/Deposit'
import DepositList from '@/components/deposit/List'
import DepositAdd from '@/components/deposit/Add'

export default {
  path: '/deposit',
  name: 'deposit',
  component: Deposit,
  children: [
    {
      path: 'add',
      name: 'deposit-add',
      component: DepositAdd,
      alias: ''
    },
    {
      path: 'list',
      name: 'deposit-list',
      component: DepositList
    }
  ]
}
