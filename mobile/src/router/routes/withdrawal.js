
import Withdrawal from '@/components/Withdrawal'
import WithdrawalList from '@/components/withdrawal/List'
import WithdraralAdd from '@/components/withdrawal/Add'
import WithdrawalDetail from '@/components/withdrawal/Detail'

export default {
  path: '/withdrawal',
  name: 'withdrawal',
  component: Withdrawal,
  children: [
    {
      path: 'list',
      name: 'withdrawal-list',
      component: WithdrawalList
    },
    {
      path: 'add',
      name: 'withdrawal-add',
      component: WithdraralAdd,
      alias: ''
    },
    {
      path: 'detail',
      name: 'withdrawal-detail',
      component: WithdrawalDetail
    }
  ]
}
