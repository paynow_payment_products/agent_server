
import Funds from '@/components/Funds'
import FundPin from '@/components/funds/Pin'
import FundsCode from '@/components/funds/Code'
import FundsReset from '@/components/funds/Reset'

export default {
  path: '/funds',
  name: 'funds',
  component: Funds,
  children: [
    {
      path: 'pin',
      name: 'fund-pin',
      component: FundPin
    },
    {
      path: 'code',
      name: 'funds-code',
      component: FundsCode
    },
    {
      path: 'reset',
      name: 'funds-reset',
      component: FundsReset
    }
  ]
}
