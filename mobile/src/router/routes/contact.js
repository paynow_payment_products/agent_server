import Contact from '@/components/Contact'
import ContactAdd from '@/components/contact/Add'
import ContactAccount from '@/components/contact/Account'

export default {
  path: '/contact',
  name: 'contact',
  component: Contact,
  children: [
    {
      path: 'add',
      name: 'contact-add',
      component: ContactAdd,
      alias: ''
    },
    {
      path: '/contact/account',
      name: 'contact-account',
      component: ContactAccount,
      alias: ''
    }
  ]
}
