import Messages from '@/components/Messages'
import MessagesList from '@/components/messages/List'
import MessagesSearch from '@/components/messages/Search'
import MessagesDetail from '@/components/messages/Detail'

export default{
  path: '/messages',
  name: 'messages',
  component: Messages,
  children: [
    {
      path: 'list',
      name: 'messages-list',
      component: MessagesList,
      alias: ''
    },
    {
      path: 'search',
      name: 'messages-search',
      component: MessagesSearch
    },
    {
      path: 'detail',
      name: 'messages-detail',
      component: MessagesDetail
    }
  ]
}
