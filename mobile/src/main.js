// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import {
  AjaxPlugin,
  AlertPlugin,
  ConfirmPlugin,
  DatetimePlugin,
  DevicePlugin,
  LoadingPlugin,
  ToastPlugin,
  Grid,
  GridItem,
  Group,
  GroupTitle,
  Cell,
  CellBox,
  ViewBox,
  XHeader,
  Tabbar,
  TabbarItem,
  XButton,
  XInput,
  Radio,
  Box,
  PopupRadio,
  Popup,
  Checker,
  CheckerItem,
  Datetime,
  PopupPicker,
  CheckIcon,
  Step,
  StepItem,
  Countdown,
  Toast,
  Badge,
  FormPreview,
  CellFormPreview,
  DatetimeRange,
  XTextarea,
  Divider,
  XImg,
  Msg,
  Flexbox,
  FlexboxItem,
  XTable,
  LoadMore,
  Confirm
} from 'vux'

Vue.use(AjaxPlugin)
Vue.use(AlertPlugin)
Vue.use(ConfirmPlugin)
Vue.use(DatetimePlugin)
Vue.use(DevicePlugin)
Vue.use(LoadingPlugin)
Vue.use(ToastPlugin)

Vue.component('confirm', Confirm)
Vue.component('group', Group)
Vue.component('grid', Grid)
Vue.component('grid-item', GridItem)
Vue.component('group-title', GroupTitle)
Vue.component('cell', Cell)
Vue.component('cell-box', CellBox)
Vue.component('view-box', ViewBox)
Vue.component('x-header', XHeader)
Vue.component('tabbar', Tabbar)
Vue.component('tabbar-item', TabbarItem)
Vue.component('x-button', XButton)
Vue.component('x-input', XInput)
Vue.component('radio', Radio)
Vue.component('box', Box)
Vue.component('popup-radio', PopupRadio)
Vue.component('popup', Popup)
Vue.component('checker', Checker)
Vue.component('checker-item', CheckerItem)
Vue.component('datetime', Datetime)
Vue.component('popup-picker', PopupPicker)
Vue.component('check-icon', CheckIcon)
Vue.component('step', Step)
Vue.component('step-item', StepItem)
Vue.component('countdown', Countdown)
Vue.component('toast', Toast)
Vue.component('badge', Badge)
Vue.component('form-preview', FormPreview)
Vue.component('cell-form-preview', CellFormPreview)
Vue.component('datetime-range', DatetimeRange)
Vue.component('x-textarea', XTextarea)
Vue.component('divider', Divider)
Vue.component('x-img', XImg)
Vue.component('msg', Msg)
Vue.component('flexbox', Flexbox)
Vue.component('flexbox-item', FlexboxItem)
Vue.component('x-table', XTable)
Vue.component('load-more', LoadMore)

import FastClick from 'fastclick'
FastClick.attach(document.body)

Vue.config.productionTip = false

import axios from 'axios'
axios.defaults.withCredentials = true

import Models from './models'
Vue.use(Models)

import Password from './components/withdrawal/Password'
Vue.use(Password)
Vue.component('password', Password)

import CodeInput from './components/funds/CodeInput'
Vue.use(CodeInput)
Vue.component('code-input', CodeInput)

import XCodeInput from './components/funds/X-CodeInput'
Vue.use(XCodeInput)
Vue.component('xcode-input', XCodeInput)

import XCodeInput1 from './components/funds/X-CodeInput1'
Vue.use(XCodeInput1)
Vue.component('xcode-input1', XCodeInput1)

import Keyboard from './components/funds/Keyboard'
Vue.use(Keyboard)
Vue.component('keyboard', Keyboard)

import Vuex from 'vuex'
Vue.use(Vuex)

/* eslint-disable no-new */
import App from './App'
import router from './router'
new Vue({
  router,
  data: {
    currentRoute: window.location.pathname
  },
  render: h => h(App)
}).$mount('#app-box')
