<?php

namespace Core;
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 06:43
 */


use Illuminate\Filesystem\Filesystem;
use Illuminate\Translation\FileLoader;
use Illuminate\Translation\Translator as T;
class Translator
{
    use Singleton;

    private $translator;
    private function __construct()
    {
        $file = new Filesystem();
        $fileLoader = new FileLoader($file, APPLICATION_PATH . '/lang');
        $this->translator = new T($fileLoader, 'zh-CN');
    }

    public function getFromJson($key, array $replace = [], $locale = null)
    {
        if (strpos($key, '.') === false)
        {
            $key = 'basic.' . $key;
        }
        return $this->translator->getFromJson($key, $replace, $locale);
    }
}