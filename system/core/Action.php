<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 7/4/15
 * Time: 21:47
 */

namespace Core;

use Dispatcher;
use Request;
use Session;

abstract class Action extends \Yaf_Action_Abstract
{
    abstract protected function run();
    final public function execute()
    {
        foreach (get_class_methods(get_called_class()) as $methodName) {
            if (strpos($methodName, '_init') !== false) {
                $this->$methodName();
            }
        }
        $this->beforeRun();
        return $this->run();
    }

    protected $disableView = false;
    final protected function _initDisableView()
    {
        if ($this->disableView || Request::isCli()) {
            Dispatcher::disableView();
        }
    }

    protected $session;
    final protected function _initSession()
    {
        $this->session = Session::getInstance();
    }

    final protected function assign($name, $value = null)
    {
        return $this->getView()->assign($name, $value);
    }

    public function session()
    {
        return $this->getController()->session;
    }

    public function beforeRun()
    {
        $this->assign('Controller', strtolower(str_replace('Controller', '', get_class($this->getController()))));
    }
}
