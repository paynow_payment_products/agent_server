<?php
/**
 * custom function helpers
 *
 * @param null $obj
 */

function pr($obj = null)
{
    echo '<pre>';
    print_r($obj);
    echo '</pre>';
}

function vd()
{
    echo '<pre>';
    foreach (func_get_args() as $a) {
        var_dump($a);
    }
}

function get_client_ip()
{
    if (isset($_SERVER['HTTP_CLIENT_IP']) && !empty($_SERVER['HTTP_CLIENT_IP'])) {
        return $_SERVER['HTTP_CLIENT_IP'];
    }
    if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        return strtok($_SERVER['HTTP_X_FORWARDED_FOR'], ',');
    }
    if (isset($_SERVER['HTTP_PROXY_USER']) && !empty($_SERVER['HTTP_PROXY_USER'])) {
        return $_SERVER['HTTP_PROXY_USER'];
    }
    if (isset($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['REMOTE_ADDR'])) {
        return $_SERVER['REMOTE_ADDR'];
    } else {
        return "0.0.0.0";
    }
}

function generateVCode($iLength = 4)
{
    return str_pad(rand(0, pow(10, $iLength) - 1), $iLength, '0', STR_PAD_LEFT);
}

function num10to62($num)
{
    if (!is_int($num) ? (ctype_digit($num)) : true) {
        $to   = 62;
        $dict = 'qpwoeirutyalskdjfhgzmxncbv1236547890QWERTYUIOPLKJHGFDSABVNCMXZ';
        $ret  = '';
        do {
            $ret = $dict[bcmod($num, $to)] . $ret;
            $num = bcdiv($num, $to);
        } while ($num > 0);

        return $ret;
    }

    return "";
}

function num62to10($num)
{
    $from = 62;
    $num  = strval($num);
    $dict = 'qpwoeirutyalskdjfhgzmxncbv1236547890QWERTYUIOPLKJHGFDSABVNCMXZ';
    $len  = strlen($num);
    $dec  = 0;
    for ($i = 0; $i < $len; $i++) {
        $pos = strpos($dict, $num[$i]);
        $dec = bcadd(bcmul(bcpow($from, $len - $i - 1), $pos), $dec);
    }

    return $dec;
}

function milliseconds()
{
    return (int) (microtime(true) * 1000);
}

function getAllParams()
{
    $params = Request::getParams();
    foreach ($_GET as $key => $value) {
        $params[$key] = Request::get($key);
    }
    foreach ($_POST as $key => $value) {
        $params[$key] = Request::getPost($key);
    }

    return $params;
}

function get($url)
{
    $curl = new Curl\Curl();
    $curl->setOpt(CURLOPT_CONNECTTIMEOUT, 25);
    $curl->setOpt(CURLOPT_TIMEOUT, 25);
    $curl->get($url);
    if ($curl->error) {
        return '';
    }
    $response = $curl->response;
    $curl->close();
    $curl = null;
    unset($curl);

    return $response;
}

function post($url, $data)
{
    $curl = new Curl\Curl();
    $curl->setOpt(CURLOPT_CONNECTTIMEOUT, 120);
    $curl->setOpt(CURLOPT_TIMEOUT, 120);
    $curl->post($url, $data);
    if ($curl->error) {
        return '';
    }
    $response = $curl->response;
    $curl->close();
    $curl = null;
    unset($curl);

    return $response;
}

function amount_format($value)
{
    return number_format($value, 2, '.', '');
}

if (!function_exists('__')) {
    function __($key = null, $replace = [], $locale = null)
    {
        return \Core\Translator::getInstance()->getFromJson($key, $replace, $locale);
    }
}

if (!function_exists('safe_card_no')) {
    function safe_card_no($card_no)
    {
        return trim(substr('**** **** **** **** **** ****', 0, (strlen($card_no) - 4 + ((strlen($card_no) - 4) / 4)))) . ' ' . substr($card_no, strlen($card_no) - 4, 4);
    }
}

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float) $usec + (float) $sec);
}

function echoResponseData($responseData)
{
    if (is_array($responseData)) {
        // foreach ($responseData as $key => $value) {
        //     echo $key . " : " . htmlspecialchars($value) . "<br />";
        // }
        pr($responseData);
    } else if (is_array(json_decode($responseData, true))) {
        pr(json_decode($responseData, true));
    } else {
        echo htmlentities($responseData);
    }
}

function mobile_format($value)
{
    return empty($value) ? '' : substr($value, 0, 3) . "****" . substr($value, strlen($value) - 4);
}
