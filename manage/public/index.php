<?php
date_default_timezone_set("Asia/Shanghai");
if(isset($_SERVER["HTTP_ORIGIN"])) {
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Origin:'.$_SERVER["HTTP_ORIGIN"]);
}

define('MAIN_PATH', dirname(dirname(dirname(__FILE__))));
define('APPLICATION_PATH', dirname(dirname(__FILE__)));
define('FRAMEWORK_PATH', MAIN_PATH . '/system');

require APPLICATION_PATH . '/conf/conf.php';
require FRAMEWORK_PATH . '/library/Functions.php';

if (file_exists(MAIN_PATH . '/vendor/autoload.php')) {
    Yaf_Loader::import(MAIN_PATH . '/vendor/autoload.php');
}

if (file_exists(FRAMEWORK_PATH . '/vendor/autoload.php')) {
    Yaf_Loader::import(FRAMEWORK_PATH . '/vendor/autoload.php');
}

$application = new Yaf_Application(APPLICATION_MAIN_CONFIG);
if ($application->environ() == 'development') {
    error_reporting(E_ALL);
    ini_set('display_errors', true);
    ini_set('display_startup_errors', true);
}

set_exception_handler(function ($e){\Lib\Error::error($e);});

if (PHP_SAPI == 'cli') {
    define('RUNTIME_DIR',
        FRAMEWORK_PATH . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR);
    define('ENDL', "\n");
    $request     = new Yaf_Request_Simple("CLI", $uri[0], $uri[1], $uri[2], $data['params']);
    $application->bootstrap()->getDispatcher()->dispatch($request);
    echo ENDL . ENDL;
} else {
    header("Content-Type:text/html;charset=utf-8");
    define('ENDL', '<br />');
    $application->bootstrap()->run();
}