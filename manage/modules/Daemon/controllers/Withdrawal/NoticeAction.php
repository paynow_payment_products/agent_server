<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/17
 * Time: 03:20
 */

use Withdrawal\NoticeModel;
use Carbon\Carbon;
use Lib\Queue;

class NoticeAction extends BaseAction
{
    public function run()
    {
        $start = time();
        while (1) {
            // restart every one hour.
            if (time() > ($start + (60 * 60))) {
                exit;
            }

            $this->createNotice();

            if ( ! Request::isCli()) {
                exit;
            }

            sleep(10);
        }
    }

    private function createNotice()
    {
        $notices = NoticeModel::where('status', 0)->where('start_at', '<=', Carbon::now())
                              ->take(100)
                              ->get();

        foreach ($notices as $notice)
        {
            $notice_id = $notice->notice_id;
            $notice->delete();

            // call service
            Queue::add('command/withdrawal/notify', [
                'notice_id' => $notice_id,
            ]);
        }
    }

}