<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/17
 * Time: 03:20
 */

use Carbon\Carbon;
use Deposit\QueryModel;
use Lib\Queue;

class QueryAction extends BaseAction
{
    public function run()
    {
        $start = time();
        while (1) {
            // restart every one hour.
            if (time() > ($start + (60 * 60))) {
                exit;
            }

            $this->createQuery();

            if (!Request::isCli()) {
                exit;
            }

            sleep(10);
        }
    }

    private function createQuery()
    {
        $querys = QueryModel::where('status', 0)->where('start_at', '<=', Carbon::now())
            ->take(100)
            ->get();

        foreach ($querys as $query) {
            $query_id = $query->query_id;
            $query->delete();

            // call service
            Queue::add('command/deposit/query', [
                'query_id' => $query_id,
            ]);
        }
    }

}
