<?php

use Carbon\Carbon;
use Deposit\DepositModel as Deposit;
use Illuminate\Database\Capsule\Manager as DB;
use Transaction\DailyreportModel as Report;
use Withdrawal\WithdrawalModel as Withdrawal;

/**
 * 每日首充人数统计
 */
class DailyreportAction extends BaseAction
{
    public function run()
    {
        set_time_limit(0);
        $startTime = milliseconds();

        $params = Request::getParams();

        // 默认统计前一天的数据
        $date = empty($params['argv'][2]) ? Carbon::yesterday()->format('Y-m-d') : $params['argv'][2];

        $start = $date . " 00:00:00";
        $end   = $date . " 23:59:59";

        // echo $date . "\n";

        $oReport = Report::firstOrcreate([
            'date' => $date,
        ]);

        $oDepositObj = Deposit::doWhere([
            'created_at' => ['between', [$start, $end]],
            'status'     => ['=', 1],
        ]);

        // 系统充值总金额
        $sysDepositSumSql = 'SELECT SUM(d.amount) dsum FROM pn_deposit.deposits d LEFT JOIN pn_user.users u ON d.user_id = u.user_id WHERE d.`status` = 1 AND is_admin_add_gold = 0 AND d.created_at BETWEEN "' . $start . '" AND "' . $end . '"';
        $sysDepositSum    = DB::select($sysDepositSumSql);
        $sysDepositSum    = (empty($sysDepositSum) || empty($sysDepositSum[0]->dsum)) ? 0 : $sysDepositSum[0]->dsum;
        // echo $sysDepositSum . "\n";

        echo "[" . date('Y-m-d H:i:s') . "] finished sysDepositSumSql cost: " . (milliseconds() - $startTime), "ms \n";

        // 人工加币充值总金额
        $adminAddGoldDepositSumSql = 'SELECT SUM(d.amount) dsum FROM pn_deposit.deposits d LEFT JOIN pn_user.users u ON d.user_id = u.user_id WHERE d.`status` = 1 AND is_admin_add_gold = 1 AND d.created_at BETWEEN "' . $start . '" AND "' . $end . '"';
        $adminAddGoldDepositSum    = DB::select($adminAddGoldDepositSumSql);
        $adminAddGoldDepositSum    = (empty($adminAddGoldDepositSum) || empty($adminAddGoldDepositSum[0]->dsum)) ? 0 : $adminAddGoldDepositSum[0]->dsum;
        // echo $adminAddGoldDepositSum . "\n";

        echo "[" . date('Y-m-d H:i:s') . "] finished adminAddGoldDepositSumSql cost: " . (milliseconds() - $startTime), "ms \n";

        // 局头打款、人工充值
        $admindepositSumSql = 'SELECT SUM(amount) dsum FROM pn_transaction.gold_operate g WHERE optype_id = 17 AND created_at BETWEEN "' . $start . '" AND "' . $end . '"';
        $admindepositSum    = DB::select($admindepositSumSql);
        $admindepositSum    = (empty($admindepositSum) || empty($admindepositSum[0]->dsum)) ? 0 : $admindepositSum[0]->dsum;
        // echo $admindepositSumSql . "\n";
        echo "[" . date('Y-m-d H:i:s') . "] finished admindepositSumSql cost: " . (milliseconds() - $startTime), "ms \n";

        $depositSum = $sysDepositSum + $adminAddGoldDepositSum + $admindepositSum;

        // 充值人数
        $depositNum = $oDepositObj->groupBy('user_id')->get()->count();
        // echo $depositNum . "\n";

        echo "[" . date('Y-m-d H:i:s') . "] finished depositNum cost: " . (milliseconds() - $startTime), "ms \n";

        $oWithdrawalObj = Withdrawal::doWhere([
            'created_at' => ['between', [$start, $end]],
            'status'     => ['=', 1],
        ]);

        // 提现总金额
        $withdrawalSum = $oWithdrawalObj->sum('amount');
        // echo $withdrawalSum . "\n";
        echo "[" . date('Y-m-d H:i:s') . "] finished withdrawalSum cost: " . (milliseconds() - $startTime), "ms \n";

        // 提现手续费总金额
        $withdrawalFeeSum = $oWithdrawalObj->sum('fee');
        // echo $withdrawalFeeSum . "\n";
        echo "[" . date('Y-m-d H:i:s') . "] finished withdrawalFeeSum cost: " . (milliseconds() - $startTime), "ms \n";

        // 提现总人数
        $withdrawalNum = $oWithdrawalObj->groupBy('user_id')->get()->count();
        // echo $withdrawalNum . "\n";
        echo "[" . date('Y-m-d H:i:s') . "] finished withdrawalNum cost: " . (milliseconds() - $startTime), "ms \n";

        // 人工出款
        $manualwithdrawalSum = Withdrawal::doWhere([
            'created_at'  => ['between', [$start, $end]],
            'status'      => ['=', 1],
            'platform_id' => ['=', 7],
        ])->sum('amount');
        // echo $manualwithdrawalSum . "\n";
        echo "[" . date('Y-m-d H:i:s') . "] finished manualwithdrawalSum cost: " . (milliseconds() - $startTime), "ms \n";

        // 当天游戏人数  排除培玩号 财务号
        $playerNumSql = 'SELECT COUNT(*) num FROM ( SELECT u.user_id, app_user_id, `name`, club_id, delta, remain, optype_id FROM pn_transaction.transactions t LEFT JOIN pn_user.users u ON t.user_id = u.user_id WHERE t.optype_id = 3 AND t.optime BETWEEN "' . $start . '" AND "' . $end . '" AND delta < 0 GROUP BY u.app_user_id) as tmp1';
        $playerNum    = DB::select($playerNumSql);
        $playerNum    = (empty($playerNum) || empty($playerNum[0]->num)) ? 0 : $playerNum[0]->num;
        // echo $playerNum . "\n";
        echo "[" . date('Y-m-d H:i:s') . "] finished playerNumSql cost: " . (milliseconds() - $startTime), "ms \n";

        // 查询百乐汇活动奖励
        $rewardSumSql = 'SELECT SUM(delta) num FROM transactions t LEFT JOIN pn_user.users u ON t.user_id = u.user_id WHERE optime BETWEEN "' . $start . '" AND "' . $end . '" AND optype_id IN (6, 9, 10, 11,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,43) AND u.app_user_id != 5655';
        $rewardSum    = DB::select($rewardSumSql);
        $rewardSum    = (empty($rewardSum) || empty($rewardSum[0]->num)) ? 0 : $rewardSum[0]->num;
        echo "[" . date('Y-m-d H:i:s') . "] finished rewardSumSql cost: " . (milliseconds() - $startTime), "ms \n";

        // 每日新增首充人数
        $firstDepositNumSql = 'SELECT COUNT(*) AS num FROM (SELECT u.app_user_id, d.amount, d.created_at, d.`status` FROM pn_deposit.deposits d LEFT JOIN pn_user.users u ON d.user_id = u.user_id WHERE `status` = 1 AND u.app_user_id > 6000 GROUP BY u.app_user_id ORDER BY u.app_user_id ASC) AS tmp1 WHERE tmp1.created_at BETWEEN "' . $start . '" AND "' . $end . '"';
        $count3             = DB::select($firstDepositNumSql);
        $firstDepositNum    = (empty($count3) || empty($count3[0]->num)) ? 0 : $count3[0]->num;
        echo "[" . date('Y-m-d H:i:s') . "] finished firstDepositNumSql cost: " . (milliseconds() - $startTime), "ms \n";

        // 用户红包
        $hongbaoSumSql = 'SELECT SUM(delta) num FROM transactions t WHERE optype_id = 46 AND delta > 0 AND created_at BETWEEN "' . $start . '" AND "' . $end . '"';
        $hongbaoSum    = DB::select($hongbaoSumSql);
        $hongbaoSum    = (empty($hongbaoSum) || empty($hongbaoSum[0]->num)) ? 0 : $hongbaoSum[0]->num;
        // echo $hongbaoSum . "\n";
        echo "[" . date('Y-m-d H:i:s') . "] finished hongbaoSumSql cost: " . (milliseconds() - $startTime), "ms \n";

        // 牌局服务费
        $serviceFeeSumSql = 'SELECT SUM(delta) num FROM transactions t WHERE optype_id = 12 AND created_at BETWEEN "' . $start . '" AND "' . $end . '"';
        $serviceFeeSum    = DB::select($serviceFeeSumSql);
        $serviceFeeSum    = (empty($serviceFeeSum) || empty($serviceFeeSum[0]->num)) ? 0 : $serviceFeeSum[0]->num;
        // echo $serviceFeeSum . "\n";
        echo "[" . date('Y-m-d H:i:s') . "] finished serviceFeeSumSql cost: " . (milliseconds() - $startTime), "ms \n";

        // 积分兑换钻石
        $changeDiamondSql = 'SELECT SUM(delta) num FROM transactions t WHERE optype_id = 44 AND created_at BETWEEN "' . $start . '" AND "' . $end . '"';
        $changeDiamondSum = DB::select($changeDiamondSql);
        $changeDiamondSum = (empty($changeDiamondSum) || empty($changeDiamondSum[0]->num)) ? 0 : $changeDiamondSum[0]->num;
        // echo $changeDiamondSum . "\n";
        echo "[" . date('Y-m-d H:i:s') . "] finished changeDiamondSql cost: " . (milliseconds() - $startTime), "ms \n";

        // exit;
        $oReport->update([
            'first_deposit_num'     => $firstDepositNum,
            'player_num'            => $playerNum,
            'deposit_num'           => $depositNum,
            'system_deposit_sum'    => $sysDepositSum,
            'admin_deposit_sum'     => $admindepositSum,
            'admin_setsucc_sum'     => $adminAddGoldDepositSum,
            'withdrawal_num'        => $withdrawalNum,
            'admin_withdrawal_sum'  => $manualwithdrawalSum,
            'system_withdrawal_sum' => $withdrawalSum - $manualwithdrawalSum,
            'deposit_sum'           => $depositSum,
            'withdrawal_sum'        => $withdrawalSum,
            'fee_sum'               => $withdrawalFeeSum,
            'reward_sum'            => $rewardSum,
            'user_hongbao'          => $hongbaoSum,
            'service_fee'           => abs($serviceFeeSum),
            'change_diamond_sum'    => abs($changeDiamondSum),
        ]);

        $date = date('Y-m-d H:i:s');
        echo "[$date] finished daliy report report_id : $oReport->id";
        return true;
    }
}
