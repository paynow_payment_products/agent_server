<?php

use Carbon\Carbon;
use Deposit\ReportModel as Report;
use Illuminate\Database\Capsule\Manager as DB;

class DepositAction extends BaseAction
{
    public function run()
    {
        set_time_limit(0);
        $startTime = milliseconds();

        $params = Request::getParams();

        // pr($params);

        if (empty($params['argv'][2]) && empty($params['argv'][3])) {
            $start = Carbon::now()->format('Y-m-d 00:00:00');
            $end   = Carbon::now()->format('Y-m-d H:i:s');
        } else if (empty($params['argv'][3]) && !empty($params['argv'][2])) {
            $start = $params['argv'][2] . " 00:00:00";
            $end   = $params['argv'][2] . " 23:59:59";
        } else if (!empty($params['argv'][3]) && !empty($params['argv'][2])) {
            $start = $params['argv'][2] . " 00:00:00";
            $end   = $params['argv'][3] . " 23:59:59";
        }

        // pr($start);
        // pr($end);
        // exit;

        $data = DB::table('pn_deposit.deposits as d')
            ->select(DB::raw("d.platform_id, p.`name`, date_format(d.created_at, '%Y%m%d') as ddays, sum(d.amount) as dsum, count(1) as dnum, sum(d.fee * 100) as fsum"))
            ->leftjoin('pn_deposit.platforms as p', 'p.platform_id', '=', 'd.platform_id')
            ->whereRaw('d.`status`=1 and d.created_at between "' . $start . '" and "' . $end . '"')
            ->groupBy(DB::raw("date_format(d.created_at, '%Y%m%d'), d.platform_id"))
            ->orderByRaw('ddays asc, d.platform_id asc')
            ->get();

        // pr($data);

        if ($data->isEmpty()) {
            exit("[" . date('Y-m-d H:i:s') . "] No data From $start to $end, cost: " . (milliseconds() - $startTime) . "ms \n");
        }

        foreach ($data as $value) {
            $oReport = Report::firstOrcreate([
                'date'        => $value->ddays,
                'platform_id' => $value->platform_id,
            ]);

            $oReport->update([
                'dsum' => $value->dsum,
                'fsum' => $value->fsum,
                'dnum' => $value->dnum,
            ]);
        }

        echo "[" . date('Y-m-d H:i:s') . "] Finished Report From $start to $end, cost: " . (milliseconds() - $startTime) . "ms \n";
    }
}
