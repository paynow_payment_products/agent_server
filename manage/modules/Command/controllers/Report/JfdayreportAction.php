<?php
use Carbon\Carbon;
use User\AccountModel;
use User\UserModel;
use Transaction\OptypeModel;
use User\AccountReportModel;

class JfdayreportAction extends BaseAction
{

    public function run()
    {
 
        $startTime = milliseconds();
    //    echo "[" . date('Y-m-d H:i:s') . "] task start \n";
        $params = Request::getParams();
        $date   = empty($params['argv'][2]) ? Carbon::yesterday()->format('Y-m-d') : $params['argv'][2];
        $start   = $date . " 00:00:00";
        $end     = $date . " 23:59:59";
        $user_info =UserModel::leftjoin('accounts',function($join){
            $join->on('users.user_id','=','accounts.user_id');          //关联id
        })->select('users.user_id','users.name','users.username','accounts.balance')->get();
        $users=[];
     //   $optype= OptypeModel::get();
     //帐变类型
        $transaction_type=[
            1 =>'recharge',
            2 =>'withdraw',
            3 =>'turnout',
            4 =>'turnin',
            5 =>'systemadd',
            6 =>'systemdel',
            7 =>'reback',
            8 =>'testadd',
            9 =>'testdel'
        ];
       foreach($user_info as $key=>$v){
            $zbdata = AccountReportModel::zbcount($v['user_id'],$start,$end);
            $users[]=['user_id'=>$v['user_id'],'name'=>$v['name'],'balance'=>$v['balance'],'transaction_data'=>$zbdata];
       }

        if ($users) {
            foreach($users as $v){
               $transaction=[];
               if(is_array($v['transaction_data'])){
                foreach($v['transaction_data'] as $d){
                    $transaction[$transaction_type[$d['type_id']]] = $d['a_total'];
                }
               }
            $dailyReport = AccountReportModel::firstOrcreate(
                [
                    'date' => $date,
                    'user_id' => $v['user_id'],
                ]
            );
            $dailyReport->update(
                [
                    'balance' => $v['balance'],
                    'name'=>$v['name'],
                    'recharge'=>key_exists('recharge', $transaction)?$transaction['recharge']:0,
                    'withdraw'=>key_exists('withdraw', $transaction)?$transaction['withdraw']:0,
                    'turnout'=>key_exists('turnout', $transaction)?$transaction['turnout']:0,
                    'turnin'=>key_exists('turnin', $transaction)?$transaction['turnin']:0,
                    'systemadd'=>key_exists('systemadd', $transaction)?$transaction['systemadd']:0,
                    'systemdel'=>key_exists('systemdel', $transaction)?$transaction['systemdel']:0,
                    'reback'=>key_exists('reback', $transaction)?$transaction['reback']:0,
                    'testadd'=>key_exists('testadd', $transaction)?$transaction['testadd']:0,
                    'testdel'=>key_exists('testdel', $transaction)?$transaction['testdel']:0,
                ]
            );
            }
            
            echo "[" . date('Y-m-d H:i:s') . "] finished " . (milliseconds() - $startTime), "ms \n";
        } else {
            echo "[" . date('Y-m-d H:i:s') . "] no date " . (milliseconds() - $startTime), "ms \n";
        }
    }

}
