<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/17
 * Time: 14:52
 */

use Carbon\Carbon;
use Withdrawal\WithdrawalModel;
use Withdrawal\QueryModel;

class QueryAction extends BaseAction
{

    private $task_rule = [
        '5',
        '10',
        '20',
        '30',
        '45',
        '60',
        '80',
        '100',
        '120',
        '180',
        '240',
        '480',
        '540',
        '600',
        '720',
    ];

    public function run()
    {
        if (empty($queryId = Request::get('query_id'))) {
            throw new Exception('error : withdrawal query id empty');
        }
        if (empty($query = QueryModel::withTrashed()->find($queryId))) {
            throw new Exception('error withdrawal query : query_id = ' . $queryId);
        }
        // check status
        if ($query->status != QueryModel::STATUS_NEW) {
            throw new Exception("error withdrawal query status : query_id = $queryId | status = $query->status");
        }
        $query->update([
            'status' => QueryModel::STATUS_WORKING,
        ]);

        $withdrawal = WithdrawalModel::find($query->withdrawal_id);

        if ($withdrawal->status != WithdrawalModel::WITHDRAWAL_STATUS_TRANSFER_START) {
            $query->update([
                'status' => QueryModel::STATUS_SUCCESS,
            ]);
            throw new Exception("error withdrawal status : withdrawal_id = $withdrawal->withdrawal_id | status = $withdrawal->status");
        }

        $oPatment = $withdrawal->platform->getPlatformAdapter();

        // 0 表示任务要从新发起  1 表示任务完成
        $result   = $oPatment->cliWithdrawalQuery($withdrawal, $query);

        if ($result == 0) {
            // save status
            $query->update([
                'status' => QueryModel::STATUS_SUCCESS,
            ]);

            // count notify times and create new query
            $withdrawal_query_times = $withdrawal->querys()->withTrashed()->count();
            if (isset($this->task_rule[$withdrawal_query_times])) {
                $start_at = $withdrawal->created_at->addMinutes($this->task_rule[$withdrawal_query_times]);
                $start_at = ($start_at >= Carbon::now()) ? $start_at : Carbon::now();

                QueryModel::create([
                    'withdrawal_id' => $withdrawal->withdrawal_id,
                    'start_at'   => $start_at,
                    'status'     => QueryModel::STATUS_NEW,
                ]);
                throw new Exception('withdrawal not finished | created new withdrawal query task.');
            }
            throw new Exception('query at max times | stop keeping query withdrawal status');
        }

        // update status
        $query->update([
            'status' => QueryModel::STATUS_SUCCESS,
        ]);

        $date = date('Y-m-d H:i:s');
        echo "[$date] finished withdrawal query | withdrawal id : $withdrawal->withdrawal_id";
        return true;
    }
}
