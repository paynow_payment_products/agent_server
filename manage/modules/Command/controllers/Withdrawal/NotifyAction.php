<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/17
 * Time: 19:37
 */

use Withdrawal\NoticeModel;
use Withdrawal\WithdrawalModel;

class NotifyAction extends BaseAction
{
    public function run()
    {
        if (empty($notice_id = Request::get('notice_id'))) {
            throw new Exception('error: withdrawal notice id empty');
        }
        if (empty($notice = NoticeModel::withTrashed()->find($notice_id))) {
            throw new Exception('error withdrawal notice : notice_id = ' . $notice_id);
        }
        // check status
        if ($notice->status != NoticeModel::STATUS_NEW) {
            throw new Exception("error withdrawal notice status : notice_id = $notice_id | status = $notice->status");
        }
        $notice->update([
            'status' => NoticeModel::STATUS_WORKING,
        ]);
        $withdrawal = WithdrawalModel::find($notice->withdrawal_id);

        if ($withdrawal->status != WithdrawalModel::WITHDRAWAL_STATUS_VERIFY_SUCCESS) {
            $notice->update([
                'status' => NoticeModel::STATUS_FAILED,
            ]);
            throw new Exception("error withdrawal status : withdrawal_id = $withdrawal->withdrawal_id | status = $withdrawal->status");
        }

        // 标记状态为开始转账
        $withdrawal->update([
            'status' => WithdrawalModel::WITHDRAWAL_STATUS_TRANSFER_START,
        ]);

        // call function
        $result = $withdrawal->platform->getPlatformAdapter()->cliWithdrawalRequest($withdrawal);

        // Update Notice Status
        if (!$result) {
            $notice->update([
                'status' => NoticeModel::STATUS_FAILED,
            ]);
            throw new Exception('call third withdrawal platform failed');
        }

        $notice->update([
            'status' => NoticeModel::STATUS_SUCCESS,
        ]);
        $date = date('Y-m-d H:i:s');
        echo "[$date] finished call third withdrawal id : $withdrawal->withdrawal_id" . PHP_EOL;

        // 提现渠道支持查询 则添加主动查询任务
        if ($withdrawal->platform->query_enabled == 1) {
            $date = date('Y-m-d H:i:s');
            if ($oQuery = $withdrawal->addQueryTask()) {
                echo "[$date] create queryTask success, query_id : $oQuery->query_id";
            } else {
                echo "[$date] create queryTask failed...";
            }
        }

        return true;
    }
}
