<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/17
 * Time: 14:52
 */

use Carbon\Carbon;
use Deposit\DepositModel;
use Deposit\NoticeModel;
use Lib\Pn\Sdk as PnSdk;

class NotifyAction extends BaseAction
{

    private $task_rule = [
        '0',
        '10',
        '30',
        '60',
        '240',
        '960',
    ];

    public function run()
    {
        if (empty($noticeId = Request::get('notice_id'))) {
            throw new Exception('error : deposit notice id empty');
        }
        if (empty($notice = NoticeModel::withTrashed()->find($noticeId))) {
            throw new Exception('error deposit notice : notice_id = ' . $noticeId);
        }
        // check status
        if ($notice->status != NoticeModel::STATUS_NEW) {
            throw new Exception("error deposit notice status : notice_id = $noticeId | status = $notice->status");
        }
        $notice->update([
            'status' => NoticeModel::STATUS_WORKING,
        ]);

        $deposit = DepositModel::find($notice->deposit_id);

        if ($deposit->status != DepositModel::DEPOSIT_STATUS_WAITINGLOAD) {
            $notice->update([
                'status' => NoticeModel::STATUS_FAILED,
            ]);
            throw new Exception("error deposit status : deposit_id = $deposit->deposit_id | status = $deposit->status");
        }

        // 5456财务号充值不加币
        if ($deposit->user_id == 47) {
            // update status
            $notice->update([
                'status' => NoticeModel::STATUS_SUCCESS,
            ]);
            $deposit->update([
                'status' => DepositModel::DEPOSIT_STATUS_SUCCESS,
            ]);

            $date = date('Y-m-d H:i:s');
            echo "[$date] finished deposit notice | deposit id : $deposit->deposit_id";
            return true;
        }

        // call remote
        $return = PnSdk::getInstance()->operateGold($deposit->user->app_user_id, $deposit->amount, PnSdk::GOLD_OPTYPE_DEPOSIT, $deposit->order_no);

        if (empty($return) || !isset($return['ret']) || $return['ret'] != 0) {
            // save status
            $notice->update([
                'status' => NoticeModel::STATUS_FAILED,
            ]);

            // count notify times and create new notice
            $deposit_notice_times = $deposit->notices()->withTrashed()->count();
            if (isset($this->task_rule[$deposit_notice_times])) {
                $start_at = $deposit->created_at->addMinutes($this->task_rule[$deposit_notice_times]);
                $start_at = ($start_at >= Carbon::now()) ? $start_at : Carbon::now();
                $start_at = $start_at->addMinutes(1);

                NoticeModel::create([
                    'deposit_id' => $deposit->deposit_id,
                    'start_at'   => $start_at,
                ]);
                throw new Exception('remote call back error | created new deposit notice task.');
            }
            throw new Exception('remote call back error | deposit status.');
        }

        // update status
        $notice->update([
            'status' => NoticeModel::STATUS_SUCCESS,
        ]);
        $deposit->update([
            'status' => DepositModel::DEPOSIT_STATUS_SUCCESS,
        ]);

        $date = date('Y-m-d H:i:s');
        echo "[$date] finished deposit notice | deposit id : $deposit->deposit_id";
        return true;
    }
}
