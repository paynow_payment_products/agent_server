<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/17
 * Time: 14:52
 */

use Carbon\Carbon;
use Deposit\DepositModel;
use Deposit\QueryModel;

class QueryAction extends BaseAction
{

    private $task_rule = [
        '0',
        '3',
        '5',
        '15',
    ];

    public function run()
    {
        if (empty($queryId = Request::get('query_id'))) {
            throw new Exception('error : deposit query id empty');
        }
        if (empty($query = QueryModel::withTrashed()->find($queryId))) {
            throw new Exception('error deposit query : query_id = ' . $queryId);
        }
        // check status
        if ($query->status != QueryModel::STATUS_NEW) {
            throw new Exception("error deposit query status : query_id = $queryId | status = $query->status");
        }
        $query->update([
            'status' => QueryModel::STATUS_WORKING,
        ]);

        $deposit = DepositModel::find($query->deposit_id);

        if ($deposit->status != DepositModel::DEPOSIT_STATUS_NEW) {
            $query->update([
                'status' => QueryModel::STATUS_SUCCESS,
            ]);
            throw new Exception("error deposit status : deposit_id = $deposit->deposit_id | status = $deposit->status");
        }

        $oPatment = $deposit->platform->getPlatformAdapter();
        $result   = $oPatment->actionDepositQuery($deposit, $query);

        if ($result == 0) {
            // save status
            $query->update([
                'status' => QueryModel::STATUS_SUCCESS,
            ]);

            // count notify times and create new query
            $deposit_query_times = $deposit->querys()->withTrashed()->count();
            if (isset($this->task_rule[$deposit_query_times])) {
                $start_at = $deposit->created_at->addMinutes($this->task_rule[$deposit_query_times]);
                $start_at = ($start_at >= Carbon::now()) ? $start_at : Carbon::now();

                QueryModel::create([
                    'deposit_id' => $deposit->deposit_id,
                    'start_at'   => $start_at,
                    'status'     => QueryModel::STATUS_NEW,
                ]);
                throw new Exception('deposit not finished | created new deposit query task.');
            }
            throw new Exception('query at max times | stop keeping query deposit status');
        }

        // update status
        $query->update([
            'status' => QueryModel::STATUS_SUCCESS,
        ]);

        $date = date('Y-m-d H:i:s');
        echo "[$date] finished deposit query | deposit id : $deposit->deposit_id";
        return true;
    }
}
