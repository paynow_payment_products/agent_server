<?php

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/17
 * Time: 04:49
 */

class WithdrawalController extends BaseCliController
{
    public $action_names = [
        'notify',
        'query',
    ];
}