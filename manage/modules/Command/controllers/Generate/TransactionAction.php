<?php

use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as DB;

/**
 * 每日首充人数统计
 */
class TransactionAction extends BaseAction
{
    private $filePath = APPLICATION_PATH . '/downloads/transactions/';

    public function run()
    {
        set_time_limit(0);
        $startTime = milliseconds();
        $params    = Request::getParams();

        // 默认统计前一天的数据
        $date           = empty($params['argv'][2]) ? Carbon::yesterday() : Carbon::parse($params['argv'][2]);
        $this->filePath = $this->filePath . $date->year . "/" . str_pad($date->month, 2, "0", STR_PAD_LEFT);
        $fileName       = $this->filePath . "/transactions_" . $date->toDateString() . ".csv";
        // pr($fileName);
        // vd(file_exists($this->filePath));exit();

        if (!file_exists($this->filePath)) {
            mkdir($this->filePath, 0755, true);
        }
        // exit();

        if (file_exists($fileName)) {
            $fp = fopen($fileName, "r+");
            if (flock($fp, LOCK_EX)) {
                ftruncate($fp, 0); // 将文件截断到给定的长度
                rewind($fp); // 倒回文件指针的位置
                flock($fp, LOCK_UN); //解锁
            } else {
                echo "[" . date('Y-m-d H:i:s') . "] file: {$fileName} locked\n";
                return false;
            }
        }
        echo "[" . date('Y-m-d H:i:s') . "] file: {$fileName} is clearn \n";

        $fp     = fopen($fileName, 'a');
        $header = "用户ID, 用户名, 俱乐部, 桌号, 操作时间, 金额, 剩余金币, 操作类型, 时间, 手数\n";
        fwrite($fp, $header);

        $start = $date->startOfDay()->toDateTimeString();
        $end   = $date->endOfDay()->toDateTimeString();

        // pr($start);
        // pr($end);
        // exit();

        $countsql = "select count(*) as num from pn_transaction.transactions where transactions.optime >= '{$start}' and transactions.optime <= '{$end}' order by optime asc";
        // pr($countsql);exit;

        $count = DB::select($countsql);

        if (empty($count) || empty($count[0]->num)) {
            echo "[" . date('Y-m-d H:i:s') . "] no data from $start to $end";
            return false;
        }

        $pre_count = 20000;
        for ($i = 0; $i < intval($count[0]->num / $pre_count) + 1; $i++) {

            $sql = "select users.app_user_id, users.name, clubs.club_name, desks.desk_name, transactions.optime, delta, remain, optypes.optype_name, transactions.created_at, transactions.hands from pn_transaction.transactions left join pn_user.users on transactions.user_id=users.user_id left join pn_user.clubs on transactions.club_id=clubs.club_id left join pn_user.desks on transactions.desk_id=desks.desk_id left join pn_transaction.optypes on transactions.optype_id=optypes.optype_id where transactions.optime >= '{$start}' and transactions.optime <= '{$end}'";

            $sql .= " order by optime asc limit " . $i * $pre_count . ",{$pre_count}";
            $list = DB::select($sql);

            if (empty($list)) {
                continue;
            }

            $str = "";
            foreach ($list as $info) {
                $str .= "$info->app_user_id, $info->name, $info->club_name, $info->desk_name, $info->optime, $info->delta, $info->remain, $info->optype_name, $info->created_at, $info->hands\n";
            }
            fwrite($fp, $str);
        }

        echo "[" . date('Y-m-d H:i:s') . "] finished generate transactions file: $fileName";
        fclose($fp);
    }
}
