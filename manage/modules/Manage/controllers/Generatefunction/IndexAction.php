<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/27
 * Time: 20:34
 */

use Manage\FunctionalitiesModel;
use Carbon\Carbon;

class IndexAction extends AdminBaseAction
{
    public $disableView = true;

    public function run()
    {
        $modules = $this->getModuleName();
        $dirPath = APPLICATION_PATH . '/modules/' . $modules . "/controllers/";
        $data = [];

        if (is_dir($dirPath)) {
            if ($dh = opendir($dirPath)) {
                while (($file = readdir($dh)) !== false) {
                    if ( $file != '.' && $file !='..' && is_dir($dirPath . $file) ) {
                        $controller = $file . "Controller";
                        // pr($controller);
                        $dh2 = opendir($dirPath . $file . '/');
                        while (($actions = readdir($dh2)) !== false) {
                            if ( $actions != '.' && $actions !='..' && is_file($dirPath . $file . '/' .$actions) ) {
                                $tmpArr = [
                                    'module' => $modules,
                                    'controller' => $controller,
                                    'action' => str_replace('.php', '', $actions),
                                    'created_at' => Carbon::now(),
                                    'updated_at' => Carbon::now(),
                                ];
                                $data[] = $tmpArr;
                            }
                        }
                    }
                }
                closedir($dh);
            }
        }

        pr($data);

        $res = FunctionalitiesModel::insert($data);
        if ( !$res ) {
            pr($res->errors());
        }

    }
}
