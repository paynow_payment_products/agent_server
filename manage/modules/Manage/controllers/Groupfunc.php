<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/27
 * Time: 20:33
 */

class GroupfuncController extends BaseController
{
    public $action_names = [
        'list',
        'add',
        'set',
        'del',
    ];
}