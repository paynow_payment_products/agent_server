<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:18
 */

use Message\MessageModel;

class ListAction extends AdminBaseAction
{
    public function run()
    {

        $request = Request::getQuery();
        $condiction = MessageModel::mkSearchCondiction($request);
        $list = MessageModel::doWhere($condiction)->paginate(10);
        $this->assign('list', $list);
        
        $searchItemsConf = MessageModel::mkSearchConfForForm($request);
        $this->assign('searchItemsConf', $searchItemsConf);
    }
}
