<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:18
 */

use Manage\GroupModel;

class ListAction extends AdminBaseAction
{
    public function run()
    {

        $request = Request::getQuery();
        $condiction = GroupModel::mkSearchCondiction($request);
        $list = GroupModel::doWhere($condiction)->paginate(10);
        $this->assign('list', $list);
        
        $searchItemsConf = GroupModel::mkSearchConfForForm($request);
        $this->assign('searchItemsConf', $searchItemsConf);
    }
}
