<?php

use Lib\Bank\CardInfo;
use Withdrawal\BankModel as Bank;
use Withdrawal\CardModel as Card;

class EditAction extends AdminBaseAction
{

    public function run()
    {
        $request  = Request::getQuery();
        $bankList = Bank::pluck('bank_name', 'bank_id');

        $card = Card::withTrashed()->find(Request::getQuery('id'));

        $card_no  = Request::getPost('card_no', '');
        $bank_id  = Request::getPost('bank_id', '');
        $name     = Request::getPost('name', '');
        $province = Request::getPost('province', '');
        $city     = Request::getPost('city', '');
        $bname    = Request::getPost('branc_name', '');

        if ($card_no && $bank_id && $name) {

            $bank_name = CardInfo::getInstance()->getBankName($card_no);

            if (empty($bank_name)) {
                $alertData = [
                    'type'      => 'danger',
                    'msg'       => '请检查银行卡',
                    'redir_url' => '/manage/card/list',
                ];
                $this->assign('alertData', $alertData);
                return;
            }

            $check = Card::withTrashed()->where('card_no', $card_no)->first();
            if (!empty($check)) {
                if ($check->user_id != $card->user_id) {
                    $alertData = [
                        'type'      => 'danger',
                        'msg'       => '银行卡已经他人被绑定',
                        'redir_url' => '/manage/card/list',
                    ];
                    $this->assign('alertData', $alertData);
                    return;
                }
            }

            $card->bank_id    = $bank_id;
            $card->card_no    = $card_no;
            $card->name       = $name;
            $card->province   = $province;
            $card->city       = $city;
            $card->branc_name = $bname;

            $res = $card->update();
            if ($res) {
                $alertData = [
                    'type'      => 'success',
                    'msg'       => '修改成功',
                    'redir_url' => '/manage/card/list',
                ];
            } else {
                $alertData = [
                    'type'      => 'danger',
                    'msg'       => '修改失败',
                    'redir_url' => '/manage/card/list',
                ];
            }
            $this->assign('alertData', $alertData);
            return;
        }

        $this->assign('request', $request);
        $this->assign('bankList', $bankList);
        $this->assign('card', $card);
    }

}
