<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:46
 */

use Withdrawal\CardModel as Card;

class DelAction extends AdminBaseAction
{
    protected $disableView = true;

    public function run()
    {
        $bSuccess = Card::find(Request::getQuery('id'))->delete();
        exit(json_encode([
            'success' => (int) $bSuccess,
            'msg'     => '',
        ]));
    }
}
