<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:46
 */

use Withdrawal\CardModel as Card;

class ListAction extends AdminBaseAction
{
    public function run()
    {

        $request = Request::getQuery();
        $condiction = Card::mkSearchCondiction($request);
        $list = Card::doWhere($condiction, Card::withTrashed())->paginate();
        $this->assign('list', $list);
        
        $searchItemsConf = Card::mkSearchConfForForm($request);
        $this->assign('searchItemsConf', $searchItemsConf);
    }
}