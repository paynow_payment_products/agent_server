<?php

use Lib\Bank\CardInfo;
use User\UserModel;
use Withdrawal\BankModel as Bank;
use Withdrawal\CardModel as Card;

class AddAction extends AdminBaseAction
{

    public function run()
    {
        $request  = Request::getQuery();
        $bankList = Bank::pluck('bank_name', 'bank_id');

        $card_no  = Request::getPost('card_no', '');
        $user_id  = Request::getPost('user_id', '');
        $bank_id  = Request::getPost('bank_id', '');
        $name     = Request::getPost('card_name', '');
        $province = Request::getPost('province', '');
        $city     = Request::getPost('city', '');
        $bname    = Request::getPost('branc_name', '');

        if ($card_no && $bank_id && $user_id && $province && $city && $bname) {

            $user = UserModel::where([
                'user_id' => $user_id,
            ])->first();

            if (empty($user)) {
                $alertData = [
                    'type'      => 'danger',
                    'msg'       => '用户不存在',
                    'redir_url' => '/manage/card/list',
                ];
                $this->assign('alertData', $alertData);
                return;
            }

            $bank_name = CardInfo::getInstance()->getBankName($card_no);

            if (empty($bank_name)) {
                $alertData = [
                    'type'      => 'danger',
                    'msg'       => '请检查银行卡',
                    'redir_url' => '/manage/card/list',
                ];
                $this->assign('alertData', $alertData);
                return;
            }

            $card = Card::withTrashed()->where('card_no', $card_no)->first();
            if (!empty($card)) {
                $alertData = [
                    'type'      => 'danger',
                    'msg'       => '银行卡已经存在',
                    'redir_url' => '/manage/card/list',
                ];
                $this->assign('alertData', $alertData);
                return;
            }
            $card = Card::Create([
                'user_id'    => $user->user_id,
                'bank_id'    => $bank_id,
                'card_no'    => $card_no,
                'status'     => Card::STATUS_NEW,
                'name'       => $name,
                'province'   => $province,
                'city'       => $city,
                'branc_name' => $bname,
            ]);

            $this->redirect('/manage/card/list');
        }

        $this->assign('request', $request);
        $this->assign('bankList', $bankList);
    }

}
