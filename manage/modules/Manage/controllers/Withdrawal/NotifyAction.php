<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/21
 * Time: 22:14
 */
use Withdrawal\WithdrawalModel;

class NotifyAction extends AdminBaseAction
{
    public $disableView = true;

    public function run()
    {
        $oWithdrawal = new WithdrawalModel();

        $data = [
            'new_withdraw_num'   => $oWithdrawal->getWithdrawalNumByStatus(WithdrawalModel::WITHDRAWAL_STATUS_MINUS_GOLD_SUCCESS),
            'faild_withdraw_num' => $oWithdrawal->getWithdrawalNumByStatus(WithdrawalModel::WITHDRAWAL_STATUS_TRANSFER_FAILED),
        ];
        $data['total_num'] = $data['new_withdraw_num'] + $data['faild_withdraw_num'];

        echo json_encode($data);
    }
}
