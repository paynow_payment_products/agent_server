<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/27
 * Time: 20:34
 */

use Illuminate\Database\Capsule\Manager as DB;
use Withdrawal\WithdrawalModel;

class ExportAction extends AdminBaseAction
{
    protected $disableView = true;

    public function run()
    {
        set_time_limit(0);
        $filename = 'Withdrawal_' . time() . '.csv';

        header("Content-type:text/csv");
        header("Content-Disposition:attachment;filename=" . $filename);
        header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
        header('Expires:0');
        header('Pragma:public');
        header("Content-type: text/csv");
        echo chr(239) . chr(187) . chr(191); // 加上bom头，系统自动默认为UTF-8编码
        echo "订单号, 用户ID, 渠道, 渠道订单号, 金额, 手续费, 状态, 发起时间, 审核时间, 出款结果返回时间\n";

        $request         = Request::getQuery();
        $condiction      = WithdrawalModel::mkSearchCondiction($request);
        $searchItemsConf = WithdrawalModel::mkSearchConfForForm($request);

        $start = !empty($condiction['created_at'][1][0]) ? $condiction['created_at'][1][0] : Carbon::today();
        $end   = !empty($condiction['created_at'][1][1]) ? $condiction['created_at'][1][1] : Carbon::tomorrow();

        $countsql = "select count(*) as num from withdrawals where created_at >= '{$start}' and created_at <= '{$end}' ";
        if (Request::getQuery('platform_id', '') !== '') {
            $countsql .= " and platform_id = " . Request::getQuery('platform_id');
        }
        if (Request::getQuery('status', '') !== '') {
            $countsql .= " and status = " . Request::getQuery('status');
        }
        $countsql .= " order by created_at asc";

        // echo $countsql;exit;

        $count = DB::select($countsql);

        if (empty($count) || empty($count[0]->num)) {
            return;
        }

        $pre_count = 20000;
        for ($i = 0; $i < intval($count[0]->num / $pre_count) + 1; $i++) {

            $sql = "SELECT w.serial_number, u.user_id, p.`name`, w.platform_order_no, w.amount, w.fee, w.`status`, w.created_at, w.admin_operate_time FROM withdrawals w LEFT JOIN users u ON w.user_id = u.user_id LEFT JOIN withdrawal_platforms p ON w.platform_id = p.platform_id where w.created_at >= '{$start}' and w.created_at <= '{$end}'";

            if (Request::getQuery('platform_id', '') !== '') {
                $sql .= " and w.platform_id = " . Request::getQuery('platform_id');
            }
            if (Request::getQuery('status', '') !== '') {
                $sql .= " and w.status = " . Request::getQuery('status');
            }
            $sql .= " order by w.created_at asc limit " . $i * $pre_count . ",{$pre_count}";
            // pr($sql);exit;

            $list = DB::select($sql);
            // pr($list);exit;
            // pr(memory_get_usage());
            $str = "";
            foreach ($list as $info) {
                $serial_number      = $info->serial_number;
                $user_id            = $info->user_id;
                $name               = $info->name;
                $platform_order_no  = $info->platform_order_no;
                $amount             = $info->amount / 100;
                $fee                = $info->fee;
                $status             = $searchItemsConf['status']['source']['data'][$info->status];
                $created_at         = $info->created_at;
                $admin_operate_time = $info->admin_operate_time;

                $str .= "$serial_number, $user_id, $name, $platform_order_no, $amount, $fee, $status, $created_at, $admin_operate_time\n";
            }
            echo $str;
        }
    }
}
