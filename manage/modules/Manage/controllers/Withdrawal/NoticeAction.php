<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/21
 * Time: 21:21
 */

use Withdrawal\NoticeModel;
class NoticeAction extends AdminBaseAction
{
    public function run()
    {
        $request = Request::getQuery();
        $condiction = NoticeModel::mkSearchCondiction($request);
        $list = NoticeModel::doWhere($condiction, NoticeModel::withTrashed())->paginate();
        $this->assign('list', $list);
        
        $searchItemsConf = NoticeModel::mkSearchConfForForm($request);
        $this->assign('searchItemsConf', $searchItemsConf);
    }
}