<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/21
 * Time: 21:21
 */

use Withdrawal\LogModel;

class LogAction extends AdminBaseAction
{
    public function run()
    {
        $request = Request::getQuery();
        $condiction = LogModel::mkSearchCondiction($request);
        $list = LogModel::doWhere($condiction)->paginate();
        $this->assign('list', $list);
        
        $searchItemsConf = LogModel::mkSearchConfForForm($request);
        $this->assign('searchItemsConf', $searchItemsConf);
    }
}