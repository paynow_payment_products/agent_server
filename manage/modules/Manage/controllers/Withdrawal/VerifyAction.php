<?php

use Carbon\Carbon;
use Deposit\DepositModel;
use Lib\Pn\Sdk as PnSdk;
use Transaction\OptypeModel;
use Transaction\TransactionModel;
use User\ClubModel;
use Withdrawal\BlackListModel;
use Withdrawal\PlatformModel;
use Withdrawal\WithdrawalModel;
use Manage\SystemConfModel;

/**
 * 提现审核详情
 */
class VerifyAction extends AdminBaseAction
{
    public function run()
    {
        $id = Request::get('id', 0);
        $this->assign('id', $id);

        $oWithdrawal = WithdrawalModel::find($id);
        if ($oWithdrawal->status != $oWithdrawal::WITHDRAWAL_STATUS_MINUS_GOLD_SUCCESS) {
            $this->redirect('/manage/withdrawal/list');
        }
        $this->assign('oWithdrawal', $oWithdrawal);

        if ($oWithdrawal->isTrashedCard()) {
            $oBlackCard = '';
        } else {
            $oBlackCard = BlackListModel::where('card_no', 'like', substr($oWithdrawal->card->card_no, 0, 4) . "%" . substr($oWithdrawal->card->card_no, -4))
                ->orWhere('name', $oWithdrawal->card->name)
                ->get()->first();
            // pr($oBlackCard);
        }
        $this->assign('oBlackCard', $oBlackCard);

        $optypeList = OptypeModel::all();
        $this->assign('optypeList', $optypeList);

        $platformList = PlatformModel::where('in_use', '1')
            ->where('status', '1')
            ->orderBy('limit', 'desc')
            ->get();
        $this->assign('platformList', $platformList);
        // pr($platformList);

        // 获取登录IP历史记录
        // pr($oWithdrawal->getUserLoginIp());
        // vd($oWithdrawal->isNormalIp());

        $this->assign('loginIps', $oWithdrawal->getUserLoginIp());
        $this->assign('isNormalIp', $oWithdrawal->isNormalIp());

        $todayDeposit = DepositModel::selectRaw('user_id, SUM(amount) amount, count(*) num')
            ->where('user_id', $oWithdrawal->user_id)
            ->where('status', 1)
            ->where('created_at', "like", Carbon::now()->toDateString())
            ->first();

        $totalDeposit = DepositModel::selectRaw('user_id, SUM(amount) amount, count(*) num')
            ->where('user_id', $oWithdrawal->user_id)
            ->where('status', 1)
            ->first();

        $todayWithdrawal = WithdrawalModel::selectRaw('user_id, SUM(amount) amount, count(*) num')
            ->where('user_id', $oWithdrawal->user_id)
            ->where('status', 1)
            ->where('created_at', "like", Carbon::now()->toDateString())
            ->first();

        $totalWithdrawal = WithdrawalModel::selectRaw('user_id, SUM(amount) amount, count(*) num')
            ->where('user_id', $oWithdrawal->user_id)
            ->where('status', 1)
            ->first();

        // pr($totalWithdrawal->toArray());

        $this->assign('todayDeposit', $todayDeposit);
        $this->assign('totalDeposit', $totalDeposit);
        $this->assign('todayWithdrawal', $todayWithdrawal);
        $this->assign('totalWithdrawal', $totalWithdrawal);

        // $userinfo = PnSdk::getInstance()->getUserInfoByUid($oWithdrawal->user->app_user_id);

        if (!empty($userinfo['financal']['status'])) {
            $club = ClubModel::find($userinfo['financal']['club_id']);
            $this->assign('club', $club);
        }

        // $notAllowedWithdrawalList = SystemConfModel::getKeyValue('not_allowed_withdrawal_list');
        // $notAllowedWithdrawalList = $notAllowedWithdrawalList ? explode(',', $notAllowedWithdrawalList) : [];
        $this->assign('notAllowedWithdrawalList', []);
    }
}
