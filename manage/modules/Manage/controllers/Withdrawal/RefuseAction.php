<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/21
 * Time: 22:13
 */

use Carbon\Carbon;
use Withdrawal\WithdrawalModel;

class RefuseAction extends AdminBaseAction
{
    public function run()
    {
        // 获取编号
        $withdrawal_id = Request::get('id');
        $remarks       = Request::get('remarks');

        if (!($remarks && $withdrawal_id)) {
            throw new Exception("请填写审核批注！");
        }

        $withdrawal = WithdrawalModel::find($withdrawal_id);

        // pr($withdrawal->toArray());

        if (empty($withdrawal)) {
            throw new Exception('no object find!');
        }

        // 检查状态
        if ($withdrawal->status != $withdrawal::WITHDRAWAL_STATUS_MINUS_GOLD_SUCCESS) {
            throw new Exception('status error');
        }

        // 退币
        $withdrawal->user->account->incrementBalance($withdrawal->amount + $withdrawal->fee, 7, $withdrawal->serial_number);

        // 修改状态
        $res = $withdrawal->update([
            'status'   => WithdrawalModel::WITHDRAWAL_STATUS_VERIFY_FAILED,
            'admin_id' => $this->session->admin_id,
            'remarks'  => $remarks,
            'admin_operate_time' => Carbon::now(),
        ]);

        if (!$res) {
            throw new Exception('update status failure!');
        }

        // remove withdraw notify sound
        // $withdrawal->sendWithdrawalNotifyToAdmin();

        echo json_encode([
            'success' => 1,
            'message' => "已经拒绝",
        ]);
    }
}
