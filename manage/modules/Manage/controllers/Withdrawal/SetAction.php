<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/21
 * Time: 22:13
 */

use Carbon\Carbon;
use Withdrawal\WithdrawalModel;

class SetAction extends AdminBaseAction
{
    public function run()
    {
        // 获取编号
        $withdrawal_id = Request::get('id');
        $type          = Request::get('type');
        $remarks       = Request::get('remarks');

        if (!($type && $withdrawal_id)) {
            throw new Exception("data error!");
        }

        $withdrawal = WithdrawalModel::find($withdrawal_id);

        // pr($withdrawal->toArray());

        if (empty($withdrawal)) {
            throw new Exception('no object find!');
        }

        // 设置为人工出款状态
        if ($type == "manualwithdrawal") {
            $statusSql = $withdrawal->status;
            if ($statusSql == WithdrawalModel::WITHDRAWAL_STATUS_MINUS_GOLD_SUCCESS) {
                $res = $withdrawal->update([
                    'status'       => WithdrawalModel::WITHDRAWAL_STATUS_SUCCESS,
                    'platform_id'  => 10,
                    'set_admin_id' => $this->session->admin_id,
                    'admin_set_at' => Carbon::now(),
                    'remarks'      => $remarks,
                ]);
                exit(json_encode(['success' => 1, 'message' => "设置成功"]));
            }
        }

        if ($type == "displayremark") {
            $res = $withdrawal->update([
                'display_remarks' => $remarks,
            ]);
            exit(json_encode(['success' => 1, 'message' => "设置成功"]));
        }

        // 检查状态
        if (!in_array($withdrawal->status, [$withdrawal::WITHDRAWAL_STATUS_TRANSFER_START, $withdrawal::WITHDRAWAL_STATUS_TRANSFER_EXCEPTION])) {
            throw new Exception('status error');
        }

        // admin_id -1 为系统自动出款 不判管理员权限
        if ($withdrawal->admin_id != $this->session->admin_id && $withdrawal->admin_id != "-1") {
            // 获取当前管理员所在用户组的最高权限组
            $curentUserGroupPriority = AdminModel::find($this->session->admin_id)
                ->group()
                ->orderBy('priority', 'asc')
                ->pluck('priority');
            $operateUserGroupPriority = AdminModel::find($withdrawal->admin_id)
                ->group()
                ->orderBy('priority', 'asc')
                ->pluck('priority');

            if ($curentUserGroupPriority >= $operateUserGroupPriority) {
                throw new Exception('权限不够');
            }
        }

        if ($type == "setfailed") {
            // 给用户退钱
            $withdrawal->user->account->incrementBalance($withdrawal->amount + $withdrawal->fee, 5, $withdrawal->serial_number);

            $res = $withdrawal->update([
                'status' => WithdrawalModel::WITHDRAWAL_STATUS_TRANSFER_FAILED,
            ]);
            $msg = '出款失败，游戏币已退回。';
        } elseif ($type == "setsuccess") {
            $res = $withdrawal->update([
                'status' => WithdrawalModel::WITHDRAWAL_STATUS_SUCCESS,
            ]);
            $msg = '出款完成';
        } else {
            throw new Exception('type error');
        }

        if (!$res) {
            throw new Exception('update status failure!');
        }

        echo json_encode([
            'success' => 1,
            'message' => $msg,
        ]);
    }
}
