<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/21
 * Time: 21:21
 */

use Withdrawal\QueryModel;

class QuerylogAction extends AdminBaseAction
{
    public function run()
    {
        $request    = Request::getQuery();
        $condiction = QueryModel::mkSearchCondiction($request);
        $list       = QueryModel::doWhere($condiction, QueryModel::withTrashed())->paginate();
        $this->assign('list', $list);

        $searchItemsConf = QueryModel::mkSearchConfForForm($request);
        $this->assign('searchItemsConf', $searchItemsConf);
    }
}
