<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/21
 * Time: 22:14
 */
use Carbon\Carbon;
use Withdrawal\NoticeModel;
use Withdrawal\PlatformModel;
use Withdrawal\WithdrawalModel;

class AcceptAction extends AdminBaseAction
{
    public $disableView = true;

    public function run()
    {
        // request id
        $withdrawal_id = Request::get('id');
        $remarks       = Request::get('remarks');
        $platform_id   = Request::get('platform_id');

        if (!($remarks && $withdrawal_id)) {
            throw new Exception("请填写审核批注！");
        }

        if (!$platform_id) {
            throw new Exception("请填选择提现渠道！！！");
        }

        $withdrawal = WithdrawalModel::find($withdrawal_id);

        // check object
        if (empty($withdrawal)) {
            throw new Exception('no object find!');
        }

        // check status
        if ($withdrawal->status != $withdrawal::WITHDRAWAL_STATUS_MINUS_GOLD_SUCCESS) {
            throw new Exception('status error');
        }

        $withdrawPlatform = PlatformModel::find($platform_id);
        if (empty($withdrawPlatform->bank_ids)) {
            throw new Exception("渠道支持的银行未设置，请先去设置渠道支持的银行");
        }

        // 用户发起提现后 删除了银行卡
        if ($withdrawal->isTrashedCard()) {
            throw new Exception("用户已经删除该银行卡，请拒绝后重新发起");
        }

        if (!in_array($withdrawal->card->bank->bank_id, $withdrawPlatform->bank_ids)) {
            throw new Exception("渠道不支持 " . $withdrawal->card->bank->bank_name . " 提现，请重新选择渠道");
        }

        // change status
        $withdrawal->update([
            'status'             => WithdrawalModel::WITHDRAWAL_STATUS_VERIFY_SUCCESS,
            'admin_id'           => $this->session->admin_id,
            'remarks'            => $remarks,
            'admin_operate_time' => Carbon::now(),
            'platform_id'        => $platform_id,
        ]);

        // add task
        $withdrawal->notices()->save(new NoticeModel([
            'start_at' => Carbon::now(),
        ]));

        echo json_encode([
            'success' => 1,
            'message' => "审核通过，正在出款！！！",
        ]);
    }
}
