<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/21
 * Time: 21:21
 */

use Withdrawal\WithdrawalModel;

class ListAction extends AdminBaseAction
{
    public function run()
    {
        $request    = Request::getQuery();
        $condiction = WithdrawalModel::mkSearchCondiction($request);
        $list       = WithdrawalModel::doWhere($condiction)->paginate();
        $this->assign('list', $list);

        $searchItemsConf = WithdrawalModel::mkSearchConfForForm($request);
        $this->assign('searchItemsConf', $searchItemsConf);
    }
}
