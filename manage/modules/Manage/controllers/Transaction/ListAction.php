<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 22:16
 */

use Transaction\TransactionModel as Transaction;

class ListAction extends AdminBaseAction
{
    public function run()
    {
        $request    = Request::getQuery();
        $condiction = Transaction::mkSearchCondiction($request);
        $list       = Transaction::doWhere($condiction)->paginate();
        $this->assign('list', $list);

        $searchItemsConf = Transaction::mkSearchConfForForm($request);
        $this->assign('searchItemsConf', $searchItemsConf);
    }
}
