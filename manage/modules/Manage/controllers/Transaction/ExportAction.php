<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/27
 * Time: 20:34
 */

use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as DB;
use Transaction\TransactionModel as Transaction;

class ExportAction extends AdminBaseAction
{
    protected $disableView = true;

    public function run()
    {
        set_time_limit(0);
        $filename = 'transaction_' . time() . '.csv';

        header("Content-type:text/csv");
        header("Content-Disposition:attachment;filename=" . $filename);
        header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
        header('Expires:0');
        header('Pragma:public');
        header("Content-type: text/csv");
        echo chr(239) . chr(187) . chr(191); // 加上bom头，系统自动默认为UTF-8编码
        echo "用户ID, 用户名, 金额, 剩余金币, 操作类型, 时间, 转出用户ID, 转入用户ID\n";

        $request    = Request::getQuery();
        $condiction = Transaction::mkSearchCondiction($request);

        $start = !empty($condiction['created_at'][1][0]) ? $condiction['created_at'][1][0] : Carbon::today();
        $end   = !empty($condiction['created_at'][1][1]) ? $condiction['created_at'][1][1] : Carbon::tomorrow();

        // $startTime = microtime_float();

        $countsql = "select count(*) as num from sc.transactions where transactions.created_at >= '{$start}' and transactions.created_at <= '{$end}'";

        if (Request::getQuery('status', '') !== '') {
            $countsql .= " and status = " . Request::getQuery('status');
        }

        if (Request::getQuery('user_id', '') !== '') {
            $countsql .= " and user_id = " . Request::getQuery('user_id');
        }

        $countsql .= " order by created_at asc";

        // pr($countsql);exit;

        $count = DB::select($countsql);

        if (empty($count) || empty($count[0]->num)) {
            return;
        }

        $pre_count = 20000;
        for ($i = 0; $i < intval($count[0]->num / $pre_count) + 1; $i++) {

            $sql = "select users.user_id, users.name, transactions.created_at, amount, remain, from_user_id, to_user_id, transaction_types.optype_name, transactions.created_at from sc.transactions left join sc.users on transactions.user_id=users.user_id left join sc.transaction_types on transactions.type_id=transaction_types.type_id where transactions.created_at >= '{$start}' and transactions.created_at <= '{$end}'";

            if (Request::getQuery('status', '') !== '') {
                $sql .= " and status = " . Request::getQuery('status');
            }

            if (Request::getQuery('user_id', '') !== '') {
                $sql .= " and users.user_id = " . Request::getQuery('user_id');
            }

            $sql .= " order by created_at asc limit " . $i * $pre_count . ",{$pre_count}";
            $list = DB::select($sql);
            // pr(memory_get_usage());

            if (empty($list)) {
                continue;
            }

            $str = "";
            foreach ($list as $info) {
                $user_id      = $info->user_id;
                $username     = $info->name;
                $amount       = $info->amount;
                $remain       = $info->remain;
                $status       = $info->optype_name;
                $created_at   = $info->created_at;
                $from_user_id = $info->from_user_id;
                $to_user_id   = $info->to_user_id;

                $str .= "$user_id, $username, $amount, $remain, $status, $created_at, $from_user_id, $to_user_id\n";
            }
            echo $str;
        }
    }
}
