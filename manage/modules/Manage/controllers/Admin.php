<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/21
 * Time: 20:14
 */

class AdminController extends BaseController
{
    public $action_names = [
        'list',
        'log',
        'changepsw',
        'add',
    ];
}