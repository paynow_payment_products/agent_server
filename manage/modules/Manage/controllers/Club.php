<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:45
 */

class ClubController extends BaseController
{
    public $action_names = [
        'list',
        'add',
        'del',
        'restore',
    ];
}