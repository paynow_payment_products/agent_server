<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:45
 */

class CardController extends BaseController
{
    public $action_names = [
        'list',
        'add',
        'del',
        'restore',
        'edit',
    ];
}