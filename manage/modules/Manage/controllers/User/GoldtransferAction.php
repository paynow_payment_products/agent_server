<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:18
 */

use Transaction\GoldoperateModel;
use User\AccountModel;
use User\UserModel;

class GoldtransferAction extends AdminBaseAction
{

    public function run()
    {

        $f_appid = Request::getPost('from_app_user_id');
        $t_appid = Request::getPost('to_user_app_id');
        $remarks = Request::getPost('remarks');
        $amount  = Request::getPost('amount');

        if ($f_appid && $t_appid && $remarks && $amount) {

            $from_user = UserModel::find($f_appid);
            $to_user   = UserModel::find($t_appid);

            if (!is_object($from_user) || !is_object($to_user)) {
                $alertData = [
                    'type' => 'danger',
                    'msg'  => '用户不存在',
                    // 'redir_url' => '/manage/user/list',
                ];
                $this->assign('alertData', $alertData);
                return;
            }

            // 财务号下单
            $res = GoldoperateModel::create([
                'user_id'  => $f_appid,
                'type_id'  => 3,
                'admin_id' => $this->session['admin_id'],
                'optime'   => date('Y-m-d H:i:s', time()),
                'amount'   => -$amount,
                'remarks'  => trim($remarks),
            ]);

            $res2 = GoldoperateModel::create([
                'user_id'  => $t_appid,
                'type_id'  => 4,
                'admin_id' => $this->session['admin_id'],
                'optime'   => date('Y-m-d H:i:s', time()),
                'amount'   => $amount,
                'remarks'  => trim($remarks),
                'bill_no'  => $res->bill_no,
            ]);

            if ($res && $res2) {

                try {
                    AccountModel::transfer($from_user, $to_user, $amount, $remarks);
                } catch (Exception $e) {
                    // pr($e->getMessage());
                    $alertData = [
                        'type' => 'danger',
                        'msg'  => $e->getMessage(),
                        // 'redir_url' => '/manage/user/list',
                    ];
                    $this->assign('alertData', $alertData);
                    return;
                }

                $res->update(['success' => 1]);
                $res2->update(['success' => 1]);
                $alertData = [
                    'type'      => 'success',
                    'msg'       => '操作成功',
                    'redir_url' => '/manage/user/list',
                ];
                $this->assign('alertData', $alertData);
            }
        }
    }
}
