<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:18
 */

use User\UserModel;

class ListAction extends AdminBaseAction
{
    public function run()
    {

        $request = Request::getQuery();
        $condiction = UserModel::mkSearchCondiction($request);
        $list = UserModel::doWhere($condiction)->paginate();
        $this->assign('list', $list);
        
        $searchItemsConf = UserModel::mkSearchConfForForm($request);
        $this->assign('searchItemsConf', $searchItemsConf);
    }
}
