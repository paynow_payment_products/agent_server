<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:18
 */

use Transaction\GoldoperateModel;
use User\AccountModel;
use User\UserModel;

class GoldoperateAction extends AdminBaseAction
{
    public function run()
    {
        $iUid  = Request::getQuery('user_id', '');
        $oUser = UserModel::find($iUid);
        if (!is_object($oUser)) {
            $alertData = [
                'type'      => 'danger',
                'msg'       => '用户不存在',
                'redir_url' => '/manage/user/list',
            ];
            $this->assign('alertData', $alertData);
            return;
        }
        $this->assign('oUser', $oUser);

        $operateGoldList = GoldoperateModel::STATUS_ENUM;
        $this->assign('operateGoldList', $operateGoldList);

        $type    = Request::getPost('transaction_type');
        $amount  = Request::getPost('amount');
        $remarks = Request::getPost('remarks');
        if ($type && $amount && $remarks) {

            $res = GoldoperateModel::create([
                'user_id'  => $iUid,
                'type_id'  => $type,
                'admin_id' => $this->session['admin_id'],
                'optime'   => date('Y-m-d H:i:s', time()),
                'amount'   => $amount,
                'remarks'  => trim($remarks),
            ]);

            if ($res) {

                try {
                    $amount = in_array($type, [6, 9]) ? -$amount : $amount;
                    AccountModel::operateBalance($iUid, $amount, $type, $res->bill_no);
                } catch (Exception $e) {
                    // pr($e->getMessage());
                    $alertData = [
                        'type' => 'danger',
                        'msg'  => $e->getMessage(),
                        // 'redir_url' => '/manage/user/list',
                    ];
                    $this->assign('alertData', $alertData);
                    return;
                }

                $res->update(['success' => 1]);
                $alertData = [
                    'type'      => 'success',
                    'msg'       => '操作成功',
                    'redir_url' => '/manage/user/list',
                ];
                $this->assign('alertData', $alertData);
            }
        }
    }
}
