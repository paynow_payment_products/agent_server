<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:18
 */

use User\UserModel;

class InfoAction extends AdminBaseAction
{
    public function run()
    {

        $uid = Request::get('user_id', '');
        if (empty($uid)) {
            throw new Exception("Empty Uid", 1);
        }

        $userInfo = UserModel::find($uid);
        // pr($userInfo);
        $this->assign('userInfo', $userInfo);
    }
}
