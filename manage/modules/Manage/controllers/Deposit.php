<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/27
 * Time: 20:33
 */

class DepositController extends BaseController
{
    public $action_names = [
        'list',
        'export',
        'log',
        'order',
    ];
}