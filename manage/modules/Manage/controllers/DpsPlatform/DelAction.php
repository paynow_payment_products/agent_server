<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:46
 */

use Deposit\PlatformModel;

class DelAction extends AdminBaseAction
{
    protected $disableView = true;

    public function run()
    {
        $bSuccess = PlatformModel::find(Request::getQuery('id'))->update([
                'status' => PlatformModel::STATUS_CLOSED,
                'deleted_at' => date('Y-m-d H:i:s', time()),
            ]);

        exit(json_encode([
            'success' => (int) $bSuccess,
            'msg'     => '',
        ]));
    }
}
