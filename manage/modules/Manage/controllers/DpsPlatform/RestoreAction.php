<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:46
 */

use Deposit\PlatformModel;

class RestoreAction extends AdminBaseAction
{
    protected $disableView = true;

    public function run()
    {
        $bSuccess = PlatformModel::withTrashed()->where('platform_id', Request::getQuery('id'))->restore();
        exit(json_encode([
            'success' => (int) $bSuccess,
            'msg'     => '',
        ]));
    }
}
