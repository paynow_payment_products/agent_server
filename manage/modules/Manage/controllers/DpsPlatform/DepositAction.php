<?php

use Deposit\DepositModel;
use Deposit\PlatformModel;

class DepositAction extends AdminBaseAction
{

    public function run()
    {
        $platform = PlatformModel::find(Request::getQuery('id'));

        if (!is_object($platform)) {
            throw new Exception('不存在的渠道');
        }

        $paymentAdapter = $platform->getPlatformAdapter();
        // pr($paymentAdapter->banks);

        $post = Request::getPost();
        if (!empty($post['bank_id']) && !empty($post['amount'])) {

            if (! empty($platform->custom_params['bank_id'])) {
                $bank_id = $platform->custom_params['bank_id'];
            } else {
                $bank_id = $post['bank_id'];

                if (empty($bank_id)) {
                    throw new Exception('请选择银行');
                }
            }

            // save deposit info
            $deposit = DepositModel::create([
                'app_id'      => 1,
                'user_id'     => 47,
                'platform_id' => $platform->platform_id,
                'bank_id'     => $bank_id,
                'amount'      => $post['amount'],
            ]);

            // parse input data
            $inputData = $paymentAdapter->compileLoadInputData($deposit);
            // pr($inputData);exit;

            // get break url
            $response = [];
            $loadUrl  = $paymentAdapter->compileLoadUrl($platform, $inputData, $response);

            // save order
            $paymentAdapter->saveLoadOrder($deposit, $inputData, $response, $loadUrl);

            if (!$loadUrl) {
                throw new Exception('支付系统错误');
            }

            if (!empty($deposit)) {

                if ($platform->load_straight) {
                    $this->redirect($loadUrl);
                } else {
                    echo json_encode([
                        'success' => 1,
                        'data'    => [
                            'order_no'      => $deposit->order_no,
                            'load_url'      => $loadUrl,
                            'load_straight' => $platform->load_straight,
                        ],
                    ]);
                }

            }
        }

        $this->assign('banklist', $paymentAdapter->banks);
    }

}
