<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/27
 * Time: 20:34
 */

use Deposit\PlatformModel;

class ListAction extends AdminBaseAction
{
    public function run()
    {
        $request = Request::getQuery();
        $condiction = PlatformModel::mkSearchCondiction($request);
        // $list = PlatformModel::doWhere($condiction, PlatformModel::withTrashed())->paginate();
        $list = PlatformModel::doWhere($condiction)->paginate();
        $this->assign('list', $list);
        
        $searchItemsConf = PlatformModel::mkSearchConfForForm($request);
        $this->assign('searchItemsConf', $searchItemsConf);
    }
}