<?php

use Deposit\PlatformModel;
use Withdrawal\BankModel;

class EditAction extends AdminBaseAction
{

    public function run()
    {
        $request = Request::getQuery();
        $id = Request::getQuery('id');

        $platform = PlatformModel::withTrashed()->find($id);

        if ( !empty(Request::getPost()) ) {

            $bSuccess = $platform->update(Request::getPost());
            if ( $bSuccess ) {
                $this->assign('alertData', ['type' => 'success', 'msg' => '修改成功！', 'redir_url' => '/manage/dpsPlatform/list']);
                return;
            }
            $this->assign('alertData', ['type' => 'danger', 'msg' => '修改失败！']);
            return;
        }

        $allBanks = BankModel::all();
        $this->assign('allBanks', $allBanks);

        $this->assign('request', $request);
        $this->assign('platform', $platform);
    }

}
