<?php

use Withdrawal\PlatformModel;

class AddAction extends AdminBaseAction
{

    public function run()
    {
        $platform = new PlatformModel();

        if ( !empty(Request::getPost()) ) {

            $bSuccess = $platform->create(Request::getPost());
            if ( $bSuccess ) {
                $this->assign('alertData', ['type' => 'success', 'msg' => '添加成功！', 'redir_url' => '/manage/wtdplatform/list']);
                return;
            }
            $this->assign('alertData', ['type' => 'danger', 'msg' => '添加失败！']);
            return;
        }

        $this->assign('platform', $platform);
    }

}
