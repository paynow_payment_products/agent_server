<?php

use Withdrawal\BankModel;
use Withdrawal\PlatformModel;

class EditAction extends AdminBaseAction
{

    public function run()
    {
        $request = Request::getQuery();
        $this->assign('request', $request);

        $id = Request::getQuery('id');

        $platform = PlatformModel::withTrashed()->find($id);
        $this->assign('platform', $platform);

        $allBanks = BankModel::all();
        $this->assign('allBanks', $allBanks);

        if (!empty(Request::getPost())) {

            $bSuccess = $platform->update(Request::getPost());
            if ($bSuccess) {
                $this->assign('alertData', ['type' => 'success', 'msg' => '修改成功！', 'redir_url' => '/manage/wtdplatform/list']);
                return;
            }
            $this->assign('alertData', ['type' => 'danger', 'msg' => '修改失败！']);
            return;
        }

    }

}
