<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/21
 * Time: 21:20
 */

class WithdrawalController extends BaseController
{
    public $action_names = [
        'list',
        'accept',
        'refuse',
        'set',
        'export',
        'notify',
        'log',
        'notice',
        'verify',
        'querylog',
    ];
}