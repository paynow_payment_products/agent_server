<?php

use Manage\FunctionalitiesModel;

class EditAction extends AdminBaseAction
{

    public function run()
    {
        $request = Request::getQuery();
        $id      = Request::getQuery('id');

        $functionalitie = FunctionalitiesModel::withTrashed()->find($id);
        $list           = FunctionalitiesModel::where([])->get();

        $data = Request::getPost();
        if (!empty($data)) {

            if ($data['parent_func_id']) {
                $parentFunc = FunctionalitiesModel::find($data['parent_func_id']);
                if ($parentFunc['forefather_ids']) {
                    $data['forefather_ids'] = $parentFunc['forefather_ids'] . "," . $data['parent_func_id'];
                } else {
                    $data['forefather_ids'] = $data['parent_func_id'];
                }
            } else {
                $data['parent_func_id'] = 0;
            }
            // pr(Request::getPost());exit;

            $bSuccess = $functionalitie->update($data);
            if ($bSuccess) {
                $this->assign('alertData', ['type' => 'success', 'msg' => '修改成功！', 'redir_url' => '/manage/functionalities/list']);
                return;
            }
            $this->assign('alertData', ['type' => 'danger', 'msg' => '修改失败！']);
            return;
        }

        $this->assign('request', $request);
        $this->assign('functionalitie', $functionalitie);
        $this->assign('list', $list);
    }

}
