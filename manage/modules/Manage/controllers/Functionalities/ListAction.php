<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/27
 * Time: 20:34
 */

use Manage\FunctionalitiesModel;

class ListAction extends AdminBaseAction
{
    public function run()
    {
        $request = Request::getQuery();
        $condiction = FunctionalitiesModel::mkSearchCondiction($request);
        $list = FunctionalitiesModel::doWhere($condiction)->paginate();
        // pr($list);
        $this->assign('list', $list);
        
        $searchItemsConf = FunctionalitiesModel::mkSearchConfForForm($request);
        $this->assign('searchItemsConf', $searchItemsConf);
    }
}