<?php

use Manage\FunctionalitiesModel;

class AddAction extends AdminBaseAction
{

    public function run()
    {
        $request = Request::getQuery();
        $id      = Request::getQuery('id');
        $list    = FunctionalitiesModel::where([])->get();

        $data = Request::getPost();
        if (!empty($data)) {

            $filePath = APPLICATION_PATH . '/modules/' . $data['module'] . "/controllers/" . str_replace("Controller", '', $data['controller']) . '/' . $data['action'] . ".php";

            if (!file_exists($filePath)) {
                $this->assign('alertData', ['type' => 'danger', 'msg' => '文件不存在，请先创建对应的文件！']);
                return;
            }

            if ($data['parent_func_id']) {
                $parentFunc = FunctionalitiesModel::find($data['parent_func_id']);
                if ($parentFunc['forefather_ids']) {
                    $data['forefather_ids'] = $parentFunc['forefather_ids'] . "," . $data['parent_func_id'];
                }else{
                    $data['forefather_ids'] = $data['parent_func_id'];
                }
            }else{
                $data['parent_func_id'] = 0;
            }

            $bSuccess = FunctionalitiesModel::create($data);
            if ($bSuccess) {
                $this->assign('alertData', ['type' => 'success', 'msg' => '创建成功！', 'redir_url' => '/manage/functionalities/list']);
                return;
            }
            $this->assign('alertData', ['type' => 'danger', 'msg' => '创建失败！']);
            return;
        }

        $this->assign('request', $request);
        $this->assign('list', $list);
    }

}
