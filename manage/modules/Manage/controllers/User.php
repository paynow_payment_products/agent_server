<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:18
 */

class UserController extends BaseController
{
    public $action_names = [
        'list',
        'goldoperate',
        'goldtransfer',
        'info',
    ];
}