<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/27
 * Time: 20:34
 */

use Transaction\GoldoperateModel;

class ExportAction extends AdminBaseAction
{
    protected $disableView = true;

    public function run()
    {

        $request         = Request::getQuery();
        $condiction      = GoldoperateModel::mkSearchCondiction($request);
        $searchItemsConf = GoldoperateModel::mkSearchConfForForm($request);
        $list            = GoldoperateModel::doWhere($condiction)->get();

        // pr($list->toArray());exit();

        $str = "";
        foreach ($list as $info) {
            $user_id     = $info->user_id;
            $order_no    = $info->bill_no;
            $optype_name = $info->type->optype_name;
            $name        = $info->admin->name;
            $optime      = $info->optime;
            $amount      = $info->amount;
            $remarks     = $info->remarks;
            $client_ip   = $info->client_ip;
            $status      = $info->success ? "成功" : "失败";

            $str .= "$user_id, $order_no, $optype_name, $name, $optime, $amount, $remarks, $client_ip, $status\n";
        }

        $header = "app用户ID, 订单编号, 操作类型, 操作人员, 操作时间, 金额, 备注, 操作IP, 接口请求结果\n";
        $data   = $header . $str;
        // pr($data);exit;
        $filename = 'Withdrawal_' . time() . '.csv';
        $this->writeDataToCsv($filename, $data); //将数据导出
    }
}
