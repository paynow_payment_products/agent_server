<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/27
 * Time: 20:34
 */

use Transaction\GoldoperateModel;

class ListAction extends AdminBaseAction
{
    public function run()
    {
        $request = Request::getQuery();
        $condiction = GoldoperateModel::mkSearchCondiction($request);
        $list = GoldoperateModel::doWhere($condiction)->paginate();
        // pr($list);
        $this->assign('list', $list);
        
        $searchItemsConf = GoldoperateModel::mkSearchConfForForm($request);
        $this->assign('searchItemsConf', $searchItemsConf);
    }
}