<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 22:05
 */

class TransactionController extends BaseController
{
    public $action_names = [
        'list',
        'export',
    ];
}