<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:46
 */

use \Manage\AdminModel;

class AddAction extends AdminBaseAction
{
    public function run()
    {
        $name     = Request::getPost('name', '');
        $newPsw     = Request::getPost('newPsw', '');

        if ( $name && $newPsw) {
            $admin = AdminModel::where('name', $name)->get();
            if ( !$admin->isEmpty() ) {
                $alertData = [
                    'type'      => 'danger',
                    'msg'       => '用户已经存在',
                    'redir_url' => '/manage/admin/list',
                ];
                $this->assign('alertData', $alertData);
            }else{
                $res = AdminModel::create(['name' => $name, 'password' => $newPsw]);
                if ( $res ) {
                    $alertData = [
                        'type'      => 'success',
                        'msg'       => '操作成功',
                        'redir_url' => '/manage/admin/list',
                    ];
                }else{
                    $alertData = [
                        'type'      => 'danger',
                        'msg'       => '操作失败',
                        'redir_url' => '/manage/admin/list',
                    ];
                }
                $this->assign('alertData', $alertData);
            }
        }
    }
}