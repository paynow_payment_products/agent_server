<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:46
 */

use \Manage\AdminModel;

class ListAction extends AdminBaseAction
{
    public function run()
    {

        $request = Request::getQuery();
        $condiction = AdminModel::mkSearchCondiction($request);
        $list = AdminModel::doWhere($condiction)->get();
        $this->assign('list', $list);
        
        $searchItemsConf = AdminModel::mkSearchConfForForm($request);
        $this->assign('searchItemsConf', $searchItemsConf);
    }
}