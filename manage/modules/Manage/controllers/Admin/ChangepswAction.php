<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:46
 */

use \Manage\AdminModel;

class ChangepswAction extends AdminBaseAction
{

    public function run()
    {
        $admin_id   = Request::get('id', '');

        if ($admin_id != $this->session->admin_id) {
            exit('没有权限修改其他人的密码');
        }

        $admin = AdminModel::find($admin_id);
        
        $oldPsw     = Request::getPost('oldPsw', '');
        $newPsw     = Request::getPost('newPsw', '');
        $confirmPsw = Request::getPost('confirmPsw', '');

        if ( $oldPsw && $newPsw && $confirmPsw) {
            if ($admin->checkpsw($oldPsw) && $newPsw == $confirmPsw) {
                $admin->update(['password' => $newPsw]);
                $this->redirect('/manage/admin/list');
            }else{
                $this->redirect('/manage/admin/list');
            }
        }
    }
}
