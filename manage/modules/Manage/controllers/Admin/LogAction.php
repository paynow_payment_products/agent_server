<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:46
 */

use \Manage\AdminLogModel;

class LogAction extends AdminBaseAction
{
    public function run()
    {

        $request = Request::getQuery();
        $condiction = AdminLogModel::mkSearchCondiction($request);
        $list = AdminLogModel::doWhere($condiction)->paginate();
        $this->assign('list', $list);
        
        $searchItemsConf = AdminLogModel::mkSearchConfForForm($request);
        $this->assign('searchItemsConf', $searchItemsConf);
    }
}