<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:18
 */
use Deposit\UserPlatformModel;

class DelAction extends AdminBaseAction
{
    protected $disableView = true;

    public function run()
    {
        $upid = Request::getQuery('id');

        $res = UserPlatformModel::find($upid)->delete();

        if ( $res ) {
            exit(json_encode(['success' => 1, 'message' => '删除成功']));
        }else{
            exit(json_encode(['success' => 0, 'message' => '删除成功']));
        }
    }
}
