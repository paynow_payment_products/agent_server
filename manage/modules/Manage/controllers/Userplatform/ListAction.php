<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:18
 */

use Deposit\UserPlatformModel;

class ListAction extends AdminBaseAction
{
    public function run()
    {
        $request    = Request::getQuery();
        $condiction = UserPlatformModel::mkSearchCondiction($request);
        $list       = UserPlatformModel::doWhere($condiction)->paginate();
        $this->assign('list', $list);

        $searchItemsConf = UserPlatformModel::mkSearchConfForForm($request);
        $this->assign('searchItemsConf', $searchItemsConf);
    }
}
