<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:18
 */

use Deposit\PlatformModel;
use Deposit\UserPlatformModel;
use User\UserModel;

class AddAction extends AdminBaseAction
{
    public function run()
    {
        $uid          = Request::getQuery('uid', '');
        $oUser        = UserModel::find($uid);
        $platformList = PlatformModel::get();
        $this->assign('platformList', $platformList);
        $this->assign('oUser', $oUser);

        $user_id   = Request::getPost('user_id', '');
        $platform_id   = Request::getPost('platform_id', '');
        $terminal_type = Request::getPost('terminal_type', '');
        $is_show       = Request::getPost('is_show', '');

        if ($user_id && $platform_id && $terminal_type && $is_show !== '') {

            $oUser = UserModel::where('user_id', $user_id)->first();

            if (!is_object($oUser)) {
                $this->assign('alertData', ['type' => 'danger', 'msg' => '用户不存在！']);
                return;
            }

            $res = UserPlatformModel::firstOrCreate([
                'terminal_type' => $terminal_type,
                'user_id'       => $oUser->user_id,
                'platform_id'   => $platform_id,
            ]);

            $res && $res = $res->update(['is_show' => $is_show]);

            if ($res) {
                $this->assign('alertData', ['type' => 'success', 'msg' => '添加成功', 'redir_url' => '/manage/userplatform/list']);
                return;
            }

            $this->assign('alertData', ['type' => 'danger', 'msg' => '添加失败！']);
            exit;
        }
    }
}
