<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:18
 */

use Deposit\UserPlatformModel;

class EditAction extends AdminBaseAction
{
    public function run()
    {
        $request       = Request::getQuery();
        $oUserPlatform = UserPlatformModel::find($request['id']);
        $this->assign('oUserPlatform', $oUserPlatform);

        $app_user_id   = Request::getPost('app_user_id', '');
        $platform_id   = Request::getPost('platform_id', '');
        $terminal_type = Request::getPost('terminal_type', '');
        $is_show       = Request::getPost('is_show', '');

        if ($app_user_id && $platform_id && $terminal_type && $is_show !== '') {

            $res = $oUserPlatform->update([
                'is_show' => $is_show,
            ]);

            if ($res) {
                $this->assign('alertData', ['type' => 'success', 'msg' => '修改成功！', 'redir_url' => '/manage/userplatform/list']);
                return;
            }

            $this->assign('alertData', ['type' => 'danger', 'msg' => '修改失败！']);
            return;
        }

    }
}
