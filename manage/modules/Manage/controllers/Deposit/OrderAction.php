<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/27
 * Time: 20:34
 */

use Deposit\OrderModel;

class OrderAction extends AdminBaseAction
{
    public function run()
    {
        $request = Request::getQuery();
        $condiction = OrderModel::mkSearchCondiction($request);
        $list = OrderModel::doWhere($condiction)->paginate();
        $this->assign('list', $list);
        
        $searchItemsConf = OrderModel::mkSearchConfForForm($request);
        $this->assign('searchItemsConf', $searchItemsConf);
    }
}