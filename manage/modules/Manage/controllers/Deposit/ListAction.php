<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/27
 * Time: 20:34
 */

use Deposit\DepositModel;

class ListAction extends AdminBaseAction
{
    public function run()
    {
        $request = Request::getQuery();
        $condiction = DepositModel::mkSearchCondiction($request);
        $list = DepositModel::doWhere($condiction)->paginate();
        $this->assign('list', $list);
        
        $searchItemsConf = DepositModel::mkSearchConfForForm($request);
        $this->assign('searchItemsConf', $searchItemsConf);
    }
}