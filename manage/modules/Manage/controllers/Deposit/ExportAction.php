<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/27
 * Time: 20:34
 */

use Deposit\DepositModel;

class ExportAction extends AdminBaseAction
{
    protected $disableView = true;

    public function run()
    {

        $request         = Request::getQuery();
        $condiction      = DepositModel::mkSearchCondiction($request);
        $searchItemsConf = DepositModel::mkSearchConfForForm($request);
        $list            = DepositModel::doWhere($condiction)->get();

        $str = "";
        foreach ($list as $info) {
            $order_no      = $info->order_no;
            $nickname        = $info->user->nickname;
            $platform_name = $info->platform->name;
            $platform_no   = $info->order->third_order_no;
            $amount        = $info->amount;
            $status        = $searchItemsConf['status']['source']['data'][$info->status];
            $optime        = $info->created_at->format('Y-m-d H:i:s');
            $IP            = $info->client_ip;

            $str .= "$order_no, $nickname, $platform_name, $platform_no, $amount, $status, $optime, $IP\n";
        }

        $header = "订单号, 昵称, 渠道, 渠道订单号, 金额, 状态, 发起时间, IP\n";
        $data   = $header . $str;
        // pr($data);exit;
        $filename = 'deposit_' . time() . '.csv';
        $this->writeDataToCsv($filename, $data); //将数据导出
    }
}
