<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/27
 * Time: 20:34
 */

use User\FeedbackModel;

class ListAction extends AdminBaseAction
{
    public function run()
    {
        $request = Request::getQuery();
        $condiction = FeedbackModel::mkSearchCondiction($request);
        $list = FeedbackModel::doWhere($condiction)->paginate();
        // pr($list);
        $this->assign('list', $list);
        
        $searchItemsConf = FeedbackModel::mkSearchConfForForm($request);
        $this->assign('searchItemsConf', $searchItemsConf);
    }
}