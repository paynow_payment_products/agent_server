<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:46
 */

use \Manage\AdminModel;

class LogoutAction extends AdminBaseAction
{
    public $disableView = true;

    protected $needCheckRights = false;

    public function run()
    {
        $admin = AdminModel::find($this->session->admin_id);
        // todo  写日志
        $this->session->del('admin_id');
        $this->redirect('/manage/index');
    }
}
