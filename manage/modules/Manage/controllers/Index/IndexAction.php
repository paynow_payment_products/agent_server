<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/21
 * Time: 17:30
 */

use \manage\AdminLogModel;
use \Manage\AdminModel;

class IndexAction extends BaseAction
{
    public function run()
    {
        if ($this->session->admin_id) {
            $this->redirect('/manage/dashboard');
        }

        $ipList = include_once APPLICATION_PATH . '/conf/ip.conf.php';

        if ( !in_array(get_client_ip(), $ipList) ) {
            // exit("非法IP");
        }

        $name = Request::getPost('name', '');
        $psw  = Request::getPost('psw', '');

        if ($name && $psw) {

            $oAdmin = AdminModel::where('name', trim($name))->first();

            if (is_object($oAdmin) && $oAdmin->checkPsw($psw)) {

                $this->session->admin_id = $oAdmin->admin_id;

                $oAdmin->update(['signin_at' => date('Y-m-d H:i:s', time())]);

                $oAdmin->logs()->save(new AdminLogModel([
                    'controller' => get_class($this->getController()),
                    'action'     => get_called_class(),
                ]));

                $this->redirect('/manage/dashboard');

            } else {
                exit("没有权限");
            }
        }
    }
}
