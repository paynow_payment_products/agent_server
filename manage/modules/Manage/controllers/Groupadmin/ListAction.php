<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:18
 */

use Manage\GroupModel;

class ListAction extends AdminBaseAction
{
    // protected $disableView = true;

    public function run()
    {

        $oGroupAdmin = GroupModel::find(Request::getQuery('id'))->admin;

        $this->assign('oGroupAdmin', $oGroupAdmin);
        $this->assign('groupId', Request::getQuery('id'));
    }
}
