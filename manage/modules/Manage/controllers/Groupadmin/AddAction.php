<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:18
 */

use Manage\AdminModel;
use Manage\GroupModel;

class AddAction extends AdminBaseAction
{
    // protected $disableView = true;

    public function run()
    {
        $groupId = Request::getQuery('group_id', '');

        $adminList = AdminModel::all();

        $post = Request::getPost();

        if ($post && !empty($post['admin_id']) && $groupId) {
            $res = GroupModel::find($groupId)->admin()->syncWithoutDetaching($post['admin_id']);
            if ($res) {
                $this->assign('alertData', ['type' => 'success', 'msg' => '添加成功！', 'redir_url' => '/manage/groupadmin/list?id=' . $groupId]);
            } else {
                $this->assign('alertData', ['type' => 'danger', 'msg' => '添加失败！', 'redir_url' => '/manage/groupadmin/list?id=' . $groupId]);
            }
            return;
        }

        $this->assign('adminList', $adminList);
    }
}
