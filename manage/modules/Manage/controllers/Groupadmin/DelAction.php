<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:18
 */

use Manage\GroupModel;

class DelAction extends AdminBaseAction
{
    protected $disableView = true;

    public function run()
    {
        $groupId = Request::getQuery('gid', '');
        $adminId = Request::getQuery('uid', '');

        if ($groupId && $adminId) {
            $res = GroupModel::find($groupId)->admin()->detach($adminId);
            if ($res) {
                $return = ['success' => 1];
            } else {
                $return = ['success' => 0, 'message' => '删除失败'];
            }

            exit(json_encode($return));
        }
    }
}
