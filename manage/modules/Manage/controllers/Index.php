<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/21
 * Time: 17:29
 */

class IndexController extends BaseController
{
    public $action_names = [
        'index',
        'logout',
    ];
}