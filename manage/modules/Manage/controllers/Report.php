<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:18
 */

class ReportController extends BaseController
{
    public $action_names = [
        'dayreport',
        'jfdayreport'
    ];
}