<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:18
 */

use Manage\FunctionalitiesModel;
use Manage\GroupFuncModel;
use Manage\GroupModel;

class SetAction extends AdminBaseAction
{
    // protected $disableView = true;

    public function run()
    {
        $group_id = Request::getQuery('id');
        if (!$group_id) {
            $this->redirect('/manage/groupfunc/list');
        }

        // $groupFunc = GroupFuncModel::where('group_id', $group_id)->pluck('function_id')->toArray();
        $groupFunc = GroupModel::find($group_id)->functionalitie->pluck('function_id')->toArray();
        // pr($groupFunc);

        FunctionalitiesModel::getTreeArray($data, 0);
        // pr($data);

        $post = Request::getPost();
        // pr($post);
        if ($post) {

            $res2 = GroupModel::find($group_id)->functionalitie()->sync($post['functionality_id']);

            if (!$res2) {
                $this->assign('alertData', ['type' => 'danger', 'msg' => '设置失败', 'redir_url' => '/manage/groupfunc/list?id=' . $group_id]);
                return;
            }

            $this->assign('alertData', ['type' => 'success', 'msg' => '成功！', 'redir_url' => '/manage/groupfunc/list?id=' . $group_id]);
        }

        $this->assign('checked', $groupFunc);
        $this->assign('datas', $data);
    }
}
