<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:18
 */

use Manage\FunctionalitiesModel;
use Manage\GroupFuncModel;

class ListAction extends AdminBaseAction
{
    // protected $disableView = true;

    public function run()
    {

        $group_id = Request::getQuery('id');
        if (!$group_id) {
            $this->redirect('/manage/groupfunc/list');
        }

        $groupFunc = GroupFuncModel::where('group_id', $group_id)->pluck('function_id')->toArray();
        // pr($groupFunc);

        FunctionalitiesModel::getTreeArray($data, 0);
        // pr($data);

        $this->assign('checked', $groupFunc);
        $this->assign('datas', $data);
    }
}
