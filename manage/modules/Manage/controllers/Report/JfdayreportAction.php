<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/26
 * Time: 21:18
 */

use User\AccountReportModel;

class JfdayreportAction extends AdminBaseAction
{
    public function run()
    {

        $request = Request::getQuery();
        $condiction = AccountReportModel::mkSearchCondiction($request);
        $list = AccountReportModel::doWhere($condiction)->paginate();
       // var_dump($list);die();
        $this->assign('list', $list);
        
        $searchItemsConf = [];
        $this->assign('searchItemsConf', $searchItemsConf);
    }
}
