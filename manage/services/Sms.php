<?php

define('SSL_CERT_DIR', __DIR__ . '/cert');
define('LOG_DIR', __DIR__ . '/logs');
define('PID_FILE', LOG_DIR . '/sms_socket_server.pid');

class Sms
{
    const RONGLIAN_YZM = 'ronglian-yzm';
    const SMS_TYPES = [self::RONGLIAN_YZM];

    public $_http;
    public $application;

    public function __construct($server_host, $server_port)
    {
        ob_start();
        $this->_http = new swoole_http_server($server_host, $server_port);
        $this->_http->set([
            'ssl_cert_file'       => SSL_CERT_DIR . '/ssl.crt',
            'ssl_key_file'        => SSL_CERT_DIR . '/ssl.key',
            'open_http2_protocol' => true,
            'worker_num'          => 1,   //一般设置为服务器CPU数的1-4倍
            'daemonize'           => true,  //以守护进程执行
            'max_request'         => 10000, //
            'dispatch_mode'       => 2, // 数据包分发策略:固定模式
            'task_worker_num'     => 8,  //task进程的数量
            "task_ipc_mode "      => 3,  //使用消息队列通信，并设置为争抢模式
            'log_file'            => LOG_DIR . '/sms_socket_server.log',
        ]);

        $this->_http->on('Request', array($this, 'onRequests'));
        $this->_http->on('Task', array($this, 'onTask'));
        $this->_http->on('Finish', function ($serv, $task_id, $data) {
            echo "Task {$task_id} finish\n";
            echo "Result: {$data}\n\n\n\n";
        });
    }

    public function run()
    {
        $this->_http->start();
    }

    public function onRequests($request, $response)
    {
        $response->status('200');
        $server = $request->server;

        // 检查请求分发任务
        if (isset($server['request_uri']) && $server['request_uri'] == '/sendsms') {
            if ( ! isset($server['query_string'])) {
                echo "no query string!\n";
                exit;
            }

            $query_string = $server['query_string'];
            parse_str($query_string, $data);
            if ( ! isset($data['mobile']) || ! isset($data['type']) || ! isset($data['message'])) {
                echo "params error!\n";
                exit;
            }

            if ( ! in_array($data['type'], self::SMS_TYPES)) {
                echo "invalid sms type id!\n";
                exit;
            }
            $this->_http->task($data);
        }
        $result = ob_get_contents();
        @ob_end_clean();
        if ($result == '') {
            $result = ' ';
        }
        $response->end($result);
    }

    public function onTask($serv, $task_id, $from_id, Array $data)
    {
        $type   = $data['type'];
        $to     = $data['mobile'];
        $vcode  = $data['message'];

        $result = '';
        switch ($type) {
            case self::RONGLIAN_YZM:
                $sender    = new RongLian();
                $result    = $sender->send($to, $vcode, '201380');
                break;
            default:
                return false;
        }

        echo $result;

        return $result;
    }

    private static $_instance;

    public static function getInstance()
    {
        $ip      = isset($argv[1]) ? $argv[1] : '127.0.0.1';
        $port    = isset($argv[2]) ? $argv[2] : 9502;
        $address = $ip . ':' . $port;
        if ( ! self::$_instance[$address] instanceof self) {
            self::$_instance[$address] = new self($ip, $port);
        }

        return self::$_instance[$address];
    }
}

class RongLian
{
    use Singleton;

    //主帐号
    public $accountSid = '8a216da85e0e48b2015e2c6cef1d0a05';

    //主帐号Token
    public $accountToken = '0d7b3f598dcf4f5da140522f702b5112';

    //应用Id
    public $appId = '8a216da85e0e48b2015e2c6cf5df0a0b';

    //请求地址，格式如下，不需要写https://
    public $serverIP = 'app.cloopen.com';

    //请求端口
    public $serverPort = '8883';

    //REST版本号
    public $softVersion = '2013-12-26';

    public function send($to,$vcode,$tempId) {
        $rest = new REST($this->serverIP, $this->serverPort, $this->softVersion);
        $rest->setAccount($this->accountSid,$this->accountToken);
        $rest->setAppId($this->appId);
        $result = $rest->sendTemplateSMS($to,[$vcode, '', '', '', '', '', '', '', '', '5'],$tempId);

        if($result == NULL ) {
            return false;
        }
        if($result->statusCode!=0) {
            return false;
        }
        return true;
    }
}

class REST
{
    private $AccountSid;
    private $AccountToken;
    private $AppId;
    private $SubAccountSid;
    private $SubAccountToken;
    private $ServerIP;
    private $ServerPort;
    private $SoftVersion;
    private $Batch;  //时间sh
    private $BodyType = "xml";//包体格式，可填值：json 、xml
    private $enabeLog = true; //日志开关。可填值：true、
    private $Filename = "/tmp/ronglian-sms-log.txt"; //日志文件
    private $Handle;

    function __construct($ServerIP, $ServerPort, $SoftVersion)
    {
        $this->Batch       = date("YmdHis");
        $this->ServerIP    = $ServerIP;
        $this->ServerPort  = $ServerPort;
        $this->SoftVersion = $SoftVersion;
        $this->Handle      = fopen($this->Filename, 'a');
    }

    /**
     * 设置主帐号
     *
     * @param AccountSid 主帐号
     * @param AccountToken 主帐号Token
     */
    function setAccount($AccountSid, $AccountToken)
    {
        $this->AccountSid   = $AccountSid;
        $this->AccountToken = $AccountToken;
    }

    /**
     * 设置子帐号
     *
     * @param SubAccountSid 子帐号
     * @param SubAccountToken 子帐号Token
     */
    function setSubAccount($SubAccountSid, $SubAccountToken)
    {
        $this->SubAccountSid   = $SubAccountSid;
        $this->SubAccountToken = $SubAccountToken;
    }

    /**
     * 设置应用ID
     *
     * @param AppId 应用ID
     */
    function setAppId($AppId)
    {
        $this->AppId = $AppId;
    }

    /**
     * 打印日志
     *
     * @param log 日志内容
     */
    function showlog($log)
    {
        if ($this->enabeLog) {
            fwrite($this->Handle, $log . "\n");
        }
    }

    /**
     * 发起HTTPS请求
     */
    function curl_post($url, $data, $header, $post = 1)
    {
        //初始化curl
        $ch = curl_init();
        //参数设置
        $res = curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, $post);
        if ($post) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $result = curl_exec($ch);
        //连接失败
        if ($result == false) {
            if ($this->BodyType == 'json') {
                $result = "{\"statusCode\":\"172001\",\"statusMsg\":\"网络错误\"}";
            } else {
                $result = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><Response><statusCode>172001</statusCode><statusMsg>网络错误</statusMsg></Response>";
            }
        }

        curl_close($ch);

        return $result;
    }

    /**
     * 创建子帐号
     *
     * @param friendlyName 子帐号名称
     */
    function createSubAccount($friendlyName)
    {
        //主帐号鉴权信息验证，对必选参数进行判空。
        $auth = $this->accAuth();
        if ($auth != "") {
            return $auth;
        }
        // 拼接请求包体
        if ($this->BodyType == "json") {
            $body = "{'appId':'$this->AppId','friendlyName':'$friendlyName'}";
        } else {
            $body = "<SubAccount>
                    <appId>$this->AppId</appId>
                    <friendlyName>$friendlyName</friendlyName>
                  </SubAccount>";
        }
        $this->showlog("request body = " . $body);
        // 大写的sig参数
        $sig = strtoupper(md5($this->AccountSid . $this->AccountToken . $this->Batch));
        // 生成请求URL
        $url = "https://$this->ServerIP:$this->ServerPort/$this->SoftVersion/Accounts/$this->AccountSid/SubAccounts?sig=$sig";
        $this->showlog("request url = " . $url);
        // 生成授权：主帐号Id + 英文冒号 + 时间戳
        $authen = base64_encode($this->AccountSid . ":" . $this->Batch);
        // 生成包头
        $header = array(
            "Accept:application/$this->BodyType",
            "Content-Type:application/$this->BodyType;charset=utf-8",
            "Authorization:$authen",
        );
        // 发请求
        $result = $this->curl_post($url, $body, $header);
        $this->showlog("response body = " . $result);
        if ($this->BodyType == "json") {//JSON格式
            $datas = json_decode($result);
        } else { //xml格式
            $datas = simplexml_load_string(trim($result, " \t\n\r"));
        }
        //  if($datas == FALSE){
//            $datas = new stdClass();
//            $datas->statusCode = '172003';
//            $datas->statusMsg = '返回包体错误';
//        }
        return $datas;
    }

    /**
     * 获取子帐号
     *
     * @param startNo 开始的序号，默认从0开始
     * @param offset 一次查询的最大条数，最小是1条，最大是100条
     */
    function getSubAccounts($startNo, $offset)
    {
        //主帐号鉴权信息验证，对必选参数进行判空。
        $auth = $this->accAuth();
        if ($auth != "") {
            return $auth;
        }
        // 拼接请求包体
        $body = "
            <SubAccount>
              <appId>$this->AppId</appId>
              <startNo>$startNo</startNo>  
              <offset>$offset</offset>
            </SubAccount>";
        if ($this->BodyType == "json") {
            $body = "{'appId':'$this->AppId','startNo':'$startNo','offset':'$offset'}";
        } else {
            $body = "
            <SubAccount>
              <appId>$this->AppId</appId>
              <startNo>$startNo</startNo>  
              <offset>$offset</offset>
            </SubAccount>";
        }
        $this->showlog("request body = " . $body);
        // 大写的sig参数
        $sig = strtoupper(md5($this->AccountSid . $this->AccountToken . $this->Batch));
        // 生成请求URL
        $url = "https://$this->ServerIP:$this->ServerPort/$this->SoftVersion/Accounts/$this->AccountSid/GetSubAccounts?sig=$sig";
        $this->showlog("request url = " . $url);
        // 生成授权：主帐户Id + 英文冒号 + 时间戳。
        $authen = base64_encode($this->AccountSid . ":" . $this->Batch);
        // 生成包头
        $header = array(
            "Accept:application/$this->BodyType",
            "Content-Type:application/$this->BodyType;charset=utf-8",
            "Authorization:$authen",
        );
        // 发送请求
        $result = $this->curl_post($url, $body, $header);
        $this->showlog("response body = " . $result);
        if ($this->BodyType == "json") {//JSON格式
            $datas = json_decode($result);
        } else { //xml格式
            $datas = simplexml_load_string(trim($result, " \t\n\r"));
        }
        //  if($datas == FALSE){
//            $datas = new stdClass();
//            $datas->statusCode = '172003';
//            $datas->statusMsg = '返回包体错误';
//        }
        return $datas;
    }

    /**
     * 子帐号信息查询
     *
     * @param friendlyName 子帐号名称
     */
    function querySubAccount($friendlyName)
    {
        //主帐号鉴权信息验证，对必选参数进行判空。
        $auth = $this->accAuth();
        if ($auth != "") {
            return $auth;
        }
        // 拼接请求包体

        if ($this->BodyType == "json") {
            $body = "{'appId':'$this->AppId','friendlyName':'$friendlyName'}";
        } else {
            $body = "
            <SubAccount>
              <appId>$this->AppId</appId>
              <friendlyName>$friendlyName</friendlyName>
            </SubAccount>";
        }
        $this->showlog("request body = " . $body);
        // 大写的sig参数
        $sig = strtoupper(md5($this->AccountSid . $this->AccountToken . $this->Batch));
        // 生成请求URL
        $url = "https://$this->ServerIP:$this->ServerPort/$this->SoftVersion/Accounts/$this->AccountSid/QuerySubAccountByName?sig=$sig";
        $this->showlog("request url = " . $url);
        // 生成授权：主帐户Id + 英文冒号 + 时间戳。
        $authen = base64_encode($this->AccountSid . ":" . $this->Batch);
        // 生成包头
        $header = array(
            "Accept:application/$this->BodyType",
            "Content-Type:application/$this->BodyType;charset=utf-8",
            "Authorization:$authen",
        );
        // 发送请求
        $result = $this->curl_post($url, $body, $header);
        $this->showlog("response body = " . $result);
        if ($this->BodyType == "json") {//JSON格式
            $datas = json_decode($result);
        } else { //xml格式
            $datas = simplexml_load_string(trim($result, " \t\n\r"));
        }
        //  if($datas == FALSE){
//            $datas = new stdClass();
//            $datas->statusCode = '172003';
//            $datas->statusMsg = '返回包体错误';
//        }
        return $datas;
    }

    /**
     * 发送模板短信
     *
     * @param to 短信接收彿手机号码集合,用英文逗号分开
     * @param datas 内容数据
     * @param $tempId 模板Id
     */
    function sendTemplateSMS($to, $datas, $tempId)
    {
        //主帐号鉴权信息验证，对必选参数进行判空。
        $auth = $this->accAuth();
        if ($auth != "") {
            return $auth;
        }
        // 拼接请求包体
        if ($this->BodyType == "json") {
            $data = "";
            for ($i = 0; $i < count($datas); $i++) {
                $data = $data . "'" . $datas[$i] . "',";
            }
            $body = "{'to':'$to','templateId':'$tempId','appId':'$this->AppId','datas':[" . $data . "]}";
        } else {
            $data = "";
            for ($i = 0; $i < count($datas); $i++) {
                $data = $data . "<data>" . $datas[$i] . "</data>";
            }
            $body = "<TemplateSMS>
                    <to>$to</to> 
                    <appId>$this->AppId</appId>
                    <templateId>$tempId</templateId>
                    <datas>" . $data . "</datas>
                  </TemplateSMS>";
        }
        $this->showlog("request body = " . $body);
        // 大写的sig参数
        $sig = strtoupper(md5($this->AccountSid . $this->AccountToken . $this->Batch));
        // 生成请求URL
        $url = "https://$this->ServerIP:$this->ServerPort/$this->SoftVersion/Accounts/$this->AccountSid/SMS/TemplateSMS?sig=$sig";
        $this->showlog("request url = " . $url);
        // 生成授权：主帐户Id + 英文冒号 + 时间戳。
        $authen = base64_encode($this->AccountSid . ":" . $this->Batch);
        // 生成包头
        $header = array(
            "Accept:application/$this->BodyType",
            "Content-Type:application/$this->BodyType;charset=utf-8",
            "Authorization:$authen",
        );
        // 发送请求
        $result = $this->curl_post($url, $body, $header);
        $this->showlog("response body = " . $result);
        if ($this->BodyType == "json") {//JSON格式
            $datas = json_decode($result);
        } else { //xml格式
            $datas = simplexml_load_string(trim($result, " \t\n\r"));
        }
        //  if($datas == FALSE){
//            $datas = new stdClass();
//            $datas->statusCode = '172003';
//            $datas->statusMsg = '返回包体错误';
//        }
        //重新装填数据
        if ($datas->statusCode == 0) {
            if ($this->BodyType == "json") {
                $datas->TemplateSMS = $datas->templateSMS;
                unset($datas->templateSMS);
            }
        }

        return $datas;
    }

    /**
     * 外呼通知
     *
     * @param to 被叫号码
     * @param mediaName 语音文件名称，格式 wav。与mediaTxt不能同时为空。当不为空时mediaTxt属性失效。
     * @param mediaTxt 文本内容
     * @param displayNum 显示的主叫号码
     * @param playTimes 循环播放次数，1－3次，默认播放1次。
     * @param respUrl 外呼通知状态通知回调地址，云通讯平台将向该Url地址发送呼叫结果通知。
     * @param userData 用户私有数据
     * @param maxCallTime 最大通话时长
     * @param speed 发音速度
     * @param volume 音量
     * @param pitch 音调
     * @param bgsound 背景音编号
     */
    function landingCall(
        $to,
        $mediaName,
        $mediaTxt,
        $displayNum,
        $playTimes,
        $respUrl,
        $userData,
        $maxCallTime,
        $speed,
        $volume,
        $pitch,
        $bgsound
    ) {
        //主帐号鉴权信息验证，对必选参数进行判空。
        $auth = $this->accAuth();
        if ($auth != "") {
            return $auth;
        }
        // 拼接请求包体
        if ($this->BodyType == "json") {
            $body = "{'playTimes':'$playTimes','mediaTxt':'$mediaTxt','mediaName':'$mediaName','to':'$to','appId':'$this->AppId','displayNum':'$displayNum','respUrl':'$respUrl',
           'userData':'$userData','maxCallTime':'$maxCallTime','speed':'$speed','volume':'$volume','pitch':'$pitch','bgsound':'$bgsound'}";
        } else {
            $body = "<LandingCall>
                    <to>$to</to>
                    <mediaName>$mediaName</mediaName>
                    <mediaTxt>$mediaTxt</mediaTxt> 
                    <appId>$this->AppId</appId>
                    <displayNum>$displayNum</displayNum>
                    <playTimes>$playTimes</playTimes>
                    <respUrl>$respUrl</respUrl>
                    <userData>$userData</userData>
                    <maxCallTime>$maxCallTime</maxCallTime>
                    <speed>$speed</speed>
                    <volume>$volume</volume>
                    <pitch>$pitch</pitch>
                    <bgsound>$bgsound</bgsound>
                  </LandingCall>";
        }
        $this->showlog("request body = " . $body);
        // 大写的sig参数
        $sig = strtoupper(md5($this->AccountSid . $this->AccountToken . $this->Batch));
        // 生成请求URL
        $url = "https://$this->ServerIP:$this->ServerPort/$this->SoftVersion/Accounts/$this->AccountSid/Calls/LandingCalls?sig=$sig";
        $this->showlog("request url = " . $url);
        // 生成授权：主帐户Id + 英文冒号 + 时间戳。
        $authen = base64_encode($this->AccountSid . ":" . $this->Batch);
        // 生成包头
        $header = array(
            "Accept:application/$this->BodyType",
            "Content-Type:application/$this->BodyType;charset=utf-8",
            "Authorization:$authen",
        );
        // 发送请求
        $result = $this->curl_post($url, $body, $header);
        $this->showlog("response body = " . $result);
        if ($this->BodyType == "json") {//JSON格式
            $datas = json_decode($result);
        } else { //xml格式
            $datas = simplexml_load_string(trim($result, " \t\n\r"));
        }
        //  if($datas == FALSE){
//            $datas = new stdClass();
//            $datas->statusCode = '172003';
//            $datas->statusMsg = '返回包体错误';
//        }
        return $datas;
    }

    /**
     * 语音验证码
     *
     * @param verifyCode 验证码内容，为数字和英文字母，不区分大小写，长度4-8位
     * @param playTimes 播放次数，1－3次
     * @param to 接收号码
     * @param displayNum 显示的主叫号码
     * @param respUrl 语音验证码状态通知回调地址，云通讯平台将向该Url地址发送呼叫结果通知
     * @param lang 语言类型
     * @param userData 第三方私有数据
     */
    function voiceVerify($verifyCode, $playTimes, $to, $displayNum, $respUrl, $lang, $userData)
    {
        //主帐号鉴权信息验证，对必选参数进行判空。
        $auth = $this->accAuth();
        if ($auth != "") {
            return $auth;
        }
        // 拼接请求包体
        if ($this->BodyType == "json") {
            $body = "{'appId':'$this->AppId','verifyCode':'$verifyCode','playTimes':'$playTimes','to':'$to','respUrl':'$respUrl','displayNum':'$displayNum',
           'lang':'$lang','userData':'$userData'}";
        } else {
            $body = "<VoiceVerify>
                    <appId>$this->AppId</appId>
                    <verifyCode>$verifyCode</verifyCode>
                    <playTimes>$playTimes</playTimes>
                    <to>$to</to>
                    <respUrl>$respUrl</respUrl>
                    <displayNum>$displayNum</displayNum>
                    <lang>$lang</lang>
                    <userData>$userData</userData>
                  </VoiceVerify>";
        }
        $this->showlog("request body = " . $body);
        // 大写的sig参数
        $sig = strtoupper(md5($this->AccountSid . $this->AccountToken . $this->Batch));
        // 生成请求URL
        $url = "https://$this->ServerIP:$this->ServerPort/$this->SoftVersion/Accounts/$this->AccountSid/Calls/VoiceVerify?sig=$sig";
        $this->showlog("request url = " . $url);
        // 生成授权：主帐户Id + 英文冒号 + 时间戳。
        $authen = base64_encode($this->AccountSid . ":" . $this->Batch);
        // 生成包头
        $header = array(
            "Accept:application/$this->BodyType",
            "Content-Type:application/$this->BodyType;charset=utf-8",
            "Authorization:$authen",
        );
        // 发送请求
        $result = $this->curl_post($url, $body, $header);
        $this->showlog("response body = " . $result);
        if ($this->BodyType == "json") {//JSON格式
            $datas = json_decode($result);
        } else { //xml格式
            $datas = simplexml_load_string(trim($result, " \t\n\r"));
        }
        //  if($datas == FALSE){
//            $datas = new stdClass();
//            $datas->statusCode = '172003';
//            $datas->statusMsg = '返回包体错误';
//        }
        return $datas;
    }

    /**
     * IVR外呼
     *
     * @param number   待呼叫号码，为Dial节点的属性
     * @param userdata 用户数据，在<startservice>通知中返回，只允许填写数字字符，为Dial节点的属性
     * @param record   是否录音，可填项为true和false，默认值为false不录音，为Dial节点的属性
     */
    function ivrDial($number, $userdata, $record)
    {
        //主帐号鉴权信息验证，对必选参数进行判空。
        $auth = $this->accAuth();
        if ($auth != "") {
            return $auth;
        }
        // 拼接请求包体
        $body = " <Request>
                  <Appid>$this->AppId</Appid>
                  <Dial number='$number'  userdata='$userdata' record='$record'></Dial>
                </Request>";
        $this->showlog("request body = " . $body);
        // 大写的sig参数
        $sig = strtoupper(md5($this->AccountSid . $this->AccountToken . $this->Batch));
        // 生成请求URL
        $url = "https://$this->ServerIP:$this->ServerPort/$this->SoftVersion/Accounts/$this->AccountSid/ivr/dial?sig=$sig";
        $this->showlog("request url = " . $url);
        // 生成授权：主帐户Id + 英文冒号 + 时间戳。
        $authen = base64_encode($this->AccountSid . ":" . $this->Batch);
        // 生成包头
        $header = array(
            "Accept:application/xml",
            "Content-Type:application/xml;charset=utf-8",
            "Authorization:$authen",
        );
        // 发送请求
        $result = $this->curl_post($url, $body, $header);
        $this->showlog("response body = " . $result);
        $datas = simplexml_load_string(trim($result, " \t\n\r"));
        //  if($datas == FALSE){
//            $datas = new stdClass();
//            $datas->statusCode = '172003';
//            $datas->statusMsg = '返回包体错误';
//        }
        return $datas;
    }

    /**
     * 话单下载
     *
     * @param date     day 代表前一天的数据（从00:00 – 23:59）
     * @param keywords   客户的查询条件，由客户自行定义并提供给云通讯平台。默认不填忽略此参数
     */
    function billRecords($date, $keywords)
    {
        //主帐号鉴权信息验证，对必选参数进行判空。
        $auth = $this->accAuth();
        if ($auth != "") {
            return $auth;
        }
        // 拼接请求包体
        if ($this->BodyType == "json") {
            $body = "{'appId':'$this->AppId','date':'$date','keywords':'$keywords'}";
        } else {
            $body = "<BillRecords>
                    <appId>$this->AppId</appId>
                    <date>$date</date>
                    <keywords>$keywords</keywords>
                  </BillRecords>";
        }
        $this->showlog("request body = " . $body);
        // 大写的sig参数
        $sig = strtoupper(md5($this->AccountSid . $this->AccountToken . $this->Batch));
        // 生成请求URL
        $url = "https://$this->ServerIP:$this->ServerPort/$this->SoftVersion/Accounts/$this->AccountSid/BillRecords?sig=$sig";
        $this->showlog("request url = " . $url);
        // 生成授权：主帐户Id + 英文冒号 + 时间戳。
        $authen = base64_encode($this->AccountSid . ":" . $this->Batch);
        // 生成包头
        $header = array(
            "Accept:application/$this->BodyType",
            "Content-Type:application/$this->BodyType;charset=utf-8",
            "Authorization:$authen",
        );
        // 发送请求
        $result = $this->curl_post($url, $body, $header);
        $this->showlog("response body = " . $result);
        if ($this->BodyType == "json") {//JSON格式
            $datas = json_decode($result);
        } else { //xml格式
            $datas = simplexml_load_string(trim($result, " \t\n\r"));
        }
        //  if($datas == FALSE){
//            $datas = new stdClass();
//            $datas->statusCode = '172003';
//            $datas->statusMsg = '返回包体错误';
//        }
        return $datas;
    }

    /**
     * 主帐号信息查询
     */
    function queryAccountInfo()
    {
        //主帐号鉴权信息验证，对必选参数进行判空。
        $auth = $this->accAuth();
        if ($auth != "") {
            return $auth;
        }
        // 大写的sig参数
        $sig = strtoupper(md5($this->AccountSid . $this->AccountToken . $this->Batch));
        // 生成请求URL
        $url = "https://$this->ServerIP:$this->ServerPort/$this->SoftVersion/Accounts/$this->AccountSid/AccountInfo?sig=$sig";
        $this->showlog("request url = " . $url);
        // 生成授权：主帐户Id + 英文冒号 + 时间戳。
        $authen = base64_encode($this->AccountSid . ":" . $this->Batch);
        // 生成包头
        $header = array(
            "Accept:application/$this->BodyType",
            "Content-Type:application/$this->BodyType;charset=utf-8",
            "Authorization:$authen",
        );
        // 发送请求
        $result = $this->curl_post($url, "", $header, 0);
        $this->showlog("response body = " . $result);
        if ($this->BodyType == "json") {//JSON格式
            $datas = json_decode($result);
        } else { //xml格式
            $datas = simplexml_load_string(trim($result, " \t\n\r"));
        }
        //  if($datas == FALSE){
//            $datas = new stdClass();
//            $datas->statusCode = '172003';
//            $datas->statusMsg = '返回包体错误';
//        }
        return $datas;
    }

    /**
     * 短信模板查询
     *
     * @param date     templateId 模板ID
     */
    function QuerySMSTemplate($templateId)
    {
        //主帐号鉴权信息验证，对必选参数进行判空。
        $auth = $this->accAuth();
        if ($auth != "") {
            return $auth;
        }
        // 拼接请求包体
        if ($this->BodyType == "json") {
            $body = "{'appId':'$this->AppId','templateId':'$templateId'}";
        } else {
            $body = "<Request>
                    <appId>$this->AppId</appId>
                    <templateId>$templateId</templateId>  
                  </Request>";
        }
        $this->showlog("request body = " . $body);
        // 大写的sig参数
        $sig = strtoupper(md5($this->AccountSid . $this->AccountToken . $this->Batch));
        // 生成请求URL
        $url = "https://$this->ServerIP:$this->ServerPort/$this->SoftVersion/Accounts/$this->AccountSid/SMS/QuerySMSTemplate?sig=$sig";
        $this->showlog("request url = " . $url);
        // 生成授权：主帐户Id + 英文冒号 + 时间戳。
        $authen = base64_encode($this->AccountSid . ":" . $this->Batch);
        // 生成包头
        $header = array(
            "Accept:application/$this->BodyType",
            "Content-Type:application/$this->BodyType;charset=utf-8",
            "Authorization:$authen",
        );
        // 发送请求
        $result = $this->curl_post($url, $body, $header);
        $this->showlog("response body = " . $result);
        if ($this->BodyType == "json") {//JSON格式
            $datas = json_decode($result);
        } else { //xml格式
            $datas = simplexml_load_string(trim($result, " \t\n\r"));
        }
        //  if($datas == FALSE){
//            $datas = new stdClass();
//            $datas->statusCode = '172003';
//            $datas->statusMsg = '返回包体错误';
//        }
        return $datas;
    }

    /**
     * 呼叫状态查询
     *
     * @param callid     呼叫Id
     * @param action   查询结果通知的回调url地址
     */
    function QueryCallState($callid, $action)
    {
        //主帐号鉴权信息验证，对必选参数进行判空。
        $auth = $this->accAuth();
        if ($auth != "") {
            return $auth;
        }
        // 拼接请求包体
        if ($this->BodyType == "json") {
            $body = "{'Appid':'$this->AppId','QueryCallState':{'callid':'$callid','action':'$action'}}";
        } else {
            $body = "<Request>
                    <Appid>$this->AppId</Appid>
                    <QueryCallState callid ='$callid' action='$action'/>
                  </Request>";
        }
        $this->showlog("request body = " . $body);
        // 大写的sig参数
        $sig = strtoupper(md5($this->AccountSid . $this->AccountToken . $this->Batch));
        // 生成请求URL
        $url = "https://$this->ServerIP:$this->ServerPort/$this->SoftVersion/Accounts/$this->AccountSid/ivr/call?sig=$sig&callid=$callid";
        $this->showlog("request url = " . $url);
        // 生成授权：主帐户Id + 英文冒号 + 时间戳。
        $authen = base64_encode($this->AccountSid . ":" . $this->Batch);
        // 生成包头
        $header = array(
            "Accept:application/$this->BodyType",
            "Content-Type:application/$this->BodyType;charset=utf-8",
            "Authorization:$authen",
        );
        // 发送请求
        $result = $this->curl_post($url, $body, $header);
        $this->showlog("response body = " . $result);
        if ($this->BodyType == "json") {//JSON格式
            $datas = json_decode($result);
        } else { //xml格式
            $datas = simplexml_load_string(trim($result, " \t\n\r"));
        }
        //  if($datas == FALSE){
//            $datas = new stdClass();
//            $datas->statusCode = '172003';
//            $datas->statusMsg = '返回包体错误';
//        }
        return $datas;
    }

    /**
     * 呼叫结果查询
     *
     * @param callSid     呼叫Id
     */
    function CallResult($callSid)
    {
        //主帐号鉴权信息验证，对必选参数进行判空。
        $auth = $this->accAuth();
        if ($auth != "") {
            return $auth;
        }
        // 大写的sig参数
        $sig = strtoupper(md5($this->AccountSid . $this->AccountToken . $this->Batch));
        // 生成请求URL
        $url = "https://$this->ServerIP:$this->ServerPort/$this->SoftVersion/Accounts/$this->AccountSid/CallResult?sig=$sig&callsid=$callSid";
        $this->showlog("request url = " . $url);
        // 生成授权：主帐户Id + 英文冒号 + 时间戳。
        $authen = base64_encode($this->AccountSid . ":" . $this->Batch);
        // 生成包头
        $header = array(
            "Accept:application/$this->BodyType",
            "Content-Type:application/$this->BodyType;charset=utf-8",
            "Authorization:$authen",
        );
        // 发送请求
        $result = $this->curl_post($url, "", $header, 0);
        $this->showlog("response body = " . $result);
        if ($this->BodyType == "json") {//JSON格式
            $datas = json_decode($result);
        } else { //xml格式
            $datas = simplexml_load_string(trim($result, " \t\n\r"));
        }
        //  if($datas == FALSE){
//            $datas = new stdClass();
//            $datas->statusCode = '172003';
//            $datas->statusMsg = '返回包体错误';
//        }
        return $datas;
    }

    /**
     * 语音文件上传
     *
     * @param filename     文件名
     * @param body   二进制串
     */
    function MediaFileUpload($filename, $body)
    {
        //主帐号鉴权信息验证，对必选参数进行判空。
        $auth = $this->accAuth();
        if ($auth != "") {
            return $auth;
        }
        // 拼接请求包体

        $this->showlog("request body = " . $body);
        // 大写的sig参数
        $sig = strtoupper(md5($this->AccountSid . $this->AccountToken . $this->Batch));
        // 生成请求URL
        $url = "https://$this->ServerIP:$this->ServerPort/$this->SoftVersion/Accounts/$this->AccountSid/Calls/MediaFileUpload?sig=$sig&appid=$this->AppId&filename=$filename";
        $this->showlog("request url = " . $url);
        // 生成授权：主帐户Id + 英文冒号 + 时间戳。
        $authen = base64_encode($this->AccountSid . ":" . $this->Batch);
        // 生成包头
        $header = array(
            "Accept:application/$this->BodyType",
            "Content-Type:application/octet-stream",
            "Authorization:$authen",
        );
        // 发送请求
        $result = $this->curl_post($url, $body, $header);
        $this->showlog("response body = " . $result);
        if ($this->BodyType == "json") {//JSON格式
            $datas = json_decode($result);
        } else { //xml格式
            $datas = simplexml_load_string(trim($result, " \t\n\r"));
        }
        //  if($datas == FALSE){
//            $datas = new stdClass();
//            $datas->statusCode = '172003';
//            $datas->statusMsg = '返回包体错误';
//        }
        return $datas;
    }

    /**
     * 子帐号鉴权
     */
    function subAuth()
    {
        if ($this->ServerIP == "") {
            $data             = new stdClass();
            $data->statusCode = '172004';
            $data->statusMsg  = 'serverIP为空';

            return $data;
        }
        if ($this->ServerPort <= 0) {
            $data             = new stdClass();
            $data->statusCode = '172005';
            $data->statusMsg  = '端口错误（小于等于0）';

            return $data;
        }
        if ($this->SoftVersion == "") {
            $data             = new stdClass();
            $data->statusCode = '172013';
            $data->statusMsg  = '版本号为空';

            return $data;
        }
        if ($this->SubAccountSid == "") {
            $data             = new stdClass();
            $data->statusCode = '172008';
            $data->statusMsg  = '子帐号为空';

            return $data;
        }
        if ($this->SubAccountToken == "") {
            $data             = new stdClass();
            $data->statusCode = '172009';
            $data->statusMsg  = '子帐号令牌为空';

            return $data;
        }
        if ($this->AppId == "") {
            $data             = new stdClass();
            $data->statusCode = '172012';
            $data->statusMsg  = '应用ID为空';

            return $data;
        }
    }

    /**
     * 主帐号鉴权
     */
    function accAuth()
    {
        if ($this->ServerIP == "") {
            $data             = new stdClass();
            $data->statusCode = '172004';
            $data->statusMsg  = 'serverIP为空';

            return $data;
        }
        if ($this->ServerPort <= 0) {
            $data             = new stdClass();
            $data->statusCode = '172005';
            $data->statusMsg  = '端口错误（小于等于0）';

            return $data;
        }
        if ($this->SoftVersion == "") {
            $data             = new stdClass();
            $data->statusCode = '172013';
            $data->statusMsg  = '版本号为空';

            return $data;
        }
        if ($this->AccountSid == "") {
            $data             = new stdClass();
            $data->statusCode = '172006';
            $data->statusMsg  = '主帐号为空';

            return $data;
        }
        if ($this->AccountToken == "") {
            $data             = new stdClass();
            $data->statusCode = '172007';
            $data->statusMsg  = '主帐号令牌为空';

            return $data;
        }
        if ($this->AppId == "") {
            $data             = new stdClass();
            $data->statusCode = '172012';
            $data->statusMsg  = '应用ID为空';

            return $data;
        }
    }
}

trait Singleton
{
    /**
     * private construct, generally defined by using class
     */
    //private function __construct() {}

    /**
     * @return static
     */
    private static $_instance;

    public static function getInstance()
    {
        if ( ! self::$_instance instanceof self) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function __clone()
    {
        trigger_error('Cloning ' . __CLASS__ . ' is not allowed.', E_USER_ERROR);
    }

    public function __wakeup()
    {
        trigger_error('Unserializing ' . __CLASS__ . ' is not allowed.', E_USER_ERROR);
    }
}

Sms::getInstance()->run();
