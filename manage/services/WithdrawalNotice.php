<?php

require_once 'BaseService.php';

date_default_timezone_set("Asia/Shanghai");
error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('display_startup_errors', true);

define('HOST', '0.0.0.0');
define('PORT', 9503);

define('LOG_DIR', __DIR__ . '/logs');

class WithdrawalNotice
{
    use Singleton;

    public $logFilePath = LOG_DIR . '/Withdrawal-Notice.logs';

    public function run()
    {

        $redis = new Redis();
        $redis->connect('127.0.0.1', 6379);
        $redis->set("fd", "[]"); //每次第一次执行都需要先清空reids里面的标识

        $_websocket = new swoole_websocket_server(HOST, PORT);

        $_websocket->set([
            'worker_num'          => 1,   //一般设置为服务器CPU数的1-4倍
            'daemonize'           => true,  //以守护进程执行
            'max_request'         => 10000, //
            'dispatch_mode'       => 2, // 数据包分发策略:固定模式
            'log_file'            => LOG_DIR . '/Withdrawal-Notice.logs',
        ]);

        $_websocket->on('open', function ($_websocket, $request) use ($redis) {

            $this->writeLogs("connection open: " . $request->fd);

            $aClientFds = json_decode($redis->get("fd"), true);
            if ($aClientFds == "") {
                $aClientFds = [];
            }

            if (!in_array($request->fd, $aClientFds)) {
                array_push($aClientFds, $request->fd); //压入数组
                $aClientFds = json_encode($aClientFds);
                $redis->set("fd", $aClientFds);
            }

        });

        $_websocket->on('message', function ($_websocket, $frame) use ($redis) {
            // echo "\n message: " . $frame->data . "\n";

            // $aClientFds = json_decode($redis->get("fd"), true);
            // foreach ($aClientFds as $key => $value) {
            //     // print_r($value);
            //     if ($frame->fd != $value) {
            //         print_r($value);
            //         $_websocket->push($value, "客户{$value}:" . $frame->data);
            //     }
            // }
        });

        $_websocket->on('close', function ($_websocket, $fd) use ($redis) {
            $this->writeLogs("connection close: " . $fd . "\n");

            $aClientFds                  = json_decode($redis->get("fd"), true);
            empty($aClientFds) or $point = array_keys($aClientFds, $fd, true); //search key
            empty($point) or array_splice($aClientFds, $point['0'], 1); //数组中删除
            $aClientFds = json_encode($aClientFds);
            $redis->set("fd", $aClientFds);
        });

        $_websocket->on('request', function ($request, $response) use ($_websocket, $redis) {
            $sData = json_encode($request->get);
            $this->writeLogs("request data: " . $sData);
            $this->writeLogs("fd data array: " . $redis->get("fd"));

            $aClientFds = json_decode($redis->get("fd"), true);
            if (is_array($aClientFds) && $sData) {
                foreach ($aClientFds as $iClientFd) {
                    $_websocket->push($iClientFd, $sData);
                }
                $response->end(json_encode(['success' => 1]));
            } else {
                $response->end(json_encode(['success' => 0, 'msg' => 'cilentFd and data must not null !']));
            }
        });

        $_websocket->start();
    }

    public function writeLogs($data, $logFilePath = "")
    {
        $logFilePath or $logFilePath = $this->logFilePath;
        file_put_contents($logFilePath, "[" . date('Y-m-d H:i:s') . "] ", FILE_APPEND);
        file_put_contents($logFilePath, $data, FILE_APPEND);
        file_put_contents($logFilePath, "\n", FILE_APPEND);
    }
}

WithdrawalNotice::getInstance()->run();
