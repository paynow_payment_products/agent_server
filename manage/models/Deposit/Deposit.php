<?php

namespace Deposit;

use BaseModel;
use Carbon\Carbon;
use Exception;

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/6
 * Time: 18:25
 * Class DepositModel
 *
 * @property                 $deposit_id
 * @property                 $order_no
 * @property                 $app_id
 * @property                 $club_id
 * @property                 $user_id
 * @property                 $platform_id
 * @property                 $bank_id
 * @property                 $amount
 * @property                 $order_id
 * @property                 $client_ip
 * @property                 $status
 * @property \Carbon\Carbon  $created_at
 * @property \Carbon\Carbon  $updated_at
 * @property \Carbon\Carbon  $deleted_at
 *********************************************
 * @property OrderModel      $order
 * @property PlatformModel   $platform
 * @property \User\UserModel $user
 * @property                 $logs
 * @property                 $notices
 */
class DepositModel extends BaseModel
{
    public $connection = 'default';
    public $primaryKey = 'deposit_id';
    public $table      = 'deposits';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public $fillable = [
        'order_no',
        'app_id',
        'user_id',
        'platform_id',
        'bank_id',
        'amount',
        'order_id',
        'client_ip',
        'status',
        'fee',
    ];

    public static $rules = [
        'user_id'     => 'required',
        'platform_id' => 'required',
        'amount'      => 'required|numeric|min:0.01',
    ];

    public static $relationsData = [
        'order'    => [self::BELONGS_TO, 'Deposit\OrderModel'],
        'platform' => [self::BELONGS_TO, 'Deposit\PlatformModel'],
        'user'     => [self::BELONGS_TO, 'User\UserModel'],
        'logs'     => [self::HAS_MANY, 'Deposit\LogModel', 'foreignKey' => 'deposit_id'],
        'notices'  => [self::HAS_MANY, 'Deposit\NoticeModel', 'foreignKey' => 'deposit_id'],
    ];

    public static $order = ['created_at' => 'desc'];

    public static $searchFormConf = [
        'order_no'    => ['type' => 'input', 'label' => '平台订单号'],
        'user_id'     => ['type' => 'input', 'label' => "ID"],
        'status'      => ['type' => 'select', 'source' => ['data' => self::STATUS_ENUM], 'label' => "订单状态"],
        'created_at'  => ['type' => 'date-range-picker', 'label' => "创建时间"],
        'client_ip'   => ['type' => 'input', 'label' => "IP"],
        'platform_id' => ['type' => 'select', 'source' => ['model' => '\Deposit\PlatformModel', "display_name" => 'name'], 'label' => "渠道"],
    ];

    public static $searchCondictionConf = [
        'user_id'     => ['='],
        'status'      => ['='],
        'created_at'  => ['date-range-picker'],
        'client_ip'   => ['='],
        'platform_id' => ['='],
        'order_no'    => ['='],
    ];

    const DEPOSIT_STATUS_NEW         = 0;
    const DEPOSIT_STATUS_SUCCESS     = 1;
    const DEPOSIT_STATUS_RECEIVED    = 2;
    const DEPOSIT_STATUS_WAITINGLOAD = 3;

    const STATUS_ENUM = [
        self::DEPOSIT_STATUS_NEW         => '待付款',
        self::DEPOSIT_STATUS_SUCCESS     => '完成',
        self::DEPOSIT_STATUS_RECEIVED    => '已付款',
        self::DEPOSIT_STATUS_WAITINGLOAD => '加币中',
    ];

    public function beforeCreate()
    {
        $this->order_no  = $this->generateDepositOrderNo(); //md5('PN/Cashier/app_id/' . $this->app_id . '/user_id/' . $this->user_id . '/' . milliseconds());
        $this->client_ip = get_client_ip();
        $this->status    = self::DEPOSIT_STATUS_NEW;
    }

    private function generateDepositOrderNo()
    {
        $order_no = 'DE';
        $order_no .= str_pad($this->app_id, 2, '0', STR_PAD_LEFT);
        $order_no .= str_pad($this->user_id, 6, '0', STR_PAD_LEFT);
        $order_no .= date('ymdHis');
        $order_no .= str_pad(milliseconds() % 10000, 4, '0', STR_PAD_LEFT);
        return substr(md5($order_no), 8, -8) . generateVCode(2);
    }

    public function getAmountAttribute($value)
    {
        return amount_format($value / AMOUNT_PRECISION);
    }

    public function setAmountAttribute($value)
    {
        if (!is_numeric($value)) {
            throw new Exception('invalid amount');
        }
        if ((($value * AMOUNT_PRECISION) % AMOUNT_PRECISION) != 0) {
            throw new Exception('invalid amount for this app');
        }
        if (empty($this->platform_id)) {
            throw new Exception('set platform_id first');
        }
        if (empty($this->platform)) {
            throw new Exception('invalid platform_id');
        }
        if ($value < $this->platform->min_load) {
            throw new Exception('invalid amount:too small');
        }
        if ($value > $this->platform->max_load) {
            throw new Exception('invalid amount:too big');
        }
        $amount                     = (int) ($value * AMOUNT_PRECISION);
        $this->attributes['amount'] = $amount;
    }

    public function isReceived()
    {
        return $this->status == self::DEPOSIT_STATUS_RECEIVED;
    }

    public function isNew()
    {
        return $this->status == self::DEPOSIT_STATUS_NEW;
    }

    public function setWaitingLoad()
    {
        $this->status = self::DEPOSIT_STATUS_WAITINGLOAD;

        return $this->save();
    }

    public function addDepositTask()
    {
        // add task
        return $this->notices()->create([
            'status'   => NoticeModel::STATUS_NEW,
            'start_at' => Carbon::now(),
        ]);
    }

    public function add_gold()
    {
        $this->user->account->incrementBalance($this->amount, 1, $this->order_no);
        $this->update([
            'status' => LogModel::STATUS_SUCCESS,
        ]);
    }
}
