<?php

namespace Deposit;

use BaseModel;

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/7
 * Time: 16:03
 * class LogModel
 *
 * @property              $log_id
 * @property              $deposit_id
 * @property              $request
 * @property              $status
 * @property              $ip
 * @property              $created_at
 * @property              $updated_at
 * @property              $deleted_at
 *************************************
 * @property DepositModel $deposit
 */
class LogModel extends BaseModel
{
    public $connection = 'default';
    public $table      = 'deposit_logs';
    public $primaryKey = 'log_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'request' => 'array',
    ];

    protected $fillable = [
        'deposit_id',
        'request',
        'status',
        'ip',
    ];

    protected static $relationsData = [
        'deposit' => [self::BELONGS_TO, 'Deposit\DepositModel'],
    ];

    const STATUS_NEW                    = 0;
    const STATUS_SUCCESS                = 1;
    const STATUS_SIGN_ERROR             = 2;
    const STATUS_DEPOSIT_FAILED         = 3;
    const STATUS_NON_DEPOSIT            = 4;
    const STATUS_AMOUNT_ERROR           = 5;
    const STATUS_SET_WAITINGLOAD_FAILED = 6;
    const STATUS_DEPOSIT_STATUS_ERROR   = 7;
    const STATUS_OTHER_ERROR            = 99;

    public $notifyStatus = [
        self::STATUS_NEW                    => '新创建',
        self::STATUS_SUCCESS                => '通知成功',
        self::STATUS_SIGN_ERROR             => '签名错误',
        self::STATUS_DEPOSIT_FAILED         => '充值失败',
        self::STATUS_NON_DEPOSIT            => '没有充值',
        self::STATUS_AMOUNT_ERROR           => '充值金额错误',
        self::STATUS_SET_WAITINGLOAD_FAILED => '设置等待加币状态失败',
        self::STATUS_DEPOSIT_STATUS_ERROR   => '订单状态错误',
        self::STATUS_OTHER_ERROR            => '其他错误',
    ];

    public function beforeCreate()
    {
        $this->status = self::STATUS_NEW;
    }

    public static $searchFormConf = [
        'created_at' => ['type' => 'date-range-picker', 'label' => "创建时间"],
        'deposit_id' => ['type' => 'hidden', 'label' => '平台订单号'],
    ];

    public static $searchCondictionConf = [
        'created_at' => ['date-range-picker'],
        'deposit_id' => ['='],
    ];
}
