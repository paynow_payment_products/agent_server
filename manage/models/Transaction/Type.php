<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/5
 * Time: 11:14
 */

namespace Transaction;

class TypeModel extends \BaseModel
{
    public $connection = 'default';
    public $table = 'transaction_types';
    public $primaryKey = 'type_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $guarded = ['type_id', 'created_at', 'updated_at', 'deleted_at'];

    protected static $relationsData = [
        'transactions' => [self::HAS_MANY, 'Transaction\TransactionModel'],
    ];
}