<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/16
 * Time: 18:37
 */

namespace Transaction;

use BaseModel;

/**
 * 帐变
 */
class TransactionModel extends BaseModel
{

    public $connection = 'default';
    public $table      = 'transactions';
    public $primaryKey = 'transaction_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $guarded = ['transaction_id', 'created_at', 'updated_at', 'deleted_at'];

    protected static $relationsData = [
        'user' => [self::BELONGS_TO, 'User\UserModel'],
        'type' => [self::BELONGS_TO, 'Transaction\TypeModel'],
    ];

    public static $order = [
        'created_at'     => 'desc',
        'transaction_id' => 'desc',
    ];

    protected $casts = [
        'extra' => 'array',
    ];

    public static $searchFormConf = [
        'user_id'    => ['type' => 'input', 'label' => "ID"],
        'type_id'    => ['type' => 'select', 'source' => ['model' => '\Transaction\OptypeModel', 'display_name' => 'optype_name'], 'label' => "订单状态"],
        'created_at' => ['type' => 'date-range-picker', 'label' => "创建时间"],
    ];

    public static $searchCondictionConf = [
        'user_id'    => ['='],
        'created_at' => ['date-range-picker'],
        'type_id'    => ['='],
    ];

    public function getBeforeAttribute($value)
    {
        return amount_format($value / 100);
    }

    public function setBeforeAttribute($value)
    {
        $this->attributes['before'] = $value * 100;
    }

    public function getAmountAttribute($value)
    {
        return amount_format($value / 100);
    }

    public function setAmountAttribute($value)
    {
        $this->attributes['amount'] = $value * 100;
    }

    public function getRemainAttribute($value)
    {
        return amount_format($value / 100);
    }

    public function setRemainAttribute($value)
    {
        $this->attributes['remain'] = $value * 100;
    }
}
