<?php

namespace User;

use BaseModel;

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/8/7
 * Time: 15:32
 */

class IpModel extends BaseModel
{

    public $connection = 'default';
    public $table = 'user_ips';
    public $primaryKey = 'ip_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $guarded = ['ip_id', 'created_at', 'updated_at', 'deleted_at'];

    protected static $relationsData = [
        'user'          => [self::BELONGS_TO, 'User\UserModel',],
    ];

    public function beforeCreate()
    {
        $this->ip = get_client_ip();
    }
}