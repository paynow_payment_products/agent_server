<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/12/7
 * Time: 17:05
 */

namespace User;

use Exception;
use Illuminate\Database\Capsule\Manager as DB;

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/11
 * Time: 22:03
 *
 * @property                             $user_id
 * @property                             $balance
 * ****************************
 * @property UserModel                    $user
 */
class AccountReportModel extends \BaseModel
{
    public $connection = 'default';
    public $table      = 'accounts_report';
    public $primaryKey = 'id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
    public $fillable = [
        'balance',
        'date',
        'user_id',
        'recharge',
        'withdraw',
        'turnout',
        'turnin',
        'systemadd',
        'systemdel',
        'reback',
        'testadd',
        'testdel',
        'name'
    ];
    protected static $relationsData = [
        'user' => [self::BELONGS_TO, 'User\UserModel'],
    ];
    
    public static function zbcount($user_id,$start,$end)
    {
        $userSql = "SELECT SUM(amount) as a_total,type_id FROM transactions WHERE user_id=".$user_id." AND created_at >'".$start."' AND created_at <'".$end."' GROUP BY type_id";
        $users = DB::select($userSql);
        return json_decode(json_encode($users),true);
    }

    public function getBalanceAttribute($value)
    {
        return amount_format($value / 100);
    }


    public function getRechargeAttribute($value)
    {
        return amount_format($value / 100);
    }

     public function getWithdrawAttribute($value)
    {
        return amount_format($value / 100);
    }


    public function getTurnoutAttribute($value)
    {
        return amount_format($value / 100);
    }

    public function getTurninAttribute($value)
    {
        return amount_format($value / 100);
    }

    public function getSystemaddAttribute($value)
    {
        return amount_format($value / 100);
    }

     public function getSystemdelAttribute($value)
    {
        return amount_format($value / 100);
    }

    public function getRebackAttribute($value)
    {
        return amount_format($value / 100);
    }

    public function getTestaddAttribute($value)
    {
        return amount_format($value / 100);
    }

    public function getTestdelAttribute($value)
    {
        return amount_format($value / 100);
    }
}
