<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/12/7
 * Time: 17:05
 */

namespace User;

use Exception;
use Illuminate\Database\Capsule\Manager as DB;

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/11
 * Time: 22:03
 *
 * @property                             $user_id
 * @property                             $balance
 * ****************************
 * @property UserModel                    $user
 */
class AccountModel extends \BaseModel
{
    public $connection = 'default';
    public $table      = 'accounts';
    public $primaryKey = 'user_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $guarded = ['user_id', 'created_at', 'updated_at', 'deleted_at'];

    protected static $relationsData = [
        'user' => [self::BELONGS_TO, 'User\UserModel'],
    ];

    public function getBalanceAttribute($value)
    {
        return amount_format($value / 100);
    }

    public function setBalanceAttribute($value)
    {
        $this->attributes['Balance'] = $value * 100;
    }

    public function incrementBalance($amount, $transaction_type, $bill_id = '')
    {
        DB::transaction(function () use ($amount, $transaction_type, $bill_id) {
            $account = DB::table('accounts')->where('user_id', $this->user_id)->lockForUpdate()->first();
            $before  = amount_format($account->balance / 100);
            $remain  = $before + $amount;
            DB::table('accounts')->where('user_id', $this->user_id)->update(['balance' => $account->balance + $amount * 100]);
            $this->user->transactions()->create([
                'before'  => $before,
                'amount'  => $amount,
                'remain'  => $remain,
                'type_id' => $transaction_type,
                'extra'   => [
                    'bill_id' => $bill_id,
                ],
            ]);
        });
    }

    public function decrementBalance($amount, $transaction_type, $bill_id = '')
    {
        DB::transaction(function () use ($amount, $transaction_type, $bill_id) {
            $account = DB::table('accounts')->where('user_id', $this->user_id)->lockForUpdate()->first();
            $before  = amount_format($account->balance / 100);
            $remain  = $before - $amount;
            if ($remain < 0) {
                throw new Exception('余额不足');
            }
            DB::table('accounts')->where('user_id', $this->user_id)->update(['balance' => $account->balance - $amount * 100]);
            $this->user->transactions()->create([
                'before'  => $before,
                'amount'  => -1 * $amount,
                'remain'  => $remain,
                'type_id' => $transaction_type,
                'extra'   => [
                    'bill_id' => $bill_id,
                ],
            ]);
        });
    }

    public static function operateBalance($user_id, $delta, $transaction_type, $bill_id = '')
    {
        if ($delta >= 0) {
            UserModel::find($user_id)->account()->firstOrCreate([])->incrementBalance($delta, $transaction_type, $bill_id);
        } else {
            UserModel::find($user_id)->account()->firstOrCreate([])->decrementBalance(abs($delta), $transaction_type, $bill_id);
        }
    }

    public static function transfer($from_user, $to_user, $amount, $remarks)
    {
        DB::transaction(function () use ($from_user, $to_user, $amount, $remarks) {
            // 检查转出方余额|扣款|记录账变
            $from_account = DB::table('accounts')->where('user_id', $from_user->user_id)->lockForUpdate()->first();
            $from_before  = $from_account->balance / 100;
            $remain       = $from_before - $amount;
            if ($remain < 0) {
                throw new Exception('余额不足');
            }
            DB::table('accounts')->where('user_id', $from_user->user_id)->update(['balance' => $from_account->balance - $amount * 100]);
            $from_user->transactions()->create([
                'before'     => $from_before,
                'amount'     => -1 * $amount,
                'remain'     => $remain,
                'type_id'    => 3,
                'to_user_id' => $to_user->user_id,
                'extra'      => [
                    'remarks' => $remarks,
                ],
            ]);

            $to_account = DB::table('accounts')->where('user_id', $to_user->user_id)->lockForUpdate()->first();
            $to_before  = $to_account->balance / 100;
            $remain     = $to_before + $amount;
            DB::table('accounts')->where('user_id', $to_user->user_id)->update(['balance' => $to_account->balance + $amount * 100]);
            $to_user->transactions()->create([
                'before'       => $to_before,
                'amount'       => $amount,
                'remain'       => $remain,
                'type_id'      => 4,
                'from_user_id' => $from_user->user_id,
                'extra'        => [
                    'remarks' => $remarks,
                ],
            ]);
        });
    }
}
