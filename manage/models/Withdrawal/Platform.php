<?php

namespace Withdrawal;

use BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/11
 * Time: 22:08
 *
 * @property $platform_id
 * @property $identifier
 * @property $name
 * @property $web
 * @property $ip
 * @property $platform_account
 * @property $platform_key
 * @property $request_url
 * @property $query_enabled
 * @property $query_url
 * @property $query_time
 * @property $max_amount
 * @property $min_amount
 * @property $in_use
 * @property $status
 */
class PlatformModel extends BaseModel
{
    public $connection = 'default';
    public $table      = 'withdrawal_platforms';
    public $primaryKey = 'platform_id';
    use SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'identifier',
        'name',
        'web',
        'ip',
        'platform_account',
        'platform_key',
        'request_url',
        'query_enabled',
        'query_url',
        'query_time',
        'max_amount',
        'min_amount',
        'in_use',
        'status',
        'custom_params',
        'bank_ids',
    ];

    protected $casts = [
        'bank_ids' => 'array',
    ];

    /**
     * @return \Lib\Withdrawal\BasePlatform
     */
    public function getPlatformAdapter()
    {
        $class = 'Lib\Withdrawal\Withdrawal' . $this->identifier;
        return $class::getInstance();
    }

    public function getCustomParamsAttribute($value)
    {
        $result = [];
        parse_str($value, $result);

        return $result;
    }
}
