<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/17
 * Time: 01:18
 */

namespace Withdrawal;

use BaseModel;

/**
 * Class NoticeModel
 *
 * @package Deposit
 * @property                $notice_id
 * @property                $deposit_id
 * @property                $status
 * @property                $start_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property DepositModel   $deposit
 */
class QueryModel extends BaseModel
{

    public $connection = 'default';
    public $primaryKey = 'query_id';
    public $table      = 'withdrawal_query_tasks';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'start_at',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public static $relationsData = [
        'withdrawal' => [self::BELONGS_TO, 'Withdrawal\WithdrawalModel'],
        'queryLog'   => [self::HAS_ONE, 'Withdrawal\QueryLogModel', 'foreignKey' => 'query_task_id'],
    ];

    protected $guarded = ['query_id', 'created_at', 'updated_at', 'deleted_at'];

    const STATUS_NEW     = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_FAILED  = 2;
    const STATUS_WORKING = 3;

    const STATUS_ENUM = [
        self::STATUS_NEW     => '请求创建',
        self::STATUS_SUCCESS => '请求成功',
        self::STATUS_FAILED  => '请求失败',
        self::STATUS_WORKING => '请求中',
    ];

    public static $searchFormConf = [
        'created_at'    => ['type' => 'date-range-picker', 'label' => "创建时间"],
        'withdrawal_id' => ['type' => 'hidden', 'label' => '平台订单号'],
    ];

    public static $searchCondictionConf = [
        'created_at'    => ['date-range-picker'],
        'withdrawal_id' => ['='],
    ];
}
