<?php

namespace Withdrawal;

use BaseModel;

class BlackListModel extends BaseModel
{
    public $connection = 'default';
    public $table      = 'withdrawal_black_list';
    public $primaryKey = 'id';

    // use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'mobile',
        'card_no',
        'remarks',
    ];

    public static $searchFormConf = [
        'mobile'  => ['type' => 'input', 'label' => "电话"],
        'card_no' => ['type' => 'input', 'label' => "银行卡号"],
        'name'    => ['type' => 'input', 'label' => "姓名"],
    ];

    public static $searchCondictionConf = [
        'mobile'  => ['='],
        'card_no' => ['='],
        'name'    => ['='],
    ];

}
