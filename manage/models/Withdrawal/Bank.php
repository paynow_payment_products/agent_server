<?php

namespace Withdrawal;

use BaseModel;

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/11
 * Time: 22:06
 */
class BankModel extends BaseModel
{
    public $connection = 'default';
    public $table      = 'banks';
    public $primaryKey = 'bank_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'bank_name',
        'identifier',
    ];
}
