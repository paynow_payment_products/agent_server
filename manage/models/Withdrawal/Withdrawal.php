<?php

namespace Withdrawal;

use BaseModel;
use Exception;
use Carbon\Carbon;

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/11
 * Time: 22:08
 *
 * @property                           $withdrawal_id
 * @property                           $serial_number
 * @property                           $platform_id
 * @property                           $platform_order_no
 * @property                           $app_id
 * @property                           $user_id
 * @property                           $card_id
 * @property                           $amount
 * @property                           $fee
 * @property                           $status
 * @property \Withdrawal\PlatformModel $platform
 * @property \User\UserModel           $user
 * @property \Withdrawal\CardModel     $card
 * @property \Withdrawal\NoticeModel   $notices
 */
class WithdrawalModel extends BaseModel
{
    public $connection = 'default';
    public $table      = 'withdrawals';
    public $primaryKey = 'withdrawal_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'platform_id',
        'platform_order_no',
        'app_id',
        'user_id',
        'card_id',
        'amount',
        'fee',
        'status',
        'admin_id',
        'remarks',
        'display_remarks',
        'admin_operate_time',
    ];

    protected static $relationsData = [
        'user'     => [self::BELONGS_TO, 'User\UserModel'],
        'card'     => [self::BELONGS_TO, 'Withdrawal\CardModel'],
        'platform' => [self::BELONGS_TO, 'Withdrawal\PlatformModel'],
        'notices'  => [self::HAS_MANY, 'Withdrawal\NoticeModel', 'foreignKey' => 'withdrawal_id'],
        'admin'    => [self::BELONGS_TO, 'Manage\AdminModel'],
        'log'      => [self::HAS_MANY, 'Withdrawal\LogModel', 'foreignKey' => 'withdrawal_id'],
        'querys'   => [self::HAS_MANY, 'Withdrawal\QueryModel', 'foreignKey' => 'withdrawal_id'],
    ];

    public static $searchFormConf = [
        'user_id'           => ['type' => 'input', 'label' => "ID"],
        'status'            => ['type' => 'select', 'source' => ['data' => self::STATUS_ENUM], 'label' => "订单状态"],
        'created_at'        => ['type' => 'date-range-picker', 'label' => "创建时间"],
        'platform_id'       => ['type' => 'select', 'source' => ['model' => '\Withdrawal\PlatformModel', "display_name" => 'name'], 'label' => "渠道"],
        'platform_order_no' => ['type' => 'input', 'label' => '渠道订单号'],
    ];

    public static $searchCondictionConf = [
        'user_id'           => ['='],
        'status'            => ['='],
        'created_at'        => ['date-range-picker'],
        'platform_order_no' => ['='],
        'platform_id'       => ['='],
    ];

    const WITHDRAWAL_STATUS_NEW                = 0;
    const WITHDRAWAL_STATUS_SUCCESS            = 1;
    const WITHDRAWAL_STATUS_MINUS_GOLD_FAILED  = 2;
    const WITHDRAWAL_STATUS_MINUS_GOLD_SUCCESS = 3;
    const WITHDRAWAL_STATUS_VERIFY_FAILED      = 4;
    const WITHDRAWAL_STATUS_VERIFY_SUCCESS     = 5;
    const WITHDRAWAL_STATUS_TRANSFER_START     = 6;
    const WITHDRAWAL_STATUS_TRANSFER_FAILED    = 7;
    const WITHDRAWAL_STATUS_TRANSFER_EXCEPTION = 8;

    const STATUS_ENUM = [
        self::WITHDRAWAL_STATUS_NEW                => '未扣币',
        self::WITHDRAWAL_STATUS_MINUS_GOLD_FAILED  => '扣币失败',
        self::WITHDRAWAL_STATUS_MINUS_GOLD_SUCCESS => '扣币成功',
        self::WITHDRAWAL_STATUS_VERIFY_FAILED      => '审核拒绝',
        self::WITHDRAWAL_STATUS_VERIFY_SUCCESS     => '审核通过',
        self::WITHDRAWAL_STATUS_TRANSFER_START     => '正在汇款',
        self::WITHDRAWAL_STATUS_TRANSFER_FAILED    => '汇款失败',
        self::WITHDRAWAL_STATUS_SUCCESS            => '完成',
        self::WITHDRAWAL_STATUS_TRANSFER_EXCEPTION => '第三方汇款接口异常，请检查',
    ];

    //    const WITHDRAWAL_STATUS_NEW = 0; // 待审核
    //    const WITHDRAWAL_STATUS_RECEIVED = 1; //申请成功
    //    const WITHDRAWAL_STATUS_VERIFY_ACCEPTED = 2; //已受理审核
    //    const WITHDRAWAL_STATUS_REFUSE = 3; // 未通过审核（审核拒绝）
    //    const WITHDRAWAL_STATUS_VERIFIED = 4; // 审核通过，待处理
    //    const WITHDRAWAL_STATUS_WITHDRAWAL_ACCEPTED = 5; //受理提现
    //    const WITHDRAWAL_STATUS_DEDUCT_FAIL = 6; // 扣游戏币失败
    //    const WITHDRAWAL_STATUS_SUCCESS = 7; // 成功
    //    const WITHDRAWAL_STATUS_DEDUCT_SUCCESS = 8; //扣款成功
    //    const WITHDRAWAL_STATUS_REMITT_VERIFIED = 9; //汇款审核
    //    const WITHDRAWAL_STATUS_MC_ERROR_RETURN = 10; //10:MC异常返回
    //    const WITHDRAWAL_STATUS_MC_WITHDRAW_FAIL = 11; //10:MC异常返回
    //    const WITHDRAWAL_STATUS_MC_PROCESSING = 12; //9.MC处理中
    //    const WITHDRAWAL_STATUS_FAIL = 13; // 失败
    //    const WITHDRAWAL_STATUS_REFUND = 14; // 已退游戏币（审核拒绝情况下）
    //    const WITHDRAWAL_STATUS_PART = 15; // 部分成功，扣减部分游戏币

    public function beforeCreate()
    {
        $this->status        = static::WITHDRAWAL_STATUS_NEW;
        $this->serial_number = $this->generateSerialNo();
    }

    private function generateSerialNo()
    {
        $no = md5('PN/Cashier/Withdrawal/app_id/' . $this->app_id . '/user_id/' . $this->user_id . '/' . milliseconds());
        return substr($no, 8, -8) . generateVCode(4);
    }

    public function getAmountAttribute($value)
    {
        return number_format($value / AMOUNT_PRECISION, 2, '.', '');
    }

    public function setAmountAttribute($value)
    {
        if (!is_numeric($value)) {
            throw new Exception('金额错误');
        }
        if (empty($this->app_id)) {
            throw new Exception('请先选择app');
        }
        if (empty($this->app)) {
            throw new Exception('错误的app');
        }
        if ((($value * AMOUNT_PRECISION) % ($this->app->amount_base * AMOUNT_PRECISION)) != 0) {
            throw new Exception('invalid amount for this app');
        }
        $amount                     = (int) ($value * AMOUNT_PRECISION);
        $this->attributes['amount'] = $amount;
    }

    public static function needFee($user)
    {
        // 前三次免提现手续费
        $need_withdrawal_fee = true;
        $withdrawal_count    = self::where('user_id', $user->user_id)->whereNotIn('status', [
            WithdrawalModel::WITHDRAWAL_STATUS_MINUS_GOLD_FAILED,
            WithdrawalModel::WITHDRAWAL_STATUS_TRANSFER_FAILED,
            WithdrawalModel::WITHDRAWAL_STATUS_VERIFY_FAILED,
        ])->count();
        if ($withdrawal_count < 2) {
            $need_withdrawal_fee = false;
        }

        return $need_withdrawal_fee;
    }

    public function transfer_failed()
    {
        // 更新状态
        $this->update([
            'status' => self::WITHDRAWAL_STATUS_TRANSFER_FAILED,
        ]);

        // 退款
        $this->user->account->incrementBalance(($this->amount + $this->fee), 7, $this->serial_number);
    }

    public function minus_gold()
    {
        // call api
        $this->user->account->decrementBalance(($this->amount + $this->fee), 2, $this->serial_number);

        $this->update([
            'status' => self::WITHDRAWAL_STATUS_MINUS_GOLD_SUCCESS,
        ]);
    }

    public function sendWithdrawalNotifyToAdmin()
    {
        $data = [
            'new_withdraw_num'   => $this->getWithdrawalNumByStatus(self::WITHDRAWAL_STATUS_MINUS_GOLD_SUCCESS),
            'faild_withdraw_num' => $this->getWithdrawalNumByStatus(self::WITHDRAWAL_STATUS_TRANSFER_FAILED),
        ];
        $data['total_num'] = $data['new_withdraw_num'] + $data['faild_withdraw_num'];
        $url               = 'http://13.94.33.44:9503/?' . http_build_query($data);
        return get($url);
    }

    public function getWithdrawalNumByStatus($status)
    {
        if (is_array($status)) {
            return $this->whereIn('status', $status)->count();
        } else {
            return $this->where('status', $status)->count();
        }

    }

    // 判断当前提现的是否删除了
    public function isTrashedCard()
    {
        return (bool) $this->card()->withTrashed()->first()->trashed();
    }

    public function getUserLoginIp()
    {
        return $this->user->ips()
            ->selectRaw("ip, count(*) as num")
            ->where('user_id', $this->user_id)
            ->groupBy('ip')
            ->orderBy('num', "desc")
            ->limit(20)
            ->get();
    }

    public function isNormalIp()
    {
        return in_array($this->client_ip, $this->getUserLoginIp()->pluck('ip')->toArray());
    }

    public function addQueryTask()
    {
        // add task
        return $this->querys()->create([
            'status'   => QueryModel::STATUS_NEW,
            'start_at' => Carbon::now()->addMinutes(2),
        ]);
    }

}
