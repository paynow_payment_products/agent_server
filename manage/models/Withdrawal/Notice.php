<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/22
 * Time: 16:59
 */

namespace Withdrawal;

use BaseModel;

/**
 * Class NoticeModel
 *
 * @package Withdrawal
 *
 * @property $notice_id
 * @property $withdrawal_id
 * @property $status
 * @property $start_at
 * @property $created_at
 * @property $updated_at
 * @property $deleted_at
 * @property $withdrawal
 */
class NoticeModel extends BaseModel
{
    public $connection = 'default';
    public $table      = 'withdrawal_notices';
    public $primaryKey = 'notice_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'start_at',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public static $relationsData = [
        'withdrawal' => [self::BELONGS_TO, 'Withdrawal\WithdrawalModel'],
    ];

    protected $guarded = ['notice_id', 'created_at', 'updated_at', 'deleted_at'];

    const STATUS_NEW     = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_FAILED  = 2;
    const STATUS_WORKING = 3;

    const STATUS_ENUM = [
        self::STATUS_NEW     => "提醒任务创建",
        self::STATUS_SUCCESS => "任务完成",
        self::STATUS_FAILED  => "任务失败",
        self::STATUS_WORKING => "任务执行中",
    ];

    public function beforeCreate()
    {
        $this->status = self::STATUS_NEW;
    }

    public static $searchFormConf = [
        'status'     => ['type' => 'select', 'source' => ['data' => self::STATUS_ENUM], 'label' => "状态"],
        'created_at' => ['type' => 'date-range-picker', 'label' => "创建时间"],
    ];

    public static $searchCondictionConf = [
        'withdrawal_id' => ['='],
        'status'        => ['='],
        'created_at'    => ['date-range-picker'],
    ];
}
