<?php

namespace Withdrawal;

use BaseModel;

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/11
 * Time: 22:07
 */
class LogModel extends BaseModel
{
    public $connection = 'default';
    public $table      = 'withdrawal_logs';
    public $primaryKey = 'log_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'request_time',
        'response_time',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'request_data'  => 'array',
        'response_data' => 'array',
    ];

    protected $fillable = [
        'withdrawal_id',
        'platform_id',
        'amount',
        'action_type',
        'url',
        'request_data',
        'request_time',
        'response_data',
        'response_time',
    ];

    // 操作类型
    const ACTION_TYPE_REQUEST = 1;
    const ACTION_TYPE_QUERY   = 2;
    const ACTION_TYPE_CONFIRM = 3;
    const ACTION_TYPE_RESULT  = 4;

    const STATUS_ENUM = [
        self::ACTION_TYPE_REQUEST => "发起提现",
        self::ACTION_TYPE_QUERY   => "提现状态查询",
        self::ACTION_TYPE_CONFIRM => "提现结果确认",
        self::ACTION_TYPE_RESULT  => "提现结果推送",
    ];

    public static $relationsData = [
        'withdrawal' => [self::BELONGS_TO, 'Withdrawal\WithdrawalModel'],
    ];

    public static $searchFormConf = [
        'action_type' => ['type' => 'select', 'source' => ['data' => self::STATUS_ENUM], 'label' => "请求类型"],
        'created_at'  => ['type' => 'date-range-picker', 'label' => "创建时间"],
    ];

    public static $searchCondictionConf = [
        'withdrawal_id' => ['='],
        'action_type'   => ['='],
        'created_at'    => ['date-range-picker'],
    ];
}
