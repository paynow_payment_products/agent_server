<?php

namespace Manage;

use BaseModel;

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/11
 * Time: 22:03
 *
 * @property                             $user_id
 * @property                             $app_id
 * @property                             $app_user_id
 * @property                             $name
 * @property                             $fund_pin
 * ****************************
 * @property AppModel                    $app
 * @property \Withdrawal\CardModel       $cards
 * @property \Deposit\DepositModel       $deposits
 * @property \Withdrawal\WithdrawalModel $withdrawals
 * @property TransactionModel            $transactions
 */
class AdminLogModel extends BaseModel
{
    public $connection = 'default';
    public $table      = 'admin_log';
    // public $primaryKey = 'admin_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $guarded = ['admin_id', 'created_at', 'updated_at', 'deleted_at'];

    protected static $relationsData = [
        'admin' => [self::BELONGS_TO, 'Manage\AdminModel'],
    ];

    public static $searchFormConf = [
        'admin_id'   => ['type' => 'select', 'source' => ['model' => '\Manage\AdminModel', "display_name" => 'name'], 'label' => "管理员"],
        'controller' => ['type' => 'input', 'label' => "控制器"],
        'action'     => ['type' => 'input', 'label' => "行为"],
        'ip'         => ['type' => 'input', 'label' => "IP"],
    ];

    public static $searchCondictionConf = [
        'admin_id'   => ['='],
        'ip'         => ['='],
        'controller' => ['='],
        'action'     => ['='],

    ];

    public function beforeCreate()
    {
        $this->request_uri  = $_SERVER['REQUEST_URI'];
        $this->request_data = json_encode($_GET);
        $this->ip           = get_client_ip();
    }
}
