<?php

namespace Manage;

use BaseModel;

class FunctionalitiesModel extends BaseModel
{
    public $connection = 'sc';
    public $table      = 'functionalities';
    public $primaryKey = 'function_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $guarded = ['function_id', 'created_at', 'updated_at', 'deleted_at'];

    protected static $relationsData = [
        'parent' => [self::BELONGS_TO, 'Manage\FunctionalitiesModel', 'foreignKey' => 'parent_func_id'],
    ];

    public static $searchFormConf = [
        'modules'      => ['type' => 'input', 'label' => "模块"],
        'controller'   => ['type' => 'input', 'label' => "控制器"],
        'action'       => ['type' => 'input', 'label' => "行为"],
        'display_name' => ['type' => 'input', 'label' => "名称"],
    ];

    public static $searchCondictionConf = [
        'module'       => ['='],
        'controller'   => ['='],
        'action'       => ['='],
        'display_name' => ['like'],
    ];

    /**
     * 
     * @param  [type] &$aTree    [description]
     * @param  [type] $iParentId [description]
     * @return [type]            [description]
     */
    public static function getTreeArray(&$aTree, $iParentId = null)
    {
        static $deep      = 0;
        $oFunctionalities = self::where('parent_func_id', $iParentId)->get();

        $deep++;
        foreach ($oFunctionalities as $oFunctionality) {
            $aTree[$oFunctionality->function_id]          = $oFunctionality->getAttributes();
            $aTree[$oFunctionality->function_id]['level'] = $deep;
            self::getTreeArray($aTree, $oFunctionality->function_id);
        }
        $deep--;
    }

}
