<?php

namespace Manage;

use BaseModel;

class GroupAdminModel extends BaseModel
{
    public $connection = 'sc';
    public $table      = 'group_admin';
    public $primaryKey = 'group_id,admin_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $guarded = ['created_at', 'updated_at', 'deleted_at'];

    protected static $relationsData = [

    ];
}
