<?php

namespace Manage;

use BaseModel;

class GroupModel extends BaseModel
{
    public $connection = 'sc';
    public $table      = 'groups';
    public $primaryKey = 'group_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $guarded = ['group_id', 'created_at', 'updated_at', 'deleted_at'];

    protected static $relationsData = [
        'admin'          => [self::BELONGS_TO_MANY, 'Manage\AdminModel', 'table' => 'group_admin', 'foreignKey' => 'group_id', 'otherKey'=>'admin_id'],
        'functionalitie' => [self::BELONGS_TO_MANY, 'Manage\FunctionalitiesModel', 'table' => 'group_func', 'foreignKey' => 'group_id', 'otherKey'=>'function_id', ['timestamps' => true]],
    ];

    public static $passwordAttributes = [
        'password',
    ];

    public $autoHashPasswordAttributes = true;

    public function checkPsw($given_psw)
    {
        return self::$hasher->check($given_psw, $this->password);
    }
}
