<?php

namespace Manage;

use BaseModel;

class GroupFuncModel extends BaseModel
{
    public $connection = 'sc';
    public $table      = 'group_func';
    public $primaryKey = 'group_id,function_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $guarded = ['created_at', 'updated_at', 'deleted_at'];

    protected static $relationsData = [

    ];
}
