<?php

namespace Manage;

use BaseModel;

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/11
 * Time: 22:03
 *
 * @property                             $user_id
 * @property                             $app_id
 * @property                             $app_user_id
 * @property                             $name
 * @property                             $fund_pin
 * ****************************
 * @property AppModel                    $app
 * @property \Withdrawal\CardModel       $cards
 * @property \Deposit\DepositModel       $deposits
 * @property \Withdrawal\WithdrawalModel $withdrawals
 * @property TransactionModel            $transactions
 */
class AdminModel extends BaseModel
{
    public $connection = 'sc';
    public $table      = 'admins';
    public $primaryKey = 'admin_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $guarded = ['admin_id', 'created_at', 'updated_at', 'deleted_at'];

    protected static $relationsData = [
        'logs'  => [self::HAS_MANY, 'Manage\AdminLogModel', 'foreignKey' => 'admin_id'],
        'group' => [self::BELONGS_TO_MANY, 'Manage\GroupModel', 'table' => 'group_admin', 'foreignKey' => 'admin_id', 'otherKey'=>'group_id'],
    ];

    public static $passwordAttributes = [
        'password',
    ];

    public $autoHashPasswordAttributes = true;

    public function checkPsw($given_psw)
    {
        return self::$hasher->check($given_psw, $this->password);
    }

    public static $searchCondictionConf = [
        'group_id' => ['='],
    ];
}
