<?php

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 7/4/15
 * Time: 23:16
 * dir -> databases
 * file -> tables
 */
class BaseModel extends \Core\Model
{
    /**
     * 搜索表单配置
     * [
     *     'colums' => [
     *         'type' => input or select
     *         'label' => 展示名
     *         'source' => ['model' => "\path\model", 'display_name', 'data'],
     *     ],
     * ]
     * @var array
     */
    public static $searchFormConf       = [];
    public static $searchCondictionConf = [];

    public static $order = ['created_at' => 'desc'];

    /**
     * 批量设置查询条件，返回Query实例
     * @param  array  $aConditions [$sColumnName => [$symbol, $data]]
     * @param  object $oQuery      LaravelArdent\Ardent\Builder
     * @return object LaravelArdent\Ardent\Builder
     */
    public static function doWhere($aConditions = [], $oQuery = null)
    {
        is_array($aConditions) or $aConditions = [];
        foreach ($aConditions as $sColumn => $aCondition) {
            $sObject   = isset($oQuery) ? '$oQuery->' : 'self::';
            $statement = '';
            switch ($aCondition[0]) {
                case '=':
                    if (is_null($aCondition[1])) {
                        $statement = '$oQuery = ' . $sObject . 'whereNull($sColumn);';
                    } else {
                        $statement = '$oQuery = ' . $sObject . 'where($sColumn , \'=\' , $aCondition[ 1 ]);';
                    }
                    break;
                case 'in':
                    $array     = is_array($aCondition[1]) ? $aCondition[1] : explode(',', $aCondition[1]);
                    $statement = '$oQuery = ' . $sObject . 'whereIn($sColumn , $array);';
                    break;
                case '>=':
                case '<=':
                case '<':
                case '>':
                case 'like':
                    if (is_null($aCondition[1])) {
                        $statement = '$oQuery = ' . $sObject . 'whereNotNull($sColumn);';
                    } else {
                        $val       = '%' . $aCondition[1] . '%';
                        $statement = '$oQuery = ' . $sObject . 'where($sColumn,$aCondition[ 0 ],$val);';
                    }
                    break;
                case '<>':
                case '!=':
                    if (is_null($aCondition[1])) {
                        $statement = '$oQuery = ' . $sObject . 'whereNotNull($sColumn);';
                    } else {
                        $statement = '$oQuery = ' . $sObject . 'where($sColumn,\'<>\',$aCondition[ 1 ]);';
                    }
                    break;
                case 'between':
                    $statement = '$oQuery = ' . $sObject . 'whereBetween($sColumn,$aCondition[ 1 ]);';
                    break;
            }
            eval($statement);
        }
        if (!isset($oQuery)) {
            $oQuery = self::where([]);
        }
        if (isset(static::$order) && is_array(static::$order)) {
            foreach (static::$order as $colums => $sort) {
                $oQuery->orderBy($colums, $sort);
            }
        }
        return $oQuery;
    }

    /**
     * 生成搜索表单配置数组
     * 给搜索表单那你模块使用
     * @param  array  $requestData Request::getQuery()
     * @return array
     */
    public static function mkSearchConfForForm($requestData = [])
    {
        $searchItemsConf = static::$searchFormConf;

        foreach ($searchItemsConf as $searchColum => $conf) {
            // 默认值
            if (array_key_exists($searchColum, $requestData)) {
                $searchItemsConf[$searchColum]['val'] = $requestData[$searchColum];
            }

            // 下拉框填充数据
            if ($conf['type'] == 'select' && !empty($conf['source']['model'])) {
                $tmp                                             = $conf['source']['model']::pluck($conf['source']['display_name'], $searchColum)->toArray();
                $searchItemsConf[$searchColum]['source']['data'] = $tmp;
            }
        }
        // pr($searchItemsConf);
        return $searchItemsConf;
    }

    /**
     * 生成搜索条件数组
     * 为筛选列表数据使用
     * @param  array  $requestData Request::getQuery()
     * @return array
     */
    public static function mkSearchCondiction($requestData = [])
    {

        $condiction      = [];
        $searchItemsConf = static::$searchCondictionConf;

        if (!empty($requestData) && is_array($requestData)) {
            foreach ($requestData as $searchColum => $val) {
                if (array_key_exists($searchColum, $searchItemsConf) && $val !== '') {
                    if ( $searchItemsConf[$searchColum][0] == 'date-range-picker' && !is_array($val) ) {
                        $val = explode(' - ', $val);
                        $val[0] = $val[0] . " 00:00:00";
                        $val[1] = $val[1] . " 23:59:59";
                        $condiction[$searchColum] = ['between', $val];
                    }else{
                        $condiction[$searchColum] = [$searchItemsConf[$searchColum][0], $val];
                    }
                }
            }
        }

        if ( !empty($requestData['club_name']) ) {
            $condiction['club_id'] = ['in', \User\ClubModel::doWhere(['club_name'=> ['like', $requestData['club_name']]])->pluck('club_id')->toArray()];
        }

        if ( !empty($requestData['desk_name']) ) {
            $condiction['desk_id'] = ['in', \User\DeskModel::doWhere(['desk_name'=> ['like', $requestData['desk_name']]])->pluck('desk_id')->toArray()];
        }

        // pr($condiction);
        return $condiction;
    }
}
