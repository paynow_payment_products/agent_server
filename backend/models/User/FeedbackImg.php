<?php

namespace User;

use BaseModel;

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/8/7
 * Time: 15:32
 */

class FeedbackImgModel extends BaseModel
{

    public $connection = 'sc';
    public $table      = 'feedback_img';
    public $primaryKey = 'image_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $guarded = ['image_id', 'created_at', 'updated_at', 'deleted_at'];

    protected static $relationsData = [
        'feedback' => [self::BELONGS_TO, 'User\FeedbackModel'],
    ];

    public function beforeCreate()
    {

    }
}
