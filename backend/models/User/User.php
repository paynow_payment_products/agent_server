<?php

namespace User;

use BaseModel;
use Illuminate\Database\Capsule\Manager as DB;
use Transaction\TransactionModel;
use Exception;

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/11
 * Time: 22:03
 *
 * @property                             $user_id
 * @property                             $mobile
 * @property                             $password
 * @property                             $nickname
 * @property                             $name
 * @property                             $fund_pin
 * ****************************
 * @property AppModel                    $app
 * @property \Withdrawal\CardModel       $cards
 * @property \Deposit\DepositModel       $deposits
 * @property \Withdrawal\WithdrawalModel $withdrawals
 * @property TransactionModel            $transactions
 * @property \User\AccountModel          $account
 */
class UserModel extends BaseModel
{
    public $connection = 'sc';
    public $table = 'users';
    public $primaryKey = 'user_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $guarded = ['user_id', 'created_at', 'updated_at', 'deleted_at'];

    protected static $relationsData = [
        'cards'        => [self::HAS_MANY, 'Withdrawal\CardModel', 'foreignKey' => 'user_id'],
        'deposits'     => [self::HAS_MANY, 'Deposit\DepositModel', 'foreignKey' => 'user_id'],
        'withdrawals'  => [self::HAS_MANY, 'Withdrawal\WithdrawalModel', 'foreignKey' => 'user_id'],
        'transactions' => [self::HAS_MANY, 'Transaction\TransactionModel', 'foreignKey' => 'user_id'],
        'feedback'     => [self::HAS_MANY, 'User\FeedbackModel', 'foreignKey' => 'user_id'],
        'ips'          => [self::HAS_MANY, 'User\IpModel', 'foreignKey' => 'user_id'],
        'account'      => [self::HAS_ONE, 'User\AccountModel', 'foreignKey' => 'user_id'],
    ];

    public static $passwordAttributes = [
        'password',
        'fund_pin',
    ];

    public static $searchFormConf = [
        'nickname' => ['type' => 'input', 'label' => "昵称"],
        'mobile'   => ['type' => 'input', 'label' => "手机号"],
        'name'     => ['type' => 'input', 'label' => "姓名"],
    ];

    public static $searchCondictionConf = [
        'user_id' => ['='],
        'mobile'  => ['='],
        'name'    => ['like'],
        // 'client_ip'   => ['='],
    ];

    public $autoHashPasswordAttributes = true;

    public function checkFundPin($given_pin)
    {
        return self::$hasher->check($given_pin, $this->fund_pin);
    }

    public function checkPassword($given_password)
    {
        return self::$hasher->check($given_password, $this->password);
    }

}
