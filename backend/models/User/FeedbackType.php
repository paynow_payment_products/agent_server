<?php

namespace User;

use BaseModel;

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/8/7
 * Time: 15:32
 */

class FeedbackTypeModel extends BaseModel
{

    public $connection = 'sc';
    public $table      = 'feedback_type';
    public $primaryKey = 'type_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $guarded = ['type_id', 'created_at', 'updated_at', 'deleted_at'];

    protected static $relationsData = [
        'feedback'  => [self::HAS_MANY, 'User\FeedbackModel', 'foreignKey' => 'feedback_id'],
    ];

    public function beforeCreate()
    {

    }
}
