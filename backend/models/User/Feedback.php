<?php

namespace User;

use BaseModel;

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/8/7
 * Time: 15:32
 */

class FeedbackModel extends BaseModel
{

    public $connection = 'sc';
    public $table      = 'feedback';
    public $primaryKey = 'feedback_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $guarded = ['feedback_id', 'created_at', 'updated_at', 'deleted_at'];

    protected static $relationsData = [
        'user' => [self::BELONGS_TO, 'User\UserModel'],
        'img'  => [self::HAS_MANY, 'User\FeedbackImgModel', 'foreignKey' => 'feedback_id'],
        'type' => [self::BELONGS_TO, 'User\FeedbackTypeModel'],
    ];

    protected $casts = [
        'discription_data' => 'array',
    ];

    public function beforeCreate()
    {

    }

    public static $searchFormConf = [
        'user_id'     => ['type' => 'hidden', 'label' => "ID"],
        'app_user_id'     => ['type' => 'input', 'label' => "APP应用ID"],
        'type_id'   => ['type' => 'select', 'source' => ['model' => '\User\FeedbackTypeModel', 'display_name' => 'name'], 'label' => "反馈类型"],
        'created_at'  => ['type' => 'input', 'label' => "时间"],
    ];

    public static $searchCondictionConf = [
        'user_id'     => ['='],
        'created_at'  => ['like'],
        'type_id'  => ['='],
    ];

    public $translateArr = [
        'order_no' => '充值订单号',
        'platform_id' => '充值渠道ID',
        'amount' => '金额',
        'platform_order_no' => '渠道订单号',
        'description' => '详细文字描述',
        'serial_number' => '提现订单号',
        'bank_id' => '银行id',
        'card_no' => '提现卡号',
        'type' => '问题类型',
        'time' => '时间',
    ];


}
