<?php

namespace Withdrawal;

use BaseModel;

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/11
 * Time: 22:07
 *
 * @property                 $card_id
 * @property                 $user_id
 * @property                 $bank_id
 * @property                 $card_no
 * @property                 $name
 * @property                 $status
 * @property                 $created_at
 * @property                 $updated_at
 * @property                 $deleted_at
 * @property BankModel       $bank
 * @property \User\UserModel $user
 */
class CardModel extends BaseModel
{
    public $connection = 'sc';
    public $table      = 'user_cards';
    public $primaryKey = 'card_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'user_id',
        'bank_id',
        'card_no',
        'name',
        'status',
        'province',
        'city',
        'branc_name',
        'name',
    ];

    protected static $relationsData = [
        'bank' => [self::BELONGS_TO, 'Withdrawal\BankModel'],
        'user' => [self::BELONGS_TO, 'User\UserModel'],
    ];

    public static $rules = [
        'card_no' => 'required|numeric',
        'status'  => 'required|in:0,1',
    ];

    const STATUS_NEW   = 0;
    const STATUS_VALID = 1;
    const STATUS_ENUM  = [
        self::STATUS_NEW   => '未审核',
        self::STATUS_VALID => '已审核',
    ];

    public static $searchFormConf = [
        'user_id'     => ['type' => 'hidden', 'label' => "ID"],
        'status'      => ['type' => 'select', 'source' => ['data' => self::STATUS_ENUM], 'label' => "订单状态"],
        'card_no'     => ['type' => 'input', 'label' => "卡号"],
    ];

    public static $searchCondictionConf = [
        'user_id'    => ['='],
        'status'     => ['='],
        'card_no'    => ['='],
    ];
}
