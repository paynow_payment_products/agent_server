<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/17
 * Time: 01:18
 */

namespace Deposit;

use BaseModel;

/**
 * Class NoticeModel
 *
 * @package Deposit
 * @property                $notice_id
 * @property                $deposit_id
 * @property                $status
 * @property                $start_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property DepositModel   $deposit
 */
class NoticeModel extends BaseModel
{

    public $connection = 'sc';
    public $table = 'deposit_notices';
    public $primaryKey = 'notice_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'start_at',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public static $relationsData = [
        'deposit' => [self::HAS_ONE, 'Deposit\DepositModel',],
    ];

    protected $guarded = ['notice_id', 'created_at', 'updated_at', 'deleted_at'];

    const STATUS_NEW = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_FAILED = 2;
    const STATUS_WORKING = 3;
}