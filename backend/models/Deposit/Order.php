<?php

namespace Deposit;

use BaseModel;

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/7
 * Time: 15:44
 *
 * @property              $order_id
 * @property              $deposit_id
 * @property              $third_order_no
 * @property              $load_url
 * @property              $load_request
 * @property              $load_response
 **************************************
 * @property DepositModel $deposit
 */
class OrderModel extends BaseModel
{
    public $connection = 'sc';
    public $table      = 'deposit_orders';
    public $primaryKey = 'order_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'load_request'  => 'array',
        'load_response' => 'array',
    ];

    public static $relationsData = array(
        'deposit' => [self::BELONGS_TO, 'Deposit\DepositModel'],
    );

    protected $fillable = [
        'deposit_id',
        'third_order_no',
        'third_order_status',
        'load_url',
        'load_request',
        'load_response',
    ];

    public static $searchFormConf = [
        'created_at' => ['type' => 'date-range-picker', 'label' => "创建时间"],
        'order_id'   => ['type' => 'hidden', 'label' => '平台订单号'],
    ];

    public static $searchCondictionConf = [
        'created_at' => ['date-range-picker'],
        'order_id'   => ['='],
    ];

}
