<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/10/4
 * Time: 16:22
 */

namespace Deposit;

use BaseModel;

class UserPlatformModel extends BaseModel
{

    public $connection = 'sc';
    public $primaryKey = 'user_platform_id';
    public $table      = 'user_deposit_platform';
    // use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public $fillable = [
        'terminal_type',
        'is_show',
        'user_id',
        'platform_id',
    ];

    public static $relationsData = [
        'platform' => [self::BELONGS_TO, 'Deposit\PlatformModel'],
        'user'     => [self::BELONGS_TO, 'User\UserModel'],
    ];

    const USER_PLATFORM_TERMINAL_TYPE_MOBILE  = 1;
    const USER_PLATFORM_TERMINAL_TYPE_PC      = 2;
    const USER_PLATFORM_TERMINAL_TYPE_IOS     = 3;
    const USER_PLATFORM_TERMINAL_TYPE_ANDROID = 4;

    public $terminal_type_arr = [
        self::USER_PLATFORM_TERMINAL_TYPE_PC      => "PC",
        self::USER_PLATFORM_TERMINAL_TYPE_MOBILE  => "MOBLIE",
        self::USER_PLATFORM_TERMINAL_TYPE_IOS     => "IOS",
        self::USER_PLATFORM_TERMINAL_TYPE_ANDROID => "ANDROID",
    ];

    public static $searchFormConf = [
        'user_id'     => ['type' => 'hidden', 'label' => "ID"],
        'app_user_id' => ['type' => 'input', 'label' => "APP用户ID"],
    ];

    public static $searchCondictionConf = [
        'user_id' => ['='],
    ];

}
