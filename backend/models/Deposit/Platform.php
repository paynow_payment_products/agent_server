<?php

namespace Deposit;

use BaseModel;

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/7
 * Time: 15:56
 *
 * @property               $platform_id
 * @property               $identifier
 * @property               $icon
 * @property               $name
 * @property               $status
 * @property               $account
 * @property               $key
 * @property               $need_bank
 * @property               $custom_params
 * @property               $load_url
 * @property               $relay_load_url
 * @property               $load_straight
 * @property               $notify_url
 * @property               $query_url
 * @property               $relay_query_url
 * @property               $max_load
 * @property               $min_load
 * @property               $pc_show
 * @property               $mobile_show
 * @property               $pc_default
 * @property               $mobile_default
 * @property               $created_at
 * @property               $updated_at
 * @property               $deleted_at
 *******************************
 * @property \BasePlatform $platform
 */
class PlatformModel extends BaseModel
{
    public $connection = 'sc';
    public $table      = 'deposit_platforms';
    public $primaryKey = 'platform_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public $fillable = [
        'icon',
        'identifier',
        'name',
        'display_name',
        'status',
        'account',
        'key',
        'need_bank',
        'custom_params',
        'load_url',
        'relay_load_url',
        'load_straight',
        'notify_url',
        'query_url',
        'relay_query_url',
        'max_load',
        'min_load',
        'pc_show',
        'mobile_show',
        'pc_default',
        'mobile_default',
        'ios_show',
        'android_show',
        'ios_default',
        'android_default',
    ];

    public static $rules = [
        'identifier'     => 'required',
        'status'         => 'required|in:0,1,2',
        'pc_show'        => 'required|in:0,1',
        'mobile_show'    => 'required|in:0,1',
        'pc_default'     => 'required|in:0,1',
        'mobile_default' => 'required|in:0,1',
    ];

    public $statusName = [
        self::STATUS_CLOSED    => '关闭',
        self::STATUS_TESTING   => '测试',
        self::STATUS_AVAILABLE => '可用',
    ];

    const STATUS_CLOSED    = 0;
    const STATUS_TESTING   = 1;
    const STATUS_AVAILABLE = 2;

    /**
     * @return \Lib\Payment\BasePlatform
     */
    public function getPlatformAdapter()
    {
        $class = 'Lib\Payment\Payment' . $this->identifier;

        return $class::getInstance();
    }

    public function getMaxLoadAttribute($value)
    {
        return $value / AMOUNT_PRECISION;
    }

    public function getMinLoadAttribute($value)
    {
        return $value / AMOUNT_PRECISION;
    }

    public function setMaxLoadAttribute($value)
    {
        $this->attributes['max_load'] = $value * AMOUNT_PRECISION;
    }

    public function setMinLoadAttribute($value)
    {
        $this->attributes['min_load'] = $value * AMOUNT_PRECISION;
    }

    public function getCustomParamsAttribute($value)
    {
        $result = [];
        parse_str($value, $result);

        return $result;
    }

    public function checkAmount($amount)
    {
        return !($amount < $this->min_load || $amount > $this->max_load);
    }
}
