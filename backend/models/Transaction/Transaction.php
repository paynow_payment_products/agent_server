<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/16
 * Time: 18:37
 */

namespace Transaction;

use BaseModel;

/**
 * 帐变
 */
class TransactionModel extends BaseModel
{

    public $connection = 'sc';
    public $table      = 'transactions';
    public $primaryKey = 'transaction_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $guarded = ['transaction_id', 'created_at', 'updated_at', 'deleted_at'];

    protected static $relationsData = [
        'user'    => [self::BELONGS_TO, 'User\UserModel'],
        'type'    => [self::BELONGS_TO, 'Transaction\TypeModel'],
        'to_user' => [self::HAS_ONE, 'User\UserModel', 'foreignKey' => 'user_id', 'localKey' => 'to_user_id'],
        'from_user' => [self::HAS_ONE, 'User\UserModel', 'foreignKey' => 'user_id', 'localKey' => 'from_user_id'],
    ];

    public static $order = [
        'created_at'     => 'desc',
        'transaction_id' => 'desc',
    ];

    protected $casts = [
        'extra' => 'array',
    ];

    public static $searchFormConf = [
        'user_id'     => ['type' => 'hidden', 'label' => "ID"],
        'app_user_id' => ['type' => 'input', 'label' => "APP应用ID"],
        'optype_id'   => ['type' => 'select', 'source' => ['model' => '\Transaction\OptypeModel', 'display_name' => 'optype_name'], 'label' => "订单状态"],
        'created_at'  => ['type' => 'date-range-picker', 'label' => "创建时间"],
        'club_name'   => ['type' => 'input', 'label' => "俱乐部"],
        'desk_name'   => ['type' => 'input', 'label' => "桌号"],
        // 'platform_id' => ['type' => 'select', 'source' => ['model' => '\Withdrawal\PlatformModel', "display_name" => 'name'], 'label' => "渠道"],
    ];

    public static $searchCondictionConf = [
        'user_id'    => ['='],
        'created_at' => ['date-range-picker'],
        'optype_id'  => ['='],
        // 'app' => ['='],
    ];

    public function getBeforeAttribute($value)
    {
        return amount_format($value / 100);
    }

    public function setBeforeAttribute($value)
    {
        $this->attributes['before'] = $value * 100;
    }

    public function getAmountAttribute($value)
    {
        return amount_format($value / 100);
    }

    public function setAmountAttribute($value)
    {
        $this->attributes['amount'] = $value * 100;
    }

    public function getRemainAttribute($value)
    {
        return amount_format($value / 100);
    }

    public function setRemainAttribute($value)
    {
        $this->attributes['remain'] = $value * 100;
    }
}
