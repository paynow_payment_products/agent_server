<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/16
 * Time: 18:37
 */

namespace Transaction;

use BaseModel;

/**
 * 管理员转账
 */
class GoldoperateModel extends BaseModel
{

    public $connection = 'sc';
    public $table      = 'gold_operate';
    public $primaryKey = 'gold_operate_id';
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const STATUS_ENUM = [
        9  => '上桌奖励',
        10 => '牌型奖励',
        11 => '称号奖励',
        19 => '邀请红包',
        18 => '新人奖励',
        6  => '活动奖励',
    ];

    protected $guarded = ['gold_operate_id', 'created_at', 'updated_at', 'deleted_at'];

    protected static $relationsData = [
        'admin'  => [self::BELONGS_TO, 'Manage\AdminModel'],
        'optype' => [self::BELONGS_TO, 'Transaction\OptypeModel'],
        'user'   => [self::BELONGS_TO, 'User\UserModel'],
    ];

    public static $order = [
        'created_at' => 'desc',
    ];

    public static $searchFormConf = [
        'user_id'     => ['type' => 'hidden', 'label' => "ID"],
        'app_user_id' => ['type' => 'input', 'label' => "APP应用ID"],
        'optype_id'   => ['type' => 'select', 'source' => ['data' => self::STATUS_ENUM], 'label' => "操作类型"],
        'created_at'  => ['type' => 'date-range-picker', 'label' => "时间"],
    ];

    public static $searchCondictionConf = [
        'user_id'    => ['='],
        'created_at' => ['date-range-picker'],
        'optype_id'  => ['='],
    ];

    public function beforeCreate()
    {
        !empty($this->bill_no) or $this->bill_no = $this->generateDepositOrderNo();
        $this->client_ip                         = get_client_ip();
    }

    private function generateDepositOrderNo()
    {
        $order_no = 'OP';
        $order_no .= str_pad($this->admin_id, 2, '0', STR_PAD_LEFT);
        $order_no .= str_pad($this->user_id, 6, '0', STR_PAD_LEFT);
        $order_no .= str_pad($this->optype_id, 2, '0', STR_PAD_LEFT);
        $order_no .= date('ymdHis');
        $order_no .= str_pad(milliseconds() % 10000, 4, '0', STR_PAD_LEFT);
        return $order_no;
    }

    public function getAmountAttribute($value)
    {
        return number_format($value / AMOUNT_PRECISION, 2, '.', '');
    }

    public function setAmountAttribute($value)
    {
        if (!is_numeric($value)) {
            throw new Exception('金额错误');
        }
        $amount                     = (int) ($value * AMOUNT_PRECISION);
        $this->attributes['amount'] = $amount;
    }

}
