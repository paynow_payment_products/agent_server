#!/usr/bin/env bash

git pull
composer dump-autoload
chmod -R 777 .
chown -R www.www .
