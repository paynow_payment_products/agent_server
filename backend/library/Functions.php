<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2018/6/2
 * Time: 19:18
 */

use Blocktrail\CryptoJSAES\CryptoJSAES;

function aes_decrypt($encrypted) {
    try {
        return CryptoJSAES::decrypt($encrypted, AES_PASSPHRASE);
    } catch (Exception $e) {
        return false;
    }
}
