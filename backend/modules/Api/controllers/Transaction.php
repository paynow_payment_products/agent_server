<?php

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/16
 * Time: 16:05
 */
class TransactionController extends BaseApiController
{
    public $disableView = true;

    public $action_names = [
        'report',
        'list',
        'desks',
        'deskin',
        'deskout',
        'optype',
        'transfer',
    ];
}