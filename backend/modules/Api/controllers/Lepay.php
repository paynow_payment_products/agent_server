<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/8/2
 * Time: 18:09
 */

use Deposit\PlatformModel;

class LepayController extends BaseController
{

    public $disableView = true;
    public $disableActions = true;

    public function indexAction() {
        $platformId = Request::get('platform_id');
        $platform   = PlatformModel::find($platformId);
        if (empty($platform)) {
            throw new Exception('invalid platform');
        }

        if ($platform->identifier !== 'LEPAY') {
            throw new Exception('invalid platform');
        }

        $oPlatform = $platform->getPlatformAdapter();
        $params    = getAllParams();

        $content = $oPlatform->compileNotifyContent($platform, $params);
        $params += $content;

        try {
            $oPlatform->actionDepositNotify($params, $platform);
        } catch (Exception $e) {
            $oPlatform->returnNotifyError($params, $e->getMessage());
        }
    }
}