<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/12
 * Time: 23:24
 */

use Withdrawal\WithdrawalModel as Withdrawal;
use Withdrawal\CardModel;

class CreateAction extends BaseAction
{
    public $disableView = true;

    public function run()
    {
        $user = $this->checkLogin();

        $app_id = $this->session()->app_id;

        $card_id = Request::get('card_id');
        $amount  = Request::get('amount');

        if ($amount < 100 || $amount > 45000) {
            throw new Exception('无效的提现金额');
        }

        $card = CardModel::find($card_id);
        if (empty($card)) {
            throw new Exception('无效的银行卡');
        }

        if ($card->user_id != $user->user_id) {
            throw new Exception('无效的银行卡');
        }

        self::checkCardBranch($card);

        // 校验pin码
        $this->checkFundPin();

        $withdrawal_fee = 0;
        if (Withdrawal::needFee($user)) {
            $withdrawal_fee = round($amount * 0.02);
        }

        $withdrawal = new Withdrawal([
            'app_id'  => $app_id,
            'card_id' => $card_id,
            'amount'  => $amount,
            'fee'     => $withdrawal_fee,
        ]);
        $user->withdrawals()->save($withdrawal);

        // 扣币
        $withdrawal->minus_gold();

        // 给客服发送通知
        // $withdrawal->sendWithdrawalNotifyToAdmin();

        echo json_encode([
            'success' => 1,
            'data'    => [
                'withdrawal_id' => $withdrawal->withdrawal_id,
            ],
        ]);
    }

    private static function checkCardBranch($card)
    {
        if (empty($card->province) || empty($card->city) || empty($card->branc_name)) {
            $province   = Request::get('province');
            $city       = Request::get('city');
            $branc_name = Request::get('branc_name');

            if (empty($province) || empty($city) || empty($branc_name)) {
                throw new Exception('请完善银行卡信息');
            }

            $card->update([
                'province'   => $province,
                'city'       => $city,
                'branc_name' => $branc_name,
            ]);
        }
    }
}