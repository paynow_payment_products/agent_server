<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/6
 * Time: 22:37
 */

use Withdrawal\WithdrawalModel;

class ListAction extends BaseAction
{
    public function run()
    {
        $user = $this->checkLogin();
        $list = WithdrawalModel::where([
            ['user_id', $user->user_id],
            ['status', '<>', WithdrawalModel::WITHDRAWAL_STATUS_MINUS_GOLD_FAILED],
        ])
            ->orderBy('created_at', 'desc')
            ->paginate($this->pageSize);

        exit(json_encode([
            'success' => 1,
            'data'    => [
                'list'     => $list->items(),
                'lastPage' => $list->lastPage(),
                'status'   => [
                    WithdrawalModel::WITHDRAWAL_STATUS_NEW                => '处理中',
                    WithdrawalModel::WITHDRAWAL_STATUS_SUCCESS            => '提现成功',
                    WithdrawalModel::WITHDRAWAL_STATUS_MINUS_GOLD_FAILED  => '提现失败',
                    WithdrawalModel::WITHDRAWAL_STATUS_MINUS_GOLD_SUCCESS => '处理中',
                    WithdrawalModel::WITHDRAWAL_STATUS_VERIFY_FAILED      => '提现失败',
                    WithdrawalModel::WITHDRAWAL_STATUS_VERIFY_SUCCESS     => '处理中',
                    WithdrawalModel::WITHDRAWAL_STATUS_TRANSFER_START     => '处理中',
                    WithdrawalModel::WITHDRAWAL_STATUS_TRANSFER_FAILED    => '提现失败',
                ],
            ],
        ]));
    }
}
