<?php

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/6
 * Time: 20:13
 */
class InfoAction extends BaseAction
{
    public function run()
    {
        $user = $this->checkLogin();
        $withdrawal = $user->withdrawals->where('withdrawal_id', Request::get('withdrawal_id'))->first();

        if (empty($withdrawal)) {
            throw new Exception('您的这笔提现订单被可乐吃了！');
        }

        exit(json_encode([
            'success' => 1,
            'data' => [
                'amount' => $withdrawal->amount,
                'order_no' => $withdrawal->serial_number,
                'redict_time' => $withdrawal->created_at->addMinutes(120)->toDateTimeString(), // 60*2
                'bank_name' => $withdrawal->card->bank->bank_name,
                'card_no' => safe_card_no($withdrawal->card->card_no),
                'fee' => $withdrawal->fee
            ]
        ]));
    }
}