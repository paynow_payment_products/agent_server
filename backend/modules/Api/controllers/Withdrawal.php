<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/4
 * Time: 00:35
 */

class WithdrawalController extends BaseApiController
{
    public $action_names = [
        'create',
        'info',
        'list',
        'notify',
    ];
}