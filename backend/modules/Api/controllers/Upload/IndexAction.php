<?php

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/3
 * Time: 01:12
 */

use Lib\Upload;

class IndexAction extends BaseAction
{
    protected $disableView = true;

    public function run() {
        return false;
        $user = $this->checkLogin();

        if ( isset($_FILES['img']) ) {
            $upload = new Upload();
            echo json_encode($upload->saveImgs($_FILES['img']));
        }
    }
}