<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2018/6/5
 * Time: 14:17
 */

use Gregwar\Captcha\CaptchaBuilder;
use Gregwar\Captcha\PhraseBuilder;

class CaptchaAction extends BaseAction
{
    public function run () {
        header('Content-type: image/jpeg');
        $phraseBuilder = new PhraseBuilder(5, '3457acdefhjkmnprstuvwxyABCDEFGHJKLMNPQRSTUVWXY');
        $code = $phraseBuilder->build(4);
        $builder = new CaptchaBuilder($code, $phraseBuilder);
        $builder->setDistortion(true);
        $builder->buildAgainstOCR(150, 60);
        $builder->output(200);

        $this->session->captcha = [
            'phrase' => $builder->getPhrase(),
            'created' => time()
        ];
    }
}