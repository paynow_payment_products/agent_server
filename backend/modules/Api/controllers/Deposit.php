<?php

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/12
 * Time: 14:00
 */
class DepositController extends BaseApiController
{
    public $disableView = true;
    public $action_names = [
        'notify',
        'platforms',
        'banks',
        'create',
        'list',
        'lepay',
        'userplatform',
        'go'
    ];
}