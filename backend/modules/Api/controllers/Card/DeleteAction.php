<?php

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/3
 * Time: 18:08
 */
class DeleteAction extends BaseAction
{
    public function run()
    {
        $user = $this->checkLogin();

        // 校验pin码
        $this->checkFundPin();

        $card_id = Request::get('card_id');

        $card = \Withdrawal\CardModel::find($card_id);

        if (empty($card) || $card->user_id != $user->user_id) {
            throw new Exception('这张卡被外星人吃了！');
        }

        if ($user->cards->count() <= 1) {
            throw new Exception('对不起，您就剩一张银行卡了，不能删除');
        }

        $card->delete();

        exit(json_encode([
            'success' => 1
        ]));
    }
}