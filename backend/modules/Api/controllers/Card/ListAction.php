<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/3
 * Time: 17:39
 */

class ListAction extends BaseAction
{
    public function run()
    {
        $user = $this->checkLogin();

        $cards = Request::get('type') == "all" ? $user->cards()->withTrashed()->get() : $user->cards;

        $cards_array = [];
        foreach ($cards as $card) {
            $cards_array[] = [
                'card_id'        => $card->card_id,
                'card_no_source' => $card->card_no,
                'card_no'        => safe_card_no($card->card_no),
                'name'           => $card->name,
                'bank'           => $card->bank,
                'province'       => (string) $card->province,
                'city'           => (string) $card->city,
                'branc_name'     => (string) $card->branc_name,
            ];
        }
        echo json_encode([
            'success' => 1,
            'data' => [
                'cards' => $cards_array
            ]
        ]);
    }
}