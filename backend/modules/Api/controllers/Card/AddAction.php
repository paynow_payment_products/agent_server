<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/18
 * Time: 00:52
 */

use Withdrawal\CardModel;
use Withdrawal\BankModel;
use User\UserModel;
use SmsCenter\Tools as Smstools;

class AddAction extends BaseAction
{
    public function run()
    {
        $user       = $this->checkLogin();
        $bank_id    = Request::get('bank');
        $card_no    = Request::get('cardNum');
        $name       = Request::get('name');
        $city       = Request::get('city');
        $province   = Request::get('province');
        $branc_name = Request::get('branc_name');

        // 校验pin码
        $this->checkFundPin();

        // 检查省市支行
        //TODO 检查数据集
        if (empty($province) || empty($city)) {
//            throw new Exception('请选择归属地');
        }

        if (empty($branc_name)) {
//            throw new Exception('请填写银行卡网点名称');
        }

        // 检查卡号
        $card = CardModel::withTrashed()->where('card_no', $card_no)->first();
        if ( ! empty($card)) {
            if ($card->user_id !== $user->user_id) {
                throw new Exception('卡号已存在');
            }
            $card->restore();
        } else {
            // 处理用户姓名
            if (empty($user->name)) {
                $user->update([
                    'name' => $name,
                ]);
            }

            $user->cards()->save(
                new CardModel([
                    'bank_id'    => $bank_id,
                    'card_no'    => $card_no,
                    'name'       => $user->name,
                    'city'       => $city,
                    'province'   => $province,
                    'branc_name' => $branc_name,
                    'status'     => 0,
                ])
            );
        }

        echo json_encode([
            'success' => 1,
        ]);
    }
}