<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/3
 * Time: 19:50
 */

use Lib\Bank\CardInfo;

class BankAction extends BaseAction
{
    public function run() {
        $card_no = Request::get('card_no');

        $bank_name = CardInfo::getInstance()->getBankName($card_no);

        if (empty($bank_name))
        {
            throw new Exception('请检查银行卡号');
        }

        exit(json_encode([
            'success' => 1,
            'data' => [
                'bank' => $bank_name,
            ]
        ]));
    }
}