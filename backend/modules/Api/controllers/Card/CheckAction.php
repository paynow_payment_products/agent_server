<?php

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/3
 * Time: 21:44
 */

use Withdrawal\CardModel;
class CheckAction extends BaseAction
{
    public function run()
    {
        $user = $this->checkLogin();
        $card_no = Request::get('card_no');

        $card = CardModel::withTrashed()->where('card_no', $card_no)->first();
        if (! empty($card) && $card->user_id !== $user->user_id) {
            throw new Exception('卡号已存在');
        }

        exit(json_encode([
            'success' => 1
        ]));
    }
}