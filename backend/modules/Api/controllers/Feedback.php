<?php

/**
 * User: Demo.x
 * Date: 2017/08/01
 * Time: 11:44
 */
class FeedbackController extends BaseApiController
{
    public $disableView = true;

    public $action_names = [
        'list',
        'report',
    ];
}