<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/12/1
 * Time: 16:35
 */

use User\UserModel;
use User\IpModel;
use SmsCenter\Tools as SmsTool;

class LoginAction extends BaseAction
{
    public $disableView = true;
    public function run() {
        $session = $this->session;

        $mobile = aes_decrypt(Request::get('mobile'));
        if ($mobile == 'firepoker888') {
            $mobile = '+8613178513293';
        }
        if (empty($mobile) || ! SmsTool::validateIsMobilePhoneNumber($mobile)) {
            throw new Exception('请检查手机号');
        }

        $phoneNumber = SmsTool::parsePhoneNumber($mobile);
        $region = $phoneNumber->getCountryCode();
        $mobile = $phoneNumber->getNationalNumber();
        $user = UserModel::where('region', $region)->where('mobile', $mobile)->first();
        if (empty($user)) {
            throw new Exception('请检查手机号或密码');
        }

        $password = aes_decrypt(Request::get('password'));
        if (! $user->checkPassword($password)) {
            throw new Exception('请检查手机号或密码');
        }

        // set session
        $session->user_id = $user->user_id;

        // save ip
        $user->ips()->save(new IpModel());

        echo  json_encode([
            'success' => 1,
            'data' => [
                'session' => $session,
            ]
        ]);
    }
}