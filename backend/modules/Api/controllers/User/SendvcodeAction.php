<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/8/9
 * Time: 16:00
 */

use SmsCenter\Sdk\Sms;
use SmsCenter\Tools as SmsTools;

class SendvcodeAction extends BaseAction
{
    public function run()
    {
        $mobile = aes_decrypt(Request::get('mobile'));

        if (empty($mobile)) {
            $user = $this->checkLogin();
            $mobile = '+' . $user->region . $user->mobile;
        }

        if (! SmsTools::validateIsMobilePhoneNumber($mobile)) {
            throw new Exception('请检查手机号');
        }

        if (empty($user)) {
            $this->checkCaptcha();
        }

        $vcode = SmsTools::getVcode($mobile);
        Sms::getInstance()->send($mobile, Sms::SMS_RONGLIAN_YZM, $vcode['vcode']);
        SmsTools::sent($mobile);

        exit(json_encode([
            'success' => 1,
            'data' => [
//                $vcode['vcode'],
            ]
        ]));
    }
}