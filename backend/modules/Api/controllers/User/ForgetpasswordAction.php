<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2018/6/21
 * Time: 16:18
 */

use User\UserModel;
use SmsCenter\Tools as SmsTool;

class ForgetpasswordAction extends BaseAction
{
    public function run () {
        $mobile = aes_decrypt(Request::get('mobile'));
        $phoneNumber = SmsTool::parsePhoneNumber($mobile);
        $region = $phoneNumber->getCountryCode();
        $mobile = $phoneNumber->getNationalNumber();
        $user = UserModel::where('region', $region)->where('mobile', $mobile)->first();
        if (empty($user)) {
            throw new Exception('请检查手机号');
        }
        $this->checkVcode();

        // 确认两次输入一致
        if (empty($new_pin = aes_decrypt(Request::get('new_password'))) || $new_pin !== aes_decrypt(Request::get('new_password_confirmed'))) {
            throw new Exception('请检查新密码');
        }

        $user->update([
            'password' => $new_pin,
        ]);

        exit(json_encode([
            'success' => 1
        ]));
    }
}