<?php

use User\UserModel;
use SmsCenter\Tools;
use User\IpModel;

class RegisterAction extends BaseAction
{
    public function run()
    {
        $session = $this->session;

        $mobile = aes_decrypt(Request::get('mobile'));

        $phone_number = Tools::parsePhoneNumber($mobile);
        $region = $phone_number->getCountryCode();
        $mobile = $phone_number->getNationalNumber();

        $user = UserModel::where('region', $region)->where('mobile', $mobile)->first();
        if ( ! empty($user)) {
            throw new Exception('用户已存在，请换个手机号');
        }

        $this->checkVcode();

        // 确认两次输入一致
        if (empty($new_password = aes_decrypt(Request::get('password'))) || $new_password !== aes_decrypt(Request::get('password_confirmed'))) {
            throw new Exception('请检查密码');
        }

        $user = UserModel::create([
            'region' => $region,
            'mobile'   => $mobile,
            'password' => $new_password,
            'nickname' => '+' . $region . substr_replace($mobile, '****', 3, 4),
        ]);

        // set session
        $session->user_id = $user->user_id;

        // save ip
        $user->ips()->save(new IpModel());

        exit(json_encode([
            'success' => 1,
        ]));
    }
}