<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/3
 * Time: 01:12
 */

use Withdrawal\WithdrawalModel;

class InfoAction extends BaseAction
{
    protected $disableView = true;

    public function run() {
        $user = $this->checkLogin();
        $account = $user->account()->firstOrCreate([]);
        echo  json_encode([
            'success' => 1,
            'data' => [
                'user_id' => $user->user_id,
                'nickname' => $user->nickname,
                'usable_gold' => $account->balance,
                'payable_gold' => $account->balance,
                'freeze_gold' => '0',
                'has_mobile' => true,
                'mobile' => empty($user->mobile) ? '' : '+' . $user->region . substr_replace($user->mobile,'****',3,4),
                'has_fund_pin' => ! empty($user->fund_pin),
                'financal' => [],
                'need_withdrawal_fee' => WithdrawalModel::needFee($user),
            ]
        ]);
    }
}