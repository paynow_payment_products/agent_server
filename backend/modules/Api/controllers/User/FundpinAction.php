<?php

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/7
 * Time: 13:27
 */
use SmsCenter\Tools as Smstools;
class FundpinAction extends BaseAction
{
    public function run()
    {
        $user = $this->checkLogin();

        // 验证资金密码
        if (!empty($user->fund_pin)) {
//            $this->checkFundPin();
            $mobile = '+' . $user->region . $user->mobile;
            if (empty($mobile)) {
                throw new Exception('尚未完成安全认证，请联系客服');
            }

            $vcode = aes_decrypt(Request::get('vcode'));

            if (! Smstools::checkVcode($mobile, $vcode)) {
                throw new Exception('请检查手机验证码');
            }
        }

        // 确认两次输入一致
        if (empty($new_pin = aes_decrypt(Request::get('new_fund_pin'))) || $new_pin !== aes_decrypt(Request::get('new_fund_pin_confirmed'))) {
            throw new Exception('请检查新密码');
        }

        $user->update([
            'fund_pin' => $new_pin,
        ]);

        exit(json_encode([
            'success' => 1
        ]));
    }
}