<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/3
 * Time: 02:12
 */

class LogoutAction extends BaseAction
{
    public function run()
    {
        $this->session->del('user_id');
        $this->session->del('app_id');
    }
}