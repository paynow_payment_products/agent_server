<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/3
 * Time: 01:12
 */

use Withdrawal\WithdrawalModel;
use User\UserModel;

class ProfileAction extends BaseAction
{
    protected $disableView = true;

    public function run() {
        $user_id = Request::get('user_id');
        $user = UserModel::find($user_id);
        if (empty($user)) {
            throw new Exception('无效的用户');
        }

        echo  json_encode([
            'success' => 1,
            'data' => [
                'user_id' => $user->user_id,
                'avatar' => '',
                'nickname' => $user->nickname,
                'mobile' => substr_replace($user->mobile,'****',3,4),
            ]
        ]);
    }
}