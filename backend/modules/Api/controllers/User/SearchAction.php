<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/12/20
 * Time: 15:08
 */

use User\UserModel;
use SmsCenter\Tools as SmsTool;

class SearchAction extends BaseAction
{
    public function run() {
        $this->checkLogin();

        $mobile = aes_decrypt(Request::get('mobile'));
        if ($mobile == 'firepoker888') {
            $mobile = '+8613178513293';
        }
        $phoneNumber = SmsTool::parsePhoneNumber($mobile);
        $region = $phoneNumber->getCountryCode();
        $mobile = $phoneNumber->getNationalNumber();
        $user = UserModel::where('region', $region)->where('mobile', $mobile)->first();

        if (empty($user)) {
            throw new Exception('找不到这个用户');
        }

        echo  json_encode([
            'success' => 1,
            'data' => [
                'user_id' => $user->user_id,
            ]
        ]);
    }
}