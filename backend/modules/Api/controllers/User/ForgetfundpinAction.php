<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/8/9
 * Time: 17:11
 */

use SmsCenter\Tools as SmsTools;

class ForgetfundpinAction extends BaseAction
{
    public function run()
    {
        // 检查是否已绑定手机号
        $user = $this->checkLogin();
        if (empty($user->mobile)) {
            throw new Exception('请先绑定银行卡然后才能找回资金密码');
        }

        // 检查手机验证码
        $vcode = Request::get('vcode');
        if (! SmsTools::checkVcode($user->mobile, $vcode)) {
            throw new Exception('请检查手机验证码');
        }

        // 确认两次输入一致
        if (empty($new_pin = Request::get('new_fund_pin')) || $new_pin !== Request::get('new_fund_pin_confirmed')) {
            throw new Exception('请检查新密码');
        }

        $user->update([
            'fund_pin' => $new_pin,
        ]);

        exit(json_encode([
            'success' => 1
        ]));
    }
}