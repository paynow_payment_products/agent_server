<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/3
 * Time: 20:22
 */

class NameAction extends BaseAction
{
    public function run()
    {
        $user = $this->checkLogin();

        exit(json_encode([
            'success' => 1,
            'data' => [
                'name' => (string) $user->name,
            ]
        ]));
    }
}