<?php

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/12
 * Time: 12:53
 */
use Withdrawal\PlatformModel as WithdrawalPlatform;
use Deposit\PlatformModel as DepositPlatform;

class JbpController extends BaseController
{
    public $disableActions = true;
    public $disableView = true;

    private $params;

    public function _initGetParams()
    {
        $this->params = getAllParams();
    }

    public function indexAction()
    {
        if ( ! key_exists('type', $this->params)) {
            echo 'error no type';
            exit;
        }

        switch ($this->params['type']) {
            case 'requestWithdrawApproveInformation':
                $this->requestWithdrawApproveInformation();
                break;
            case 'withdrawalResult':
                $this->withdrawalResult();
                break;
            case 'addTransfer':
                $this->addTransfer();
                break;
            case 'exceptionWithdrawApply':
                $this->exceptionWithdrawApply();
                break;
        }
    }

    private function requestWithdrawApproveInformation()
    {
        $oWithdrawalPlatform = WithdrawalPlatform::withTrashed()->find(1);
        $oWithdrawalPlatform->getPlatformAdapter()->actionWithdrawalInfoConfirm($this->params, $oWithdrawalPlatform);
    }

    private function withdrawalResult()
    {
        $oWithdrawalPlatform = WithdrawalPlatform::withTrashed()->find(1);
        $oWithdrawalPlatform->getPlatformAdapter()->actionWithdrawalResult($this->params, $oWithdrawalPlatform);
    }

    private function addTransfer()
    {
        $oDepositPlatform = DepositPlatform::withTrashed()->find(1);
        try {
            $oDepositPlatform->getPlatformAdapter()->actionDepositNotify($this->params, $oDepositPlatform);
        } catch (Exception $e) {
            $oDepositPlatform->getPlatformAdapter()->returnNotifyError($this->params, $e->getMessage());
        }
    }

    private function exceptionWithdrawApply()
    {
    }
}