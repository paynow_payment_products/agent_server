<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/3
 * Time: 17:38
 */

class CardController extends BaseApiController
{
    public $action_names = [
        'list',
        'delete',
        'bank',
        'add',
        'check',
    ];
}