<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/3
 * Time: 17:35
 */
use Withdrawal\BankModel;

class ListAction extends BaseAction
{
    public function run()
    {
        $banks = BankModel::all();
        echo json_encode([
            'success' => 1,
            'data' => [
                'banks' => $banks,
            ]
        ]);
    }
}