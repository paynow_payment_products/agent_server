<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/12
 * Time: 14:01
 */

use Deposit\PlatformModel;

class NotifyAction extends BaseAction
{

    public function run()
    {
        $platformId = Request::get('platform_id');
        $platform   = PlatformModel::find($platformId);
        if (empty($platform)) {
            throw new Exception('invalid platform');
        }

        $oPlatform = $platform->getPlatformAdapter();
        $params    = getAllParams();
        try {
            $oPlatform->actionDepositNotify($params, $platform);
        } catch (Exception $e) {
            $oPlatform->returnNotifyError($params, $e->getMessage());
        }
    }

}