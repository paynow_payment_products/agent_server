<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/4
 * Time: 02:09
 */

use Deposit\PlatformModel;

class BanksAction extends BaseAction
{
    public function run()
    {
        $platform = PlatformModel::find(Request::get('platform_id'));
        exit(json_encode([
            'success' => 1,
            'data' => [
                'lists' => $platform->getPlatformAdapter()->banks,
            ]
        ]));
    }
}