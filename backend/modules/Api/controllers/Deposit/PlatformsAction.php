<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/3
 * Time: 22:32
 */

use Deposit\PlatformModel;
use Deposit\UserPlatformModel;

class PlatformsAction extends BaseAction
{
    public function run()
    {
        if ( Request::get('type') == "all" ) {
            $platforms = PlatformModel::withTrashed()->orderBy('limit', 'desc')->get()->all();
        }else{
            $platforms = PlatformModel::all();
        }

        $lists = [];
        foreach ($platforms as $platform) {
            $lists[] = [
                'id' => $platform->platform_id,
                'name' => $platform->display_name,
                'need_bank' => $platform->need_bank,
                'bank_id' => isset($platform->custom_params['bank_id']) ? $platform->custom_params['bank_id'] : '',
                'icon' => $platform->icon,
                'max_load' => $platform->max_load,
                'min_load' => $platform->min_load,
                'pc_show' => $platform->pc_show,
                'pc_default' => $platform->pc_default,
                'mobile_show' => $platform->mobile_show,
                'mobile_default' => $platform->mobile_default,
                'ios_show' => $platform->ios_show,
                'ios_default' => $platform->ios_default,
                'android_show' => $platform->android_show,
                'android_default' => $platform->android_default,
                'description' => $platform->description,
            ];
        }

        $user = $this->checkLogin();
        $relations = UserPlatformModel::where('user_id', $user->user_id)->get();
        $data = [];
        foreach ($relations as $relation) {
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false) {
                if ($relation->terminal_type != 4) {
                    continue;
                }
            } else {
                if ($relation->terminal_type != 3) {
                    continue;
                }
            }

            $data[$relation->platform_id] = [
                'terminal_type' => $relation->terminal_type,
                'is_show' => $relation->is_show,
            ];
        }

        exit(json_encode([
            'success' => 1,
            'data' => [
                'lists' => $lists,
                'user_platform' => $data,
            ]
        ]));
    }
}