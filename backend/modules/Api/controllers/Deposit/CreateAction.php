<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/6
 * Time: 14:46
 */

use Deposit\DepositModel as DepositModel;
use Deposit\PlatformModel;

class CreateAction extends BaseAction
{
    public function run()
    {
        // check user
        $user = $this->checkLogin();

        $platform = PlatformModel::find(Request::get('platform_id'));
        if (empty($platform)) {
            throw new Exception('支付平台不存在');
        }

        $paymentAdapter = $platform->getPlatformAdapter();
        $bank_id        = '';
        if ($platform->need_bank) {
            if (!empty($platform->custom_params['bank_id'])) {
                $bank_id = $platform->custom_params['bank_id'];
            } else {
                $bank_id = Request::get('bank_id');

                if (empty($bank_id)) {
                    throw new Exception('请选择银行');
                }
            }
        }

        $fee = $platform->rate ? amount_format($platform->rate * Request::get('amount') / AMOUNT_PRECISION) : '';

        // save deposit info
        $deposit = DepositModel::create([
            'app_id'      => $this->session->app_id,
            'user_id'     => $user->user_id,
            'platform_id' => $platform->platform_id,
            'bank_id'     => $bank_id,
            'amount'      => Request::get('amount'),
            'fee'         => $fee,
        ]);

        // parse input data
        $inputData = $paymentAdapter->compileLoadInputData($deposit);

        // get break url
        $response = [];
        $loadUrl  = $paymentAdapter->compileLoadUrl($platform, $inputData, $response);

        // save order
        $paymentAdapter->saveLoadOrder($deposit, $inputData, $response, $loadUrl);

        if (!$loadUrl) {
            throw new Exception('支付系统错误');
        }

        if (!empty($deposit)) {
            echo json_encode([
                'success' => 1,
                'data'    => [
                    'order_no'      => $deposit->order_no,
                    'load_url'      => $loadUrl,
                    'load_straight' => $platform->load_straight,
                ],
            ]);
        }
    }
}
