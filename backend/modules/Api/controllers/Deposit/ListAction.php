<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/6
 * Time: 22:37
 */
use Deposit\DepositModel;

class ListAction extends BaseAction
{
    public function run()
    {
        $user = $this->checkLogin();

        $list = DepositModel::where('user_id', $user->user_id)
            ->orderBy('created_at', 'desc')
            ->paginate($this->pageSize);

        exit(json_encode([
            'success' => 1,
            'data'    => [
                'list'     => $list->items(),
                'lastPage' => $list->lastPage(),
                'status'   => [
                    DepositModel::DEPOSIT_STATUS_NEW         => '待付款',
                    DepositModel::DEPOSIT_STATUS_SUCCESS     => '完成',
                    DepositModel::DEPOSIT_STATUS_RECEIVED    => '处理中',
                    DepositModel::DEPOSIT_STATUS_WAITINGLOAD => '处理中'],
            ],
        ]));
    }
}
