<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/6
 * Time: 14:46
 */

use Deposit\DepositModel as DepositModel;
use Deposit\PlatformModel;

class GoAction extends BaseAction
{
    public function run()
    {
        // check user
        $user = $this->checkLogin();

        $order_no = Request::get('order_no');
        $deposit = $user->deposits->where('order_no', $order_no)->first();
        echo $deposit->order->load_response['returnHtml'];
    }
}
