<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/9/29
 * Time: 20:59
 */

use Transaction\OptypeModel;

class OptypeAction extends BaseAction
{
    public function run()
    {
        $list = OptypeModel::all();

        exit(json_encode([
            'success' => 1,
            'data'    => [
                'lists' => $list,
            ],
        ]));
    }
}