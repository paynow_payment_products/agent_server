<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/3
 * Time: 23:06
 */

class ListAction extends BaseAction
{
    public function run()
    {
        $user = $this->checkLogin();

        $type  = Request::get('type');
        $start = Request::get('start');
        $end   = Request::get('end');

        $transactions = $user->transactions();
        if (!empty($type)) {
            $transactions = $transactions->whereIn('type_id', explode(',', $type));
        }
        if (!empty($start)) {
            $transactions = $transactions->where('created_at', '>=', $start);
        }
        if (!empty($end)) {
            $transactions = $transactions->where('created_at', '<=', $end . " 23:59:59");
        }
        $lists = $transactions->orderBy('created_at', 'desc')
            ->paginate($this->pageSize);

        $datas = [];
        foreach ($lists->items() as $list) {
            $optype           = $list->type->display_name;
            $from_user        = $list->from_user()->first();
            $to_user          = $list->to_user()->first();
            $from_user_id     = array_get($from_user, 'user_id');
            $to_user_id       = array_get($to_user, 'user_id');
            $from_user_mobile = array_get($from_user, 'mobile');
            $to_user_mobile   = array_get($to_user, 'mobile');

            if ($list->type->app_optype == 3) {
                $optype .= '(转给' . $to_user_mobile . ')';
            }
            if ($list->type->app_optype == 4) {
                $optype .= '(转自' . $from_user_mobile . ')';
            }
            $datas[] = [
                'optype_id'        => $list->type_id,
                'optype'           => $optype,
                'optime'           => $list->created_at->toDatetimeString(),
                'delta'            => $list->amount,
                'remain'           => $list->remain,
                'from_user_id'     => $from_user_id,
                'from_user_mobile' => $from_user_mobile,
                'to_user_id'       => $to_user_id,
                'to_user_mobile'   => $to_user_mobile,
                'extra'            => $list->extra,
            ];
        }

        exit(json_encode([
            'success' => 1,
            'data'    => [
                'lists'    => $datas,
                'lastPage' => $lists->lastPage(),
            ],
        ]));
    }
}
