<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/12/8
 * Time: 15:06
 */
use User\UserModel;
use User\AccountModel;

class TransferAction extends BaseAction
{
    public function run() {
        $to_uid = Request::get('to_uid');
        $amount = Request::get('amount');
        $remarks = Request::get('remarks');

        // 检查登陆态
        $user = $this->checkLogin();
        if ($user->user_id == $to_uid) {
            throw new Exception('不能给自己转账');
        }

        // 检查收款人的uid
        $to_user = UserModel::find($to_uid);
        if (empty($to_user)) {
            throw new Exception('找不到收款用户');
        }

        if (!is_numeric($amount) || $amount < 0.01) {
            throw new Exception('转账金额不得低于1分钱');
        }

        // 转账
        AccountModel::transfer($user, $to_user, $amount, $remarks);

        // 完成
        exit(json_encode([
            'success' => 1,
            'data'    => [
            ],
        ]));
    }
}