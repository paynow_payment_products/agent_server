<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/7/3
 * Time: 01:12
 */
class UserController extends BaseApiController
{
    public $action_names = [
        'token',
        'info',
        'logout',
        'name',
        'fundpin',
        'forgetfundpin',
        'forgetpassword',
        'sendvcode',
        'login',
        'register',
        'search',
        'profile',
    ];
}
