<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/16
 * Time: 16:06
 */

use User\FeedbackModel;

class ReportAction extends BaseAction
{

    public function run()
    {
        $user = $this->checkLogin();

        $wechat  = Request::get('wechat', '');
        $qq      = Request::get('qq', '');
        $type_id = Request::get('type', '');
        $dsc     = Request::get('dsc', '');
        $imgs    = Request::get('imgs', '');
        $uid     = Request::get('uid', '');

        if ( empty($dsc) || (empty($wechat) && empty($qq)) ) {
            exit(json_encode([
                'success' => 0,
                'msg' => 'data can\'t be null',
            ]));
        }

        $t = FeedbackModel::create([
            'description_data' => $dsc,
            'type_id'          => $type_id,
            'uid'              => $uid,
        ]);

        if ( is_array(json_decode($imgs, true)) ) {
            $t->img()->createMany(json_decode($imgs, true));
        }

        if (!empty($wechat)) {
            $user->wechat = $wechat;
        }

        if (!empty($qq)) {
            $user->qq = $qq;
        }

        $user->save();
        $t->user()->associate($user)->save();

        exit(json_encode([
            'success' => 1,
        ]));
    }
}
