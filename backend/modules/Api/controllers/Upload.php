<?php

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/16
 * Time: 16:05
 */
class UploadController extends BaseApiController
{
    public $disableView = true;

    public $action_names = [
        'index',
        'captcha',
    ];
}