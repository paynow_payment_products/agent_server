<?php

/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/6/18
 * Time: 19:22
 */
class IndexAction extends BaseAction
{
    public function run()
    {
        $session = $this->session();
        $user_id = $session->user_id;
        if (!empty($user_id)) {
            $user = \User\UserModel::find($user_id);

            $this->assign('user', $user);
        }
    }
}