<?php
/**
 * Created by PhpStorm.
 * User: roger.s
 * Date: 2017/12/1
 * Time: 16:35
 */

use User\UserModel;
use User\IpModel;

class LoginAction extends BaseAction
{
    public $disableView = true;
    public function run() {
        $session = $this->session;

        $mobile = Request::get('mobile');
        $password = Request::get('password');

        $user = UserModel::where('mobile', $mobile)->first();

        if (empty($user)) {
            echo '请检查手机号或密码1';
            exit;
        }

        if (! $user->checkPassword($password)) {
            echo '请检查手机号或密码2';
            exit;
        }

        // set session
        $session->user_id = $user->user_id;

        // save ip
        $user->ips()->save(new IpModel());

        $this->redirect('/wap/');
    }
}