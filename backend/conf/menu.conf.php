<?php
return [
    [
        'name'      => '资金管理',
        'icon'      => 'desktop',
        'childrens' => [
            [
                'name'      => '充值管理',
                'childrens' => [
                    [
                        'name' => '全部',
                        'link' => '/manage/deposit/list',
                    ],
                    [
                        'name' => '新建',
                        'link' => '/manage/deposit/list?status=0',
                    ],
                    [
                        'name' => '已完成',
                        'link' => '/manage/deposit/list?status=1',
                    ],
                    [
                        'name' => '待充值',
                        'link' => '/manage/deposit/list?status=3',
                    ],
                ],
            ],
            [
                'name'      => '提现管理',
                'childrens' => [
                    [
                        'name' => '全部',
                        'link' => '/manage/withdrawal/list',
                    ],
                    [
                        'name' => '待审核',
                        'link' => '/manage/withdrawal/list?status=3',
                    ],
                    [
                        'name' => '已通过',
                        'link' => '/manage/withdrawal/list?status=5',
                    ],
                    [
                        'name' => '被拒绝',
                        'link' => '/manage/withdrawal/list?status=4',
                    ],
                ],
            ],
            [
                'name' => '账变管理',
                'link' => '/manage/transaction/list',
            ],
            [
                'name'      => '人工操作',
                'childrens' => [
                    [
                        'name' => '列表',
                        'link' => '/manage/goldoperate/list',
                    ],
                ],
            ],
            [
                'name' => '银行卡管理',
                'link' => '/manage/card/list',
            ],
        ],
        'sideBar'   => ['deposit', 'withdrawal', 'transaction', 'goldoperate', 'card',
        ],
    ],
    [
        'name'      => '系统管理',
        'childrens' => [
            [
                'name' => '用户列表',
                'link' => '/manage/user/list',
            ],
            [
                'name' => '俱乐部列表',
                'link' => '/manage/club/list',
            ],
            [
                'name'      => '平台充值渠道管理',
                'childrens' => [
                    [
                        'name' => '列表',
                        'link' => '/manage/dpsplatform/list',
                    ],
                ],
            ],
            [
                'name'      => '用户充值渠道管理',
                'childrens' => [
                    [
                        'name' => '列表',
                        'link' => '/manage/userplatform/list',
                    ],
                ],
            ],
            [
                'name'      => '平台提现渠道管理',
                'childrens' => [
                    [
                        'name' => '列表',
                        'link' => '/manage/wtdplatform/list',
                    ],
                ],
            ],
        ],
        'sideBar'   => ['user', 'club', 'dpsplatform', 'userplatform', 'wtdplatform',
        ],
    ],
    [
        'name'      => '通信日志',
        'childrens' => [
            [
                'name' => '第三方接口',
                'childrens' => [
                    [
                        'name' => '金币',
                        'link' => '/manage/remote/gold',
                    ],
                    [
                        'name' => '转账',
                        'link' => '/manage/remote/sendgold',
                    ],
                    [
                        'name' => 'token',
                        'link' => '/manage/remote/token',
                    ],
                    [
                        'name' => '用户信息',
                        'link' => '/manage/remote/userinfo',
                    ],
                    [
                        'name' => '消息列表',
                        'link' => '/manage/message/list',
                    ],
                ],
            ],
            [
                'name' => '充值',
                'childrens' => [
                    [
                        'name' => '充值发起日志',
                        'link' => '/manage/deposit/order',
                    ],
                    [
                        'name' => '充值提醒日志',
                        'link' => '/manage/deposit/log',
                    ],
                ],
            ],
            [
                'name' => '提现',
                'childrens' => [
                    [
                        'name' => 'notice日志',
                        'link' => '/manage/withdrawal/notice',
                    ],
                    [
                        'name' => '接口日志',
                        'link' => '/manage/withdrawal/log',
                    ],
                ],
            ],
        ],
        'sideBar'   => [
            'remote', 'remote', 'message',
        ],
    ],
    [
        'name'      => '管理员管理',
        'icon'      => 'list-alt',
        'childrens' => [
            [
                'name' => '管理员列表',
                'link' => '/manage/admin/list',
            ],
            [
                'name' => '管理员操作日志',
                'link' => '/manage/admin/log',
            ],
            [
                'name' => '角色列表',
                'link' => '/manage/group/list',
            ],
            [
                'name' => '功能列表',
                'link' => '/manage/functionalities/list',
            ],
        ],
        'sideBar'   => [
            'admin','group','functionalities','groupadmin','groupfunc',
        ],
    ],
    [
        'name'      => '意见反馈',
        'icon'      => 'list-alt',
        'childrens' => [
            [
                'name' => '列表',
                'link' => '/manage/feedback/list',
            ],
        ],
        'sideBar'   => [
            'feedback',
        ],
    ],
];
